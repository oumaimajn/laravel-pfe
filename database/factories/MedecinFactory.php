<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Medecin;
use Faker\Generator as Faker;

$factory->define(Medecin::class, function (Faker $faker) {
    return [
        
        'name' => $faker->name,
        'slug' => $faker->name,
        'title' => $faker->name,
        'description' => $faker->text,
        'image' => $faker->imageUrl,
        
         

    ];
});
