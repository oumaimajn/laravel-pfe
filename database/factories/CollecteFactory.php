<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Collecte;
use Faker\Generator as Faker;

$factory->define(Collecte::class, function (Faker $faker) {
    return [
        'titre' => $faker->sentence,
        'date' => $faker->date,
        'objectif' => $faker->sentence,
        'type' => $faker->sentence,
        'productimg' => $faker->imageUrl,
        'description' => $faker->paragraph,
        'descriptionn' => $faker->paragraph,
        'descriptionnn' => $faker->paragraph,
        
    ];
});
