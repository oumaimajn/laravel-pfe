<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Reclamation;
use Faker\Generator as Faker;

$factory->define(Reclamation::class, function (Faker $faker) {
    return [
        
        'description' => $faker->text,
        'image' => $faker->imageUrl,
        'user_id' => factory('App\User')->create()
    ];
});
