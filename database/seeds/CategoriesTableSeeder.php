<?php
use App\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'Foi',
            'slug' => 'foi'
        ]);

        Category::create([
            'name' => 'Sang',
            'slug' => 'sang'
        ]);

        Category::create([
            'name' => 'Sein',
            'slug' => 'sein'
        ]);

        Category::create([
            'name' => 'Peau ',
            'slug' => 'peau'
        ]);

        Category::create([
            'name' => 'Cancer de lestomac',
            'slug' => 'estomac'
        ]);
    }
}
