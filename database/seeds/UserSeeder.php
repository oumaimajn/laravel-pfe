<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name' => 'admin']);
        Role::create(['name' => 'medecin']);
        Role::create(['name' => 'patient']);
          

        //create user admin and assign role admin
        $admin = factory(App\User::class)->create([
            'name' => 'admin',
            'email' => 'admin@gmail.com'
            //password par defaut


        ]);

        $admin->assignRole('admin');
         

        //create user admin and assign role admin
        $admin = factory(App\User::class)->create([
            'name' => 'medecin',
            'email' => 'medecin@gmail.com'
            //password par defaut


        ]);

        $admin->assignRole('medecin');

        //create user admin and assign role admin
        $client = factory(App\User::class)->create([
            'name' => 'patient',
            'email' => 'patient@gmail.com'
            //password par defaut


        ]);

        $client->assignRole('patient');

    }
}
