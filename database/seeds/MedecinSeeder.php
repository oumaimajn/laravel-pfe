<?php
use App\Medecin;
use Illuminate\Database\Seeder;

class MedecinSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($i=0; $i < 10; $i++) {
Medecin::create([
    'name'=> $faker->sentence(4),
    'slug'=> $faker->slug,
    'title'=>$faker->sentence(5),
    'description' => $faker->text,
    'image' =>'https://via.placeholder.com/200x250'

    
]);
        }
    }
}

