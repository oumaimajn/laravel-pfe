<?php
use App\Specialite;
use Illuminate\Database\Seeder;

class SpecialiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($i=0; $i < 10; $i++) {
Specialite::create([
    'name'=> $faker->sentence(4),
   
]);
        }
    }
}
