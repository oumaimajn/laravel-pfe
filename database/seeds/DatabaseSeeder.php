<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       // $this->call(RolesTableSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(MedecinSeeder::class);
       // $this->call(SpecialiteSeeder::class);
        $this->call(CategoriesTableSeeder::class);

         $this->call(ProductsTableSeeder::class);
    }
}
