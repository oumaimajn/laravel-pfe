<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollectesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collectes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titre');
            $table->dateTime('date');
            $table->string('objectif');
            $table->string('type');
            $table->string('productimg');
            $table->longText('description');
            $table->longText('descriptionn');
            $table->longText('descriptionnn');
            
            $table->timestamps();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collectes');
    }
}
