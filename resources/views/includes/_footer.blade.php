<footer class="ftco-footer ftco-section">
      <div class="container">
        <div class="row d-flex">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Solution de Cancer</h2>
              <p>solution de cancer ce une application web qui aide les maladie cancerieuse.</p>
              <ul class="ftco-footer-social list-unstyled float-lft mt-3">
                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4 ml-md-4">
              <h2 class="ftco-heading-2">Liens populaires</h2>
              <ul class="list-unstyled">
                <li><a href="#">les aides financiers</a></li>
                <li><a href="#">le forum de duscusion</a></li>
                <li><a href="#">Traitement & conseil</a></li>
                <li><a href="#">Brochures d'information</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Liens rapides</h2>
              <ul class="list-unstyled">
                <li><a href="http://laravel-tp1.test/">ACCUEIL</a></li>
                <li><a href="http://laravel-tp1.test/informations">INFORMATION</a></li>
                <li><a href="http://laravel-tp1.test/boutique">MÉDICAMENT </a></li>
                <li><a href="http://laravel-tp1.test/medecin">MÉDCINS</a></li>
                <li><a href="http://laravel-tp1.test/register">Inscript</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Vous avez des questions?</h2>
              <div class="block-23 mb-3">
                <ul>
                  <li><span class="icon icon-map-marker"></span><span class="text">tunis,nabeul</span></li>
                  <li><a href="#"><span class="icon icon-phone"></span><span class="text">+216 00 000 000</span></a></li>
                  <li><a href="#"><span class="icon icon-envelope"></span><span class="text">sc@solutioncancer.com</span></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">

            <p class="mb-0">
              <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
              Copyright &copy;<script>
                document.write(new Date().getFullYear());

              </script> site solution de cancer <i class="icon-heart" aria-hidden="true"></i> par <a href="https://colorlib.com" target="_blank">oumaima & sirine</a>
              <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </p>
          </div>
        </div>
      </div>
    </footer>
    
 