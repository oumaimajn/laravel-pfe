
@extends('adminlte::page')

@section('title', 'create specialite')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
<h1>create specialite</h1>
<form action="{{  route('specialites.store') }}" method="post" enctype="multipart/form-data">
@csrf
  <div class="form-row">
  
    <!-- error pour les condition  -->
    <div class="form-group col-md-6">
      <label for="name">name</label>
      <input type="text" class="form-control @error('name') is-invalid @enderror"  name="name" id="title">
      @error('name')
      <span class="invalid-feedback" role="alert">
         <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>

    
  <button type="submit" class="btn btn-primary">envoye!</button>
</form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop




