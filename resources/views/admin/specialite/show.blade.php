

@extends('adminlte::page')

@section('title', 'show')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
     
<h1>{{$specialite->name}}</h1> 
    



    <div class="card" style="width: 18rem;">
   
   <div class="card-body">
     
     <p class="card-text">{{$specialite->name}}</p>
     
   </div>
 </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
