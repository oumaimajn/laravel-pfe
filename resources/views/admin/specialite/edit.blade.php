





@extends('adminlte::page')

@section('title', 'edit specialite')

@section('content_header')
<h1>update specialite</h1>
@stop

@section('content')


<div>
 @if(session()->get('success'))
<div class="alert alert-danger" role="alert">
A simple danger alert—check it out!
</div>
@endif
</div>



<form action="{{  route('specialites.update',$specialite->id) }}" method="post">
@csrf
@method('patch')
<div class="form-row">

  
  <div class="form-group col-md-6">
    <label for="name">name</label>
    <input type="text" name="name" class="form-control" id="name" value="{{$specialite->name}}">
  </div>

</div>
<button type="submit" class="btn btn-primary">update</button>
</form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop





