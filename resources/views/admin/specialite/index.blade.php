

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')



<!-- flash sms 1 -->
<div>
 @if(session()->get('success'))
<div class="alert alert-success" role="alert">
{{ session()->get('success')}}
</div>
@endif
</div>


<!-- flash sms 2 -->
<div>
 @if(session()->get('error'))
<div class="alert alert-danger" role="alert">
{{ session()->get('error')}}
</div>
@endif
</div>



      <a href="{{route('specialites.create')}}" class="btn btn-primary"> new specialite</a>
      <table class="table">

<thead>
  <tr>
    
    
    <td>name</td>
   

  </tr>
</thead>
<tbody>
 @foreach($specialites as $specialite)
  <tr>
    
    <td>{{ $specialite->name}}</td>
    
    
    
  <td><a href="{{ route('specialites.show',$specialite->id) }}" class="btn btn-primary">show</a></td>
  <td><a href="{{ route('specialites.edit',$specialite->id) }}" class="btn btn-success">edit</a></td>
  <!--delete -->
  <td>
  <form action="{{  route('specialites.destroy',$specialite->id) }}" method="post">
  @csrf
  @method('DELETE')
  <button type="submit" class="btn btn-danger">delete</button>
  </from>
  
  </td>
   <!-- end delete -->
  
  </tr>
  @endforeach
</tbody>

</table>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop