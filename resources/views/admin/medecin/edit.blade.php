

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>update medecin</h1>
@stop

@section('content')


<div>
 @if(session()->get('success'))
<div class="alert alert-danger" role="alert">
A simple danger alert—check it out!
</div>
@endif
</div>

<form action="{{  route('medecins.update',$medecin->id) }}" method="post">
@csrf
@method('patch')
<div class="form-row">
  <div class="form-group col-md-6">
      <label for="name">name</label>
      <input type="text" class="form-control" name="name" id="name" value="{{$medecin->name}}">
    </div>

  
<div class="form-group col-md-6">
    <label for="title">title</label>
    <input type="text" name="title" class="form-control" id="title" value="{{$medecin->title}}">
  </div>
  
  <div class="form-group col-md-6">
    <label for="description">description</label>
    <textarea name="description" class="form-control" id="description" cols="30" rows="10">{{$medecin->description}}</textarea>
  </div>



</div>
<button type="submit" class="btn btn-primary">update</button>
</form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
