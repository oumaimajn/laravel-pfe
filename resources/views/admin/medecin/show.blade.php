
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Show </h1>
@stop

@section('content')
   

<h1>{{ $medecin->name}}</h1>
    <h1>{{ $medecin->title}}</h1>
    <h1>{{ $medecin->description}}</h1>
    <h1>{{ $medecin->image}}</h1>

  

   <div class="card" style="width: 18rem;">
  <img src="{{$medecin->name}}" class="card-img-top" alt="...">
  <div class="card-body">
    
    <p class="card-text">{{$medecin->name}}</p>
    <p class="card-text">{{$medecin->title}}</p>
    <p class="card-text">{{$medecin->description}}</p>
    <p class="card-text">{{$medecin->image}}</p>
    
    
  </div>

</div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
