
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1>page index</h1>
@stop

@section('content')



<!-- flash sms 1 -->
<div>
 @if(session()->get('success'))
<div class="alert alert-success" role="alert">
{{ session()->get('success')}}
</div>
@endif
</div>


<!-- flash sms 2 -->
<div>
 @if(session()->get('error'))
<div class="alert alert-danger" role="alert">
{{ session()->get('error')}}
</div>
@endif
</div>

     

      <a href="{{route('medecins.create')}}" class="btn btn-primary"> New medecin</a>
      <table class="table">

<thead>
  <tr>
   
    <td>name</td>
    <td>title</td>
    <td>description</td>
    
    <td>image</td>
    
  </tr>
</thead>
<tbody>
 @foreach($medecins as $medecin)
  <tr>
    
    <td>{{ $medecin->name}}</td>
    <td>{{ $medecin->title}}</td>
    <td>{{ $medecin->description}}</td>
    <td>{{ $medecin->image}}</td>
    
    
  <td><a href="{{ route('medecins.show',$medecin->id) }}" class="btn btn-primary">show</a></td>
  <td><a href="{{ route('medecins.edit',$medecin->id) }}" class="btn btn-success">edit</a></td>
  <!--delete -->
  <td>
  <form action="{{  route('medecins.destroy',$medecin->id) }}" method="post">
  @csrf
  @method('DELETE')
  <button type="submit" class="btn btn-danger">delete</button>
  </from>
  
  </td>
   <!-- end delete -->
  
  </tr>
  @endforeach
</tbody>

</table>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop