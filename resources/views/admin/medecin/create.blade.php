
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>create</h1>
@stop

@section('content')
<form action="{{  route('medecins.store') }}" method="post" enctype="multipart/form-data">
@csrf
  <div class="form-row">
  
    <!-- error pour les condition  -->
    <div class="form-group col-md-6">
      <label for="name">name</label>
      <input type="text" class="form-control @error('name') is-invalid @enderror"  name="name" id="name">
      @error('name')
      <span class="invalid-feedback" role="alert">
         <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>

    <div class="form-group col-md-6">
      <label for="slug">slug</label>
      <input type="text" class="form-control @error('slug') is-invalid @enderror"  name="slug" id="slug">
      @error('slug')
      <span class="invalid-feedback" role="alert">
         <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>

    

    <div class="form-group col-md-6">
      <label for="title">title</label>
      <input type="text" class="form-control @error('title') is-invalid @enderror"  name="title" id="title">
      @error('title')
      <span class="invalid-feedback" role="alert">
         <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>

    <div class="form-group col-md-6">
      <label for="description">description</label>
      <textarea name="description" class="form-control  @error('description') is-invalid @enderror" id="description" cols="30" rows="10"></textarea>
      @error('description')
      <span class="invalid-feedback" role="alert">
         <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>

    <div class="form-group col-md-6">
      <label for="image">image</label>
      <input type="file" class="form-control-file @error('image') is-invalid @enderror" name="image" id="image">
      @error('image')
      <span class="invalid-feedback" role="alert">
         <strong>{{ $message }}</strong>
      </span>
      @enderror
  
  </div>


  <button type="submit" class="btn btn-primary">envoye!</button>
</form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop