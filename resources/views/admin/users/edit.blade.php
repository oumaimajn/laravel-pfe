@extends('layouts.master6')

@section('content')
<div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('master5.name', 'Solution Cancer') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                

                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a href="{{ route('topics.create') }}" class="dropdown-item">creér un forum</a>
                                    
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                    <a class="dropdown-item" href="{{ route('admin.users.index') }}">Liste des utilisateurs</a>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>




        <section class="hero-wrap js-fullheight" style="background-image: url('images/z1.jpg');" data-stellar-background-ratio="0.5">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Modifier <strong>{{ $user->name }}</strong></div>
                <div class="card-body">
                 <form action="{{ route('admin.users.update', $user) }}" method="post">
                   @csrf
                   @method('PATCH')
                 @foreach ($roles as $role)
                       <div class="form-group form-check">
             <input type="checkbox" class="form-check-input" name="roles[]" value="{{ $role->id }}"
                         id="{{ $role->id }}" @foreach ($user->roles as $userRole) 
                           @if ($userRole->id == $role->id)
                            checked @endif
                         @endforeach >
                     <label for="{{ $role->id }}" class="form-check-label">{{ $role->name }}</label>
                       </div>
                  @endforeach
                  <button type="submit" class="btn btn-primary">Modifier les roles</button>
                 </form>
                </div>    

            </div>
        </div>
    </div>
</div>
</section>
@endsection
