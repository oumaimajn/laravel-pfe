@extends('adminlte::page')

@section('title', 'create product')

@section('content_header')
<h1><a class="btn btn-primary" href="{{ route('product.index') }}"><i class="fas fa-hand-point-left"></i> go back</a></h1>
    @stop
@section('content')
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Edit products</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->



  <div>
   @if(session()->get('success'))
  <div class="alert alert-danger" role="alert">
  A simple danger alert—check it out!
 </div>
 @endif
  </div>



<form action="{{  route('product.update',$product->id) }}" method="post">
@csrf
@method('patch')
  <div class="form-row">
  <div class="form-group col-md-6">
      <label for="title">title</label>
      <input type="text" name="title" class="form-control" id="name" value="{{$product->title}}">
    </div>

    <div class="form-row">
  <div class="form-group col-md-6">
      <label for="slug">slug</label>
      <texte name="slug" class="form-control" id="slug" value="{{$product->slug}}">
    </div>
    
    <div class="form-row">
  <div class="form-group col-md-6">
      <label for="subtitle">subtitle</label>
      <texte name="subtitle" class="form-control" id="subtitle" value="{{$product->subtitle}}">
    </div>
    
    <div class="form-group col-md-6">
      <label for="description">description</label>
      <textarea name="description" class="form-control" id="description" cols="30" rows="10">{{$product->description}}</textarea>
    </div>

    <div class="form-row">
  <div class="form-group col-md-6">
      <label for="price">price</label>
      <texte name="price" class="form-control" id="price" value="{{$product->price}}">
    </div>
  </div>
  <button type="submit" class="btn btn-primary">update</button>
</form>

<!-- /.card -->

</div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop
