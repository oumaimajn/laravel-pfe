@extends('adminlte::page')

@section('title', 'list categories')
@section('content_header')
<h1><a class="btn btn-primary" href="{{route('product.create')}}">New produit <i class="fas fa-plus-square"></i></a></h1>
@stop

@section('content')

<div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">List produit</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table class="table table-bordered">
          <thead>                  
            <tr>
              <th>Name</th>
              <th style="width: 40px">Action</th>
            </tr>
          </thead>
          <tbody>


<!-- flash sms 1 -->
<div>
 @if(session()->get('success'))
<div class="alert alert-success" role="alert">
{{ session()->get('success')}}
</div>
@endif
</div>


<!-- flash sms 2 -->
<div>
 @if(session()->get('error'))
<div class="alert alert-danger" role="alert">
{{ session()->get('error')}}
</div>
@endif
</div>



     
      <table class="table">

<thead>
  <tr>
    
    
    <td>title</td>
    <td> slug</td>
    <td>subtitle</td>
    <td>price</td>
   
    
    
    <td></td>
    
   

  </tr>
</thead>
<tbody>
 @foreach($products as $product)
  <tr>
    
    <td>{{ $product->title}}</td>
    <td>{{ $product->slug}}</td>
    <td>{{ $product->subtitle}}</td>
    <td>{{ $product->price}}</td>
  
  
    

    
    
  <td><a href="{{ route('product.show',$product->id) }}" class="btn btn-primary">show</a></td>
  <td><a href="{{ route('product.edit',$product->id) }}" class="btn btn-success">edit</a></td>
  <!--delete -->
  <td>
  <form action="{{  route('product.destroy',$product->id) }}" method="post">
  @csrf
  @method('DELETE')
  <button type="submit" class="btn btn-danger">delete</button>
  </from>
  
  </td>
   <!-- end delete -->
  
  </tr>
  @endforeach
</tbody>

</table>
 <!-- /.card-body -->
 <div class="card-footer clearfix">
        <ul class="pagination pagination-sm m-0 float-right">
          <li class="page-item"><a class="page-link" href="#">«</a></li>
          <li class="page-item"><a class="page-link" href="#">1</a></li>
          <li class="page-item"><a class="page-link" href="#">2</a></li>
          <li class="page-item"><a class="page-link" href="#">3</a></li>
          <li class="page-item"><a class="page-link" href="#">»</a></li>
        </ul>
      </div>
    </div>
    <!-- /.card -->

  </div>
@stop

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
@stop

@section('js')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

    <script src="https://kit.fontawesome.com/3521b5e541.js" crossorigin="anonymous"></script>
@stop

 


