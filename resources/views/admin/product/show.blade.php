@extends('adminlte::page')

@section('title', 'show product')
@section('content_header')
<h1><a class="btn btn-primary" href="{{ route('product.index') }}"><i class="fas fa-hand-point-left"></i> go back</a></h1>
    @stop




@section('content')
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">show products</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->



  

   <div class="card" style="width: 18rem;">
  <img src="{{asset('storage/'.$product->name)}}" class="card-img-top" alt="...">
  <div class="card-body">
    
    <p class="card-text">{{$product->title}}</p>
    <p class="card-text">{{$product->slug}}</p>
    <p class="card-text">{{$product->subtile}}</p>
    <p class="card-text">{{$product->description}}</p>
    <p class="card-text">{{$product->price}}</p>
   
    
  </div>

</div>


<!-- /.card -->

</div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

