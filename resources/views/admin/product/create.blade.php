@extends('adminlte::page')

@section('title', 'create produit')

@section('content_header')
    <h1><a class="btn btn-primary" href="{{ route('product.index') }}"><i class="fas fa-hand-point-left"></i> go back</a></h1>
@stop

@section('content')
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Create produit</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->




<form action="{{  route('product.store') }}" method="post" enctype="multipart/form-data">
@csrf
  <div class="form-row">
  
    <!-- error pour les condition  -->
    <div class="form-group col-md-6">
      <label for="title">title</label>
      <input type="text" class="form-control @error('title') is-invalid @enderror"  name="title" id="title">
      @error('title')
      <span class="invalid-feedback" role="alert">
         <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>

    

    <div class="form-group col-md-6">
      <label for="slug">slug</label>
      <input type="text" class="form-control @error('slug') is-invalid @enderror"  name="slug" id="slug">
      @error('slug')
      <span class="invalid-feedback" role="alert">
         <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>
    <div class="form-group col-md-6">
      <label for="subtitle">subtitle</label>
      <input type="text" class="form-control @error('subtitle') is-invalid @enderror"  name="subtitle" id="subtitle">
      @error('subtitle')
      <span class="invalid-feedback" role="alert">
         <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>

    <div class="form-group col-md-6">
      <label for="description">description</label>
      <textarea name="description" class="form-control  @error('description') is-invalid @enderror" id="description" cols="30" rows="10"></textarea>
      @error('description')
      <span class="invalid-feedback" role="alert">
         <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>
    <div class="form-group col-md-6">
      <label for="price">price</label>
      <input type="text" class="form-control @error('price') is-invalid @enderror"  name="price" id="price">
      @error('price')
      <span class="invalid-feedback" role="alert">
         <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>

    <div class="form-group col-md-6">
      <label for="image">image</label>
      <input type="file" class="form-control-file @error('image') is-invalid @enderror" name="image" id="image">
      @error('image')
      <span class="invalid-feedback" role="alert">
         <strong>{{ $message }}</strong>
      </span>
      @enderror
  
  </div>
  
  <button type="submit" class="btn btn-primary">envoye!</button>
</form>

 <!-- /.card -->

 </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop
