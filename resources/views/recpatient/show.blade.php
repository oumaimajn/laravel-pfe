@extends('layouts.master4')

@section('title')
index reclamation
@endsection




@section('content')
 
  

<section class="hero-wrap hero-wrap-2" style="background-image: url('images/a1.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
          <h1>{{$reclamation->description}}</h1> 
    



    <div class="card" style="width: 18rem;">
   <img src="{{$reclamation->image}}" class="card-img-top" alt="...">
   <div class="card-body">
     
     <p class="card-text">{{$reclamation->description}}</p>
     
   </div>
 </div>
            
          </div>
        </div>
      </div>
    </section>

@endsection


