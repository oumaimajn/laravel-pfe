@extends('layouts.master4')

@section('title')
update reclamation
@endsection

@section('content')


<h1>update reclamation</h1>

  <div>
   @if(session()->get('success'))
  <div class="alert alert-danger" role="alert">
  A simple danger alert—check it out!
 </div>
 @endif
  </div>



<form action="{{  route('reclamation.update',$reclamation->id) }}" method="post">
@csrf
@method('patch')
  <div class="form-row">
  
    
    <div class="form-group col-md-6">
      <label for="description">description</label>
      <textarea name="description" class="form-control" id="description" cols="30" rows="10">{{$reclamation->description}}</textarea>
    </div>
  
  </div>
  <button type="submit" class="btn btn-primary">update</button>
</form>

@endsection


