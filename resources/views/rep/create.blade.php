
@extends('layouts.master4')

@section('title')
create reclamation
@endsection

@section('content')

<section class="hero-wrap hero-wrap-2"  data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-3 bread">Create Reclamation</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Medecin</a></span> <span>Contact us</span></p>
          </div>
        </div>
      </div>
    </section>






    <div class="col-md-12 ftco-animate text-center">
<form action="{{  route('repondres.store') }}" method="post" enctype="multipart/form-data">
@csrf
  <div class="form-row">
  
    <!-- error pour les condition  -->
    <div class="form-group col-md-6">
      <label for="description"><strong >description</strong></label>
      <textarea name="description" class="form-control  @error('description') is-invalid @enderror" id="description" cols="30" rows="10"></textarea>
      @error('description')
      <span class="invalid-feedback" role="alert">
         <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>

   
  </div>
  <button type="submit" class="btn btn-primary py-3 px-5">envoye!</button>
</form>
</div>

@endsection
