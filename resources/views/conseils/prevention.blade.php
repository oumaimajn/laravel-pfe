<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>conseil</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/blog/">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <!-- Bootstrap core CSS -->
<link href="/docs/4.5/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/4.5/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/4.5/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/4.5/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon.ico">
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
<meta name="msapplication-config" content="/docs/4.5/assets/img/favicons/browserconfig.xml">
<meta name="theme-color" content="#563d7c">
<style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
     <!-- Custom styles for this template -->
     <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="blog.css" rel="stylesheet">
  </head>
  <body>
  
    
  <div class="text-center bg-light ">
    <div class="col-md-6 px-0 text-center">
   
       
      <table>
      <tr>
      <td><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAO8AAADTCAMAAABeFrRdAAAAhFBMVEX39/eK4/n///+A2e6D4vmA4fn7+Pf8+PeF4/nz/P73/f+Q5PmL4/n7/v/s+v6u6/vP8/x11u277vvj+P3F8PzM8vyY5vrB7/zY9f2g6Pqw6/vd9v2l6frs8vWv5PXk8PW95/Xa7fXO6vSb4PHE6PSW3/S05fWK3O/U7PR23/iU5vmj4fVmFIAvAAAUf0lEQVR4nO2dC3equhKAfSQgbwREVLRaa3eP/f//7yaTB1FAQwRt7+qss87p7nELH/PIzCQko9Gf/Mmf/Mmf/Mmf/Mn/m1iqvPpmhhMCZ9v2aLQ9fFEp4N+H7WhEfvt/Bk5IrdHha7875RhjpAr5c37a7b8OI/KZ/wdootTRYf/tEczZbNwssxkB9773B6Lr38xM9Lp9/zxh1AZ6hY3w6fN9+0v1TGDf9jVWbsNC6B9rzPu334ds2YdPX2WlmH4eJVm8ScPQIRKGaVrEWVLmYwquMvufh99k2MSM9z6eKah+mRRhMGkTN40TQl0xz7C/3/4SZMt6P6mw56RwWklVCeO1VzHP8On9F5g1Va24Z4THUeFqsVbMZeXSiCr51UA3xd7uhGoJbJJewWzi5bpc5Z4/o1C+l6+IQxfplfo3a0k8w7sfTKzSorUK62yyyGsIxxCwye/z9eLi2WwiYdg/l7iiRTiPVRONxrjGWcMmiVaiGH+wOOOfTGyNPgUtWoeKbSoh6J4QXefL6u+mJf+rM/w5+lmRy7I/kKBNpJKKCOnDCmbsV27vCE+eoY+fNDrZbyfEaZdilE3XY9yRVSJ7SxHC3IQTo9PbTzFqa7TDQreCNs47a/YSeVUI4jV/bHj3M4zaPvjMlHHkXinlEcF+xh+eUzLimX94vYotiysX56FwuodUWwnCIhSkHvvG16vYemPKRYiPQE7UE+0lccbjlv/2UmASltmDL9l9BUmPtECMltxHVjxGfLzOpq3RN3/sPLgsevDbGvGYW07MA/X3q2za2npgy0K56bl/Wvh+HhmcHL5/5m1fAmwfeKBasOe/NhxudYgTdoklv+Ir4rT9ARdHPnv4m/EwymWCfJZzpTxcPN+J7U+GuxpcuUy4il1m0+jzycA2i1T8LkJvSOUyQV6oPFn0/VRg68giFQudi6GVy4nZ1TK42uz4xKBlnViSwZwqeg4uebxrVnYx4NPzgBnuOFQ86imC8qCKWrPTk2i5dn2o2cKnwYKw0SDkwE/RsM1813Mr23qiMB9y4OfZ8QlBi0dmhvukSKUKLirgJ0Rpe8e0G1Sh8tnAsaLh3cDA9p5FJ1dJ754OvKh8GO0HBba+2GgPoSp5Da4ATuHy+GvAoGW9sWukL9Qu3EBcxUo8ZAfAhysUL/NdCazcgj8YrQ0DL85eMhBdAYOJRdSHZ6eBXNjeU1wUVc7zQmEhJIcfh4lZFqvvzzAaPC+HbBOfjogu/IgPg7gwOC97rv5LUZnklZ0N4cL2NzgvBIrV69VLnjzU3ktw4f7zLOudfjFz3uQn4Ipnz1z4vXeLBkYfWlX3Y9V1X3aAPu2Y+5bLGi0903JrTuUFbt9IEqtNAJQvskE6PrkcGXu2aOsLrBk8ZnX/PsLlKi4qwCgsyT8D8LIbgm9G/eaVEJA9vRIQZSSzxoV8LijEYzQOh1CwYnB9xmgLMg280Rt50eZMH30V1eiggcNBMhRfqmC270/BW7jXUtOaMeh3o+iXLstx/CEUjNYyRuNtX7gsWEE0jO9qCa/DSbgsC8V/yzBah66zGYCX2I3IOnoLWawKZBOTd3WEiziNcLIo1fh8zjJvFkbOEMBnWTj0VRnaJ/hirUwDLbNijcbXk0l0/PWd1RBVBqLFvwtf3E+hZB1gLKIltkbMCdetJl8W8SCDUiAUgXqpG5h6fTnS3bz2Imofd/CmHETBiXS0PhSsqPf+3aIwWbebfF5svP44q4s6om7oQ8FMvZ7eWLSK0xsejtNkiFKDjUk9KZgHZ031ZuvNDaA7/9tYsFTw4yGa99f11IvuKLBcpBjx9zRavwOjc577uMNCH6hSIat8vP8OqRXK9FpWKF3cfCj+pvAy+qbGJk5WTcgIyQXx4WKljQwKhsDxaJLFMmfk6gRnMvYXN93Xw+my5EtPaB1dXj1BNF5MVHF01yVCiA77yKItKIyovTgaQ0mZtRsB8tMCb9bLlQIUrtSP4+XkWgLNdSEwBoNp+Q/xssEIqi6NyIqSdat+cUQr86KMZxdAsfy8WJ9xJaHWdDokWZCxP1YHs0rhrJU506uWbfEXswgQl5v/Lnn4IrIxKptoqWhNU8F4SX94sGqQ0arQ4S3Klo+x1lqIsyj97/plJOj8sEZgs9yvyVQTfKSTZb1Dne9MtOpeUuiXcSMvwyUhYLlOcXrNQxR4C1dv7iaaiIj1QKvSPtKvWk30ohUZjspFE69Y4+rgJElxUePJ8Kr2uwtZ3jcuTD8H2eoDOdYWIgCNBZlW1EjLxs/l/LZdvCa8izpP1v5qIZP8/rUL8VzMh2DWY8eu1gWBN2rSBBaBN0DRMmXTix1FoxAtxcfMe+8sOlPtaJlzG6/0TcZbH2U15Hj/4tRGaLbwQISW0bnRLTV5oa0peTcoaadql8387sWpQT8UoXmyQa1RJzq38SJ509R/DXkn0+m9q1Mz2jxSBfPcGRSjybtqiFe5vOcQr9eG9jw53gWmHZjgkRyaVfrw2PSyWOyGDYZfDa0bvA4do3hFQvj0HjAYIsRV0xEJ3JcOH9pdiYZ5cMV+STJCytqG8UhDivk9YAg0S3MHVtxXazS6dRtMlrIT1l1SynsbmI5IDziw9dHRfce0dVH7qKLPBHobZrwh8N4Gnogux4cJr/1Nv4NmeppdVJS4jlszfUWfa49+wIzXYby3gMEUIaU0GoFtf8y9T2/0ZZqMar+t+CJatS3N/Jfr9xYwXAmu7xsFLAhXRRPDjevVRmrFnkvKm5jxpoK3HRh0k5kGLCVcaTbJ4dmca7+t4lVOGwfrB+LzHWDqe8YBi4eroEO4ory1EUkZj3yaepRm/ptNp/eBJzzTNwlYLLuiFqg7Mw+89V9X+QamI0ZuxhtN7wNDG5X+YJJhVeFZd04A/Ldu+zKfdODtrHG93teRf9P7wNA6AYcyCNA2TTK6hGfmqQ2pibjlDZT6/23akG6JO5/eBwYDgzZ5bhCgZTGom03Cw6l35WW5n9HnH/5X61/pyOaKtxG4uluTAC2HI+1J6qjx4cgBeEV7TEWtPykkLBaLorEHTWQ3vZYmXjpRaDggWW8Q2KnxaWfP1FMbGpQ8YAXQlUtwI0+w9GF3HTxeNm6oVMNtBKbxkHXdO88TKsOvLi54akMw5wV/DO6be004MUJjNnE4Rk0FVHptzs3A9IEbDsBsBSG0nrUnJlueDmIR6kwbfy5/5/FS1niMz1nqBmFcYph6uWvOjcC05Idkv/usigU7L3RJN7inNkQ3mCopYPSNG9rPFBfJX4c5qnffm9TbAIyEgaHOCQdv5gRa62FVsAaDpooNeCMMXaPA9AF999LJohK2CirRdc24aOG9BqZf7pq1dKxPMa+v14tl14N54nqEXpLfYjapWu/WBWzFCezKgnDu0ACJLhvw/1pwr4FBPcD7acbrNyuslZf24hrsAREan1lufSYhg71Z5LQnCsk3oIumXm3wbQOmzxvc70m8TMEN01seQiGotyH2nulVFK+HP/l66r0CfoQX7qBLuQACoak+gYnYa7urvI7rYKJMV11tGZNLYiXxuKXeS2DJi5In8bLqL71a+wteSft1DbnEBpPx6iJLIc9srIbxm+q9AH6Yt6s9y0UYWbVbKMJndvOL/5qyxQIRXV4MYuQhe0oXqDU414Hl8GnA+2nGK7OFdFmex2h8LjNOmTRXCnVe/4I3uEerAD8cn8dBp/GIXbSxwg1z3FwLpJj49kXJuaL2LKvG6D7udMq7KtX4a8jbMd8QKvauexhhhOvrGJiQwSe7eKYkfybxSlRRjZlzG7DMrwx42SsaNMAYLHkkKWEs9gMO0gw272uIzSA5ddiousiMjkeyqtCiFSbtTcTa/u75FbTrwAbNlngiPpOQiDmHtonQGJFgHMj3sYgdBFWNVOryArCsj7o37Hh9RI3QdM0yu+kqFDWVChP2/cEkYJuJYrrxE8lIxbPQs2YBXNW/3esjtsyb3qLeZHcbrzIDjtoi1vgc0A068zxiFZaIVmEHXAI8kw13g/r3INfmrE15odOuzoD7zZtBZ6RAktHMJeURz551hiJVqv6VScMd6shkorX6SZN33NjcAGC8goXAKSmT5AqAe4nVtcxlt820Xwce0bxmTpf3spvbEqRTjxDTa5HILlV97Ig7ncv+swGvBWHqPDHfm4AVdZdPi+/oVJfCh66GV0XxzrjTuZhfGHsG8wuweoOu3u2ecHC2pM5LbqWlH5tzXp6qBF2NmcpEZIMGKzhYggV5juHuBIy3tmIWNSda5wtexwT3OBHDb+f0SjawNpMODXct3nFT0X/FmxrQTqdy8sdkvowPSNQHDQM0422YbWvsyaq890vAJpnL+X2jBSuwOLZKWQx41y28jVFL4f02wp3O5dw8Mlkia7OwPOleEV7wNkf3Wc2JBW/hGMFSmcjdGozWq8AEMAQswwz6Bu+4tsyO885KM+VOebiCqxmtz+EBi+YsN96BvCXQ6Whrj+CV08Q7NtbuVK6vM1tAyfegWJs78E3eapl/T7yQXUFtY/hODtulwDd3YGjO3vi7yC/61C/9FrblmdmKfhvcFhzYbAHlPV7a+ZHE+aO8MtswXG42slkLazExHYHhrRP3tm2gcUazXidj5m/OO5fr22eGmyUzB26b9NPlDe79VYRXGbwKWjqO8wBvIEdf41fqmDfILxqGl94hB6etLlPeo1SL8RtmrESqXoToKlDs6k+Xl0ny+WmsXppMst6C8QtXrEdpbtCsuNc7fFCs4zHNNsCcIayarX4GYdtrIvlVHQXeK9Ve7gK8XVtWUipzRm+muMKgoWQxMGjWrdKtnsFvTHnnG+l0D7w/yFJKZpcGBs14dUMdlNqBqT3LJ/vQG+0sxYJZBoMc2u/EC1e5XiepK3tZKzy0YwE3aLkdQEdhiaLmh2EyvFuHXcq82lDhoS1H2AvPMC1qMM3AXtXXjnT0w3fWLrTJTiauj7zuTAWsGHJKzZfMFEGdeCE7MezkhDJaoYdo5fblnRQlhE3iajo+qw7N8g06GLFpzEc3WBE7XNOxovMWyGyRoOb0BJsjOxqpN5W51cMb6PDNzXITBbMGpWagA+M3G36PVVrz8AZJvEiCp9/Zg7sMSFAsa65euFJvKPOhHnbdtFl+BAruGqLZEhWtWoNNhi8fVK9hpX+h4Hck76frGNzFoMEUTGZR5k6l3j62CeYKhhAddU2ydEckFp2Nso1q+7o+1CsVDGOwfjHLhFmpzgbKgfFoVLlM91XezcB+dUtdD0zJ9RTMXzI0UC+0rXhP8LG9oCperuC1pm2qwkbVux7MKuW9gXYhWLHMubdNvvkYDIuTuoYsVhPeOYuDJWImtSAEK+4vvZ3DwE8iYMaptZNOJdxSb+5wzxdm6a8tq3AXsu7t81QCtrEMv/eOFs0XXd04e5K/nWFSGh2rBL3XHc1Z3c/yh65nTTDfDFr3ZePadbvTTqewhInfW2/bP4/EUSL83jvWDeJtouZj7OS2dQapBjStuDX3ub33SMwl8YSpY2tHLDHa1Hf4RvJtdxPcREmBvH5PnOB1IT+XpuugJNZUXZzqjRDOxcoV13Q5jjC23k9BYpNnxG6qY1o6aFhuPhguopxOnCAvX8dywntjQDudBlUwMZ0iuyEWL+tW+iWAAvxPXa8RBBfLVYKdcRUomoEDnEQoLJqvqupaCuNdy7q6wKgG5LGKO2/31311gMVZ3TBcdj7OC813Dcuf02hqvtZK3ERPdcK18KyDx6zup9NN5/+SotKymy6OZrAEd6kY2QCnAXERr704ii11ASY3Op//OxI5/SM/Ga88ml/0EQY7sE24MDtLywy4D2G4Is8b8EA++50DsxzxRcAM1+XGht8HPGHS/uQBMTcE7oGY4QZ8eBz4JG8Rs/g55QbHlD+MCxlowAfe4WIVF37YsQA2OFTzQdz4AvcJRx6LIM1MWmdz5h6BYepk4ooW/nBnaUqxtgL4rBym/STgfzASynN2/O0TTrS2tvJycHWDA8tNlXtkNYe43lNwVWD+6sWTohZLqpTU/Tm4KjBvPRmsFzbgTa8i5LNwFR8WpbwzuE3Pj6yGlGdUPMmYJTBfNodW7D66H3/cDZe/liReE549FZcAj05ynSCzszDvStxduXLwm51GT8Ud0UxLvpHN21Nx11NCdWmnfFG47HCiobOqRuBPefkzq+R1TzvpRjznj7PaxAQPmzO3Ar9LPPHK7uUBMH0Az4+8Q1A9y0EropvAbyJqERXzhlza0Y3v0fIeUCGVO/PfXoRLo5Z04jFe85fVN910fJOWP0SnOikJfT89Uqlif1R3IucK0rLDYVytwPNv0d9TWmX442XKZWK9eXJgQr7YV0X7ZCoifiPsNBF7Gyyqr5p5w/VutIGrOE1nv+T7RIW+kmuw86P8moVyNCeJyy/HHdGwValY3TvH1T9x7RJ24TbRzk6vC1SXYtl7XBEjP5P368blWItZmvFxITvUTjJTaPH+RyiXib39Vnc4QpEyXZRmK8J8D5rodXrap9XEUlqqfwd/b3+IcrnYh5OKhM+ZOl8UxuucnjXRRE13jUX5eqGwTpylpw5q6HT4WbQjatTvF/PZCK8Wl1NkThov16vzGFcy9lZREm+uPpflFz6A/PcfZMqVWPaHf/FyFcL5snHzCdd1HMdt3EwnJZq9sIKZ//Ejaalc65giozLT3eE6zUp0Fd1+qm6FWPbXCV+9QUfdM4nT5r2RuMLTOMmvWUlMPn39aFoqlv22Q7V3BmlM8vNouSjS0HXp/H4QBK4bpsViGeU+aghlM7R7+/G0VCx79F5TssBmu1rPgOfGybhEte+jX0ELYtnbfQuyhhDY/fb3wDIhdv1xwnXLvseK8Onjd9hxTahh7zx9ZsLq7b5+kRnXxbLt7RcxbQJ9i3pGnfm0/9ravxmWi2XZFoHenTwITzMQ8R8awLzTjqCST/1+VikEmqhue/j62n8SSei7+p+f+6+vw5Yawf8TqioWiK3+59W39Cd/8id/8id/8id/0q/8D5gpjKXSLU3LAAAAAElFTkSuQmCC" class="rounded-circle" alt="..." width="50" height="60"></td>
      <td><h3 class=" font-italic">La prévention des cancers</h3></td>
      
      
      </tr>
      </table>
    </div>
    </div>
   
  </div>
  <section class=" text-left">
    <div class="container">
    
    <div class="col-md-6">
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
        <h2><i>Les premiers facteurs de risque de cancer sont le tabac et l’alcool.</i></h2>
    <p>Le tabac est directement responsable de cancers bronchiques, de la vessie ou de la vésicule biliaire. Le tabagisme actif et/ou passif est le premier risque à éradiquer pour un mode de vie équilibré. L’arrêt du tabac permet de réduire le risque de cancer. Voir tous les conseils ici.

L'alcool, et particulièrement lorsqu'il est associé au tabac majore le risque de développement des cancers de la langue, de la gorge, de l'œsophage du pharynx, du sein et du foie. En France, 7 % à 10 % des cancers seraient associés à une consommation d'alcool trop importante. Il est conseillé de limiter sa consommation d'alcool à 10 verres par semaine, sans dépasser 2 verres par jour, et avec 2 jours d’abstinence par semaine.</p>
    <h2><i>Une alimentation équilibrée et la pratique régulière d’une activité physique jouent aussi un rôle protecteur dans l’apparition des cancers</i></h2>
    <p>La prévention secondaire des cancers s'adresse à des individus qui ne sont pas malades mais qui présentent un certain risque. Il s'agit du dépistage, qui consiste à rechercher de façon systématique dans une population en bonne santé les porteurs de symptômes latents. Le dépistage vise à abaisser la mortalité liée au cancer, seul critère permettant de juger de son efficacité</p>
    <h2><i>Une alimentation équilibrée et la pratique régulière d’une activité physique jouent aussi un rôle protecteur dans l’apparition des cancers</i></h2>
    <p>Aujourd'hui, de nombreuses affections cancéreuses sont imputables à un déséquilibre alimentaire. L'influence de l'apport calorique en graisses semble de plus en plus établi dans le développement des cancers du sein, du côlon, de la prostate et de l'endomètre. L'obésité joue un rôle important dans les cancers du côlon chez l'homme et du sein chez la femme. Dans ce contexte, surveiller son poids et son alimentation devient essentiel dans le cadre de la prévention du cancer. L'alimentation se doit d'être variée et équilibrée, adaptée aux besoins métaboliques individuels.

Dans tous les cas, une activité physique adaptée à la condition de chacun s'impose. Moralement et physiquement, les bienfaits de l'activité sportive sont démontrés. Elle contribue à l'entretien et au maintien de la santé. Une demi-heure de marche chaque jour ou une heure d'activité sportive vigoureuse par semaine sont de bonnes habitudes à adopter pour limiter les risques de cancer.:</p>
<h2><i>De nombreux produits chimiques, agents physiques ou poussières biologiques</i></h2>
<p>Ils peuvent s'avérer extrêmement dangereux pour la santé de l'appareil respiratoire. Ces substances constituent un réel danger en cas d'exposition prolongée et sont souvent à l'origine de cancers professionnels ou environnementaux :</p>
    <ul>
    <li>L'amiante</li>
    <li>Le radon</li>
    <li>La pollution atmosphérique</li>
    <li>Les rayonnements selon leur type et leur qualité</li>
    <li>Le glyphosate utilisé dans les produits de jardinage</li>
    </ul>
    
    </div>