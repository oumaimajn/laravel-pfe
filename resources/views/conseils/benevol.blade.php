
@extends('layouts.master7')

@section('title')
benevol
@endsection

@section('content')


   


<div class="row featurette">
<div class="row justify-content-center mb-5">
  <div class="col-md-7">
  <div class="col-md-9 ftco-animate text-center">
          
          <h1 class="mb-3 bread"><span>
        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAMAAACahl6sAAAAdVBMVEX///8ZGhoaGhqMjIwVFhbt7e1QT09rbGywrq4ZGBgSExMrLCzRz88LDAwuLS2amZkgICDg39/X19dLS0u9vb35+fmen59BQkJbXFx+fX0wMC/l5eXHxMSTk5OPjIyDhIQnJia0tLR0cnFjZGQ5ODinp6Y/Pj52WSVGAAAMSElEQVR4nO1ci1bjOAytRdpCoLxLeQy7MAP7/5+41LGtK8mlaeqE3XN8Zw6UJLZ1raedpLNZRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRcX/CvPr8/Nz5xxt/x/QJOB6XOkOwNwBejYh55kT9W4yAdYdBa+QvkQ6CodwnwKODtZIpP4fI3K4aXUs6L9GhKJUvZ2dojn2tsYpQMFt+3vu3IFtDRv0hm6HNfwGDry9Xwv2kaEamV87V5wJm8lhRA5qojq43k5BaSYQgQ4hEv1kwIjz606bhZlA9D3YRwaZ1vo5th/E5OX99SSH15Ch/RSv+0lyeMTG1ud+ND/m/ODGJ2/BObsYRSmAAih2DKVUHtjQt7jd3AVs4jWb5Wl2Wjoegcj1gUyuIO6j3JzWwq+u3xxLjXiy85ELPMbtlhkqH9gvHcZkQaJ7lyxJKyQQiYzjDzMB8DfNExG8LH7+1LJsYg/Bng9hEtuqkaxcxBpht+EW5DL0XSQiuSaFPVlZZH/9mSyw/++shnKm5duw+ImjISLnJTpk+6J4hM5gnvoy+QR7dupDFE5qZL6rAcHf6SNohMwMtRdSmEcmzGrvyUQYdJyoJB56TtSIqNJ5honVgUbGRBT9r1/tqZbm0ZLtqZPXJLH2eCIzdiKSGw4QGybuF3wG0L5aeR7FZWFa+jA5j5NIKv5kBTUaSRoQnQAXl4ta3UXN75xAGwiJyWn3M3nhfrXkRIYLJSLyFFskmW4CEXuCHvMibdSFfnae99UU7yyJEEyzIDD4ucNGJtc4tkn0EeTrz26EIGBlHIIhiJ7vYXKCc5oPunjUS7XOULfN4iVegFsizVjO8RPy2uA8Rdfdw+SJ7VZkPsOmE+wtlFLJEVBw4SMJ2+tFt7kqavl1BCxtI3oIre+/JbI018tPFKVzHFAzVFHSXEQz/iET4WOzNWZmsj6XIXQrxvc8kIgI/1Z4ppD+Bq1Q9logKA1LJsLHpjvKUWx9rpPYw/c8vojIAoNkUtRuw7ajSDh5yCiB4AepRLjVR3cRF17rc9n9Ph5SIzmBrA1JreOkE0wHdoVG63mIRLgB7ifM5BLnJJM5c0TScJyJiAdGiVEFKK3SkZ4CEVBcsxQ8RN888+s3bvhk5M4QgbkThpSLr7nymKCdKgwU4fi3SIQp1JJmst0S6s1jtrS2YphA3SHKB3FetsOrtKWKRLhxemyOTpHJnx48oo9Io9YioRqkKmCeNQ3RX9rAUIkwlzCQCWkF7iPCQQqrE2L710Yn3SqeMNOAHzOJUPBIrTk0bzfq+vEIRGLlizYuM0O8iBUhTAZilqEiLK+94bH9/hXOWgRvZ72oimwPEe2rBGsk+UmKqSUnWNdJF4m/MBHGfTienbRoZia9t+iWsisYNHHCmKIIZde2ihx0TJgIeT/RWuSh21mzfEJUbKzumSiRpsBqFIe6BPJXVh9yCmgYkyULcjY2cEXI+hCGkOqj636bszmNLA6dg2Owfs5aItj0voWUxs8QmT/nwzb+Pj+sS84jExLxKdtYlKL192F9cmafjkh3P0poQBSg/uPeul1hmQLsZES4GIQiSDrI4TwgIU5FJPDQOhFmtXc9aLFMm8YTEWG7Qh4CNIBH0AhN5uwv1zFTKHMiqI8G8IASZRIiNy0XknLvCCq0ITxyRH6dGnQbjPb4r9Dk1p56yY1200JlKKrjtHqgffs+u4gkK01E7hqNla9ZL1bmxF1ocmLO2BsGkQeXJdLR41/DeHiNdFOUiIg7WP5nV3xftMoI2K9OHLfpzjYZIl92lVmhiLJzqD5SHkHTWnCvAYIIgWcykbiCiUJliHR2lYtTsD0xlEfaRaGcRiwRUZtrjeBJS+Sm1dfEWYEFz2AeaamLpmUmLRIRFZEmgresMxq5b2W30jECo+E8eF8ro5EUHEEjcnw0LW5CGY3ct4JE/s7YMQ/RLFMcRI2IO85xy/kCZenOZ5zdZTUieBhVhL+Oehgosx5ZxAlLHpgzLaURUIf/KYnctxknJ1ANHctjBxEci3YRIUVE3IkQRNg/snT8r+bIh7OWLt7y6xW11OhAhCD4kvQRb1eQfaKjEZN37a/ZcchFLSEvGSJg1cpHYN6ByKn0D8rppTkiXiERFCoRYX/MOHuGCFYeYFpXbcyhkg4md9x/HE7EmJZeL+yMWkzEbG8xkTOMtqmGF5QK8IB9LdQIKQO2tVbOtFJRK0xrZQxJZUFXgkeujP9zqfHmI8rtmzkR71y82ibR6O/9jU56awyFgnY1m2DN/nDmxf37QRY4XGSV4QG33kYi8hRd5q8mu+V9XYZHDL80mkY+/ACrr09PDVhVpDJg3z2PZYqYIxE58zHrcvvRM+E0sv1UjMfoPrJeeYPq7sv+aVQgLscj3NUdz7S6oBWD8WODK6uSPHxCHNO0rpKLeDyicZXkkXZRxtqg++07v0x/p+ccnHtOPEoQWrpYkCYi7wuD9+3xmztzPD6S8GTO3IWo2gUteHRhETXC99tPD7yDsItI0EkicmUfs77aHvclSqxcZImyYIsRVY0PWlBBvp98xK6Zx33Lj9IcQSQth4AIxPtOPiYiHxJAIhKByHqVXOT06aNtUhphHl/rtSJEYultiCBQI/hOhiLCC6ZI5N2Lfnn6+3LVoI6Zx3aXqAwRPbvfEJH1PYTshdhdASKvncIb7EzY1ct2dooRkQnxSm/6gEYwCzg0LTA41AgXDrwuaFabxKN7m6qoRoRppfVPcF/0EUzNYFqKfCDyAWrY/mxW18sHjrbr7u5VMR+RK8QrdHSlETAr5SNqGRiIwKKSmlaQmPFTAwU1ooig2xrTSuIaZ4fp74jMV4F009LjgynY42OLozm7TCOkNAKLiWzUAtN698+Ptk2GxCxleSpDxCQ3iFppdW5MS2mRicR7qx2R16Y9W1xdZMadpafOaExnd+wIigjYlrPhV2vkYReJma8f4wCT5xFlPkojwiD1O0cGXVYq6yOdBQkfUfkNTQuZiDwiDK8nkWk0wr6wVyOaRx8i3E05ImJhdWHf1/VSvdjj8XXIT3sqe3vaENl6IpUpGjvLHm2puxO4JV5UIxM+5uRR3rTM3u80QI8rlxDH23zYjRXXmYU0ErxkeiIuzuFI4ffT7iT4+2LfPFSzc/PhjzmRHhFfcZlQLmqJXZQTVTTG/bVvHqpZmNdhQx65dBppi2vFI5Rbs4vwexKeGKG0tgpE0rM9iabaRQEAEbFAJiCS+hlpPSLv/pNDIljDg18t1BkkAsuB7T8mQmleRvKRE1yJoGntvhm6cKoNCSKpPCQniRQ1LUuEKZAmop4hEUtdh+3QR4TRZUyr0L5WlEs7O4xvNBKml/TCipftWSLkss5eigjJrZ2dT8hctMHMYwwgoxG+4btLI8LZo7kVI+L6EkGJspsPFOaFNRL3keMaeLzwG6dMmBa4pw6/2aWuS2oKEHmEz1DGR0ourLRGQhqRDyhetGDt3fT2CL8YsbbQGikZflX1y1ErZn2RRxC5fS2x1L2UDSgbtUqWKE6XKOLRM+0jaTvebj7EK4CIDOXj5REK3x4gnT2qnTKmxSZv1+zxeWpZazGXkUsU0kTkU0ioEVEa7t18+LZoLEsk3YHiZ+PtTsJtR8S8xxbb7Nx8+LzSSG+mr9hGR4paE2HFWhopj0yERGS0DbqJsOLYUFIjk76+5xFrrZIa+SHTSsG82C7KzxDhjFss/P6cj3Q6KUQk5OKfIELFw+907yEmFM4jXOr+82434EbEe+FN7Cuucs3227jgfNjdRzoS96EzWEY5GKL7TfZYBsSJwVyoekjVcDfosW8qeMjx5Muau4V2csXCr1IYQqqb3JdcDPlKYIuFmnAzjpBLfaULOScOyBfG4sXpNR+rdI+eX+GyB/eOYEYlAfygNhfwWFZz9mrRKR44+t2RDhuHpkF6PHhJEk1OvjNsvGK3ksOlMFDf76LZhxszUSQNSH7K6IVyV1K2B5eZln33f3vjNI2Gr41QrLG1FHCQQDb5Gq4Kg2TJpgO5t5MH4kF9501yzviTzc68YKSDkohl4Psk9ANtBr28vgv3WrjdkEYHtmacS4SLuGGsmQ9/yTiP9VIMamRPv3WWISGubh3FRvlF77nvxD4S89e7zOSLgdNGdFCGlQ4343bphru5ey35bLxkMyHG4lBRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRMRH+BZQogFZ4hBM8AAAAAElFTkSuQmCC"  height="100" alt="..."> 
        </span>La journée mondiale du bénévolat (5 décembre) met en lumière l'engagement bénévole partout dans le monde.</h1>
                <div class="icon"></div>
          <p class="breadcrumbs"><span class="mr-2"><a href="index.html">un proche d'une personne</a></span> <span>malade</span></p>
          
        </div>
    <p class="lead">Rejoignez les 13 800 bénévoles de notre mouvement associatif qui donnent chaque jour de leur temps, de leur énergie, et de leurs compétences pour faire reculer le cancer sur tous les territoires. 1 000 missions vous attendent.

L'activité bénévole repose sur un statut de « bénévolat » et s'articule autour d'un engagement moral envers un projet associatif et des objectifs fixés par le Comité départemental de la Ligue contre le cancer. Par définition non rémunérée, cette action est cependant riche en relations humaines gratifiantes. Inscrites dans une activité coordonnée, ces missions bénévoles sont définies avec des objectifs, des moyens et des règles. Les bénévoles sont défrayés de leurs frais liés à la mission (sur justificatifs) et sont couverts par une assurance  pour leur activité dans l'association.

</p>
<h6>Quelles sont les fonctions accessibles aux bénévoles ?</h6>
<ul>
<li>La recherche : saisie des dossiers de demandes,  tri, classement,</li>
<li>Les actions auprès des personnes malades : visites à domicile, points d'accueil, groupes de parole</li>
<li>La prévention : concours scolaires, conférences, contacts après des DRH d'entreprise, services sociaux</li>
<li>L'information : relations avec les médecins généralistes, les unités hospitalières, les unités du secteur privé, les médias,</li>
<li>Organisation et administration : secrétariat, contacts téléphoniques, comptabilité, informatique, collectes de fonds</li>
</ul>
<h6>Quel temps donner?</h6>
<p>Aucune durée n'est imposée à un bénévole. Certains donneront deux heures par an, d'autres plusieurs jours par semaine. Dans tous les cas, le temps consacré à la Ligue contre le cancer doit faire l'objet d'un accord entre le bénévole et l'équipe avec laquelle il va travailler, et être compatible avec la mission confiée.</p>
 <h6>Pour rejoindre nos équipes, remplissez le formulaire suivante:</h6>
 
  </div>
  
  </div>
</div>






<hr class="featurette-divider">

<section class="hero-wrap js-fullheight" style="background-image: url('images/bg_1.jpg');" data-stellar-background-ratio="0.5">
<div class="row featurette">
  <div class="col-md-7 order-md-2">
    <h2 class="featurette-heading">Quelles sont les fonctions accessibles aux bénévoles ?</h2>
    <p class="lead">Trois types de bénévolat existent au sein du Comité Départemental : Le bénévolat Administratif, le Bénévolat évènementiel prévention et le Bénévolat soutien aux personnes malades.</p>
  <ul>
  <li>Le bénévole Administratif assiste le service dans sa gestion quotidienne. Réception du courrier donateur, gestion des aisies, mise à jour des contacts</li>
  <li>Le bénévole évènementiel prend part à l'élaboration et participe aux manifestations culturelles, sportives, médicales...</li>
  <li>Le bénévolat soutien aux personnes malades se déroule dans les Centres de soin comme actuellement le Centre Antoine Lacassagne, la clinique Saint George et l’Hôpital l’Archet II. Les  bénévoles visitent les malades en chambre pour leur apporter un soutien moral et les informer des différents services proposés pour eux par la Ligue contre le Cancer. A ce sujet, des bénévoles peuvent également animer des ateliers au sein de l’Espace-Ligue, s’ils ont des compétences particulières (ex : les soins esthétiques, la réflexologie..) </li>
  </ul>
  <h6>Le parcours du bénévole</h6>
  <p>Pour devenir bénévole, un premier rendez-vous doit être pris avec la Déléguée à l’Action pour les malades, suivi d’un autre rendez-vous avec la Psychologue du Comité. Ces entretiens déterminent si les motivations et les qualités de la personne désireuse de devenir bénévole coïncident avec les besoins du comité.</p>
  </div>
  
  <div class="col-md-5">
    <img src="https://remeng.rosselcdn.net/sites/default/files/dpistyles_v2/ena_16_9_extra_big/2019/10/26/node_104243/11398396/public/2019/10/26/B9721390432Z.1_20191026181543_000%2BG26EPE4NU.1-0.jpg?itok=gIuG5Ci41572106908" class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" width="500" height="500"></img>
  </div>
  
</div>
</section>
@endsection