<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>conseil</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/blog/">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <!-- Bootstrap core CSS -->
<link href="/docs/4.5/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/4.5/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/4.5/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/4.5/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon.ico">
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
<meta name="msapplication-config" content="/docs/4.5/assets/img/favicons/browserconfig.xml">
<meta name="theme-color" content="#563d7c">
<style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
     <!-- Custom styles for this template -->
     <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="blog.css" rel="stylesheet">
  </head>
  <body>
  
     <div class="text-center bg-light ">
    <div class="col-md-6 px-0 text-center">
   
       
      <table>
      <tr>
      <td><img src="https://cdn.iconscout.com/icon/premium/png-512-thumb/pet-medicament-2024286-1714494.png" class="rounded-circle" alt="..." width="50" height="60"></td>
      <td><h3 class=" font-italic">Quelques conseils pour pallier les effets secondaires du traitement du cancer</h3></td>
      
      
      </tr>
      </table>
    </div>
   
  </div>
  <section class=" text-left">
    <div class="container">
    
    <div class="col-md-6">
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
        <h2><i>Le traitement du cancer peut provoquer une chute de cheveux</i></h2>
    <p>Afin de limiter la chute de cheveux pendant le traitement du cancer, faites une coupe de cheveux plus courte et moins épaisse qu'à l'habitude pour permettre au casque réfrigérant de mieux refroidir le cuir chevelu ; évitez les permanentes, les décolorations et teintures, le sèche-cheveux. Utilisez des shampooings doux.</p>
    <h2><i>Les nausées et vomissements causés par le traitement du cancer</i></h2>
    <ul>
    <li>Prenez les médicaments antinauséeux et anti-émétiques prescrits par votre médecin</li>
    <li>Evitez de boire pendant les repas, mais beaucoup avant et après</li>
    <li>Buvez 1 à 1,5 litre d'eau, de bouillon, tout au long de la journée pour ne pas vous déshydrater</li>
    <li>Vous pouvez boire des boissons gazeuses qui peuvent améliorer vos nausées</li>
    <li>Buvez des boissons froides ou glacées et choisissez des plats froids ou tièdes (moins d'odeurs)</li>
    <li>Mangez souvent, en petites quantités, lentement et en mastiquant bien</li>
    <li>Reposez vous après les repas</li>
    </ul>
    <h2><i>Le traitement du cancer peut entraîner un mauvais goût dans la bouche</i></h2>
    <p>Outre les nausées et les vomissements, le goût est souvent perturbé par la chimiothérapie : nausées et mauvais goût peuvent conduire à une modification des habitudes alimentaires. Parlez-en à votre médecin.

Quelques conseils :</p>
    <ul>
    <li>Sucez des bonbons mentholés ou rincez-vous la bouche au citron</li>
    <li>Lavez-vous les dents après chaque repas</li>
    <li>Faire des exercices de relaxation avant et pendant le traitement peut être efficace</li>
    <li>Emportez avec vous votre activité préférée lors du traitement, pour moins y penser</li>
    <li>Surveillez votre poids</li>
    </ul>
    <h2><i>Les aphtes dus au traitement du cancer</i></h2>
    <ul>
    <li>Brossez-vous les dents avec une brosse souple, avant et après chaque repas, au moins trois fois par jour</li>
    <li>Utilisez des bains de bouche qui sont prescrits par le médecin</li>
    <li>Supprimez ou réduisez tabac et alcool, surtout dans les semaines qui suivent le traitement</li>
    <li>Evitez les aliments trop épicés ou acides (jus de citron, vinaigrette, moutarde)</li>
    <li>Si vous avez un dentier, enlevez-le pour la nuit, avant votre dernier bain de bouche</li>
    </ul>
    </div>