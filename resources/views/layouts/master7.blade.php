<!DOCTYPE html>
<html lang="fr">
<head>
<title>Site: Solution Cancer</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Prata&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{asset('css/aos.css')}}">

    <link rel="stylesheet" href="{{asset('css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{asset('css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/jquery.timepicker.css')}}">

    
    <link rel="stylesheet" href="{{asset('css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('css/icomoon.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

	
	
</head>
<body>

    
<!-- Example single danger button -->
<!-- Image and text -->


<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light ftco-navbar-light-2" id="ftco-navbar">
	    <div class="container">
	      

<div class="btn-group">
  <button type="button"   class="btn dropdown-toggle btn-lg border-0 rounded-0 "
   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  style="width:180px;">

    <p style="color:pink;">Je Suis..</p><p style="color:black;">une/un</p>
  </button>
  

<!-- 1er ligne -->
  <div class="dropdown-menu">
    <table >
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="{{route('premiers.index')}}">
    <img src="https://fr.seaicons.com/wp-content/uploads/2015/10/Person-Male-Light-icon.png" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="400" >une personne atteinte d'un cancer</td>	
	</tr>
</table>
<!-- 2emme ligne -->
<div class="dropdown-divider"></div>
<table>
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="#">
    <img src="https://www.icone-png.com/png/48/48142.png" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="400" >un proche d'une personne malade</td>	
	</tr>
</table>
<!-- 3emmme ligne -->
<div class="dropdown-divider"></div>
<table>
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="#">
    <img src="https://fr.seaicons.com/wp-content/uploads/2015/06/female-user-info-icon.png" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="400" >un bénévole</td>	
	</tr>
</table>
<!-- 4eme ligne -->
<div class="dropdown-divider"></div>
<table>
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="#">
    <img src="https://image.flaticon.com/icons/png/512/1154/1154993.png" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="400" >un chercheur</td>	
	</tr>
</table>
<!-- 5 -->
<div class="dropdown-divider"></div>
<table>
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="#">
    <img src="https://img.icons8.com/cotton/2x/small-business.png" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="400" >une entreprise et donateur</td>	
	</tr>
</table>
<!-- 6 -->

    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="#">toujours: ecouter , aider , soutenir , parle ....</a>
  </div>

</div>


<!-- 2emme action -->

<div class="btn-group">
  <button type="button"   class="btn dropdown-toggle btn-lg border-0 rounded-0 "   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 180px;">
    <p style="color:pink;">Je M'informe</p><p style="color:black;">sur le cancer</p>
  </button>


  <div class="dropdown-menu">
    <table >
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="#">
    <img src="https://www.etoilespassion.com/wp-content/uploads/2018/09/ICONE-RECHERCHER.png" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="400" >Le cancer, Définition et chiffres</td>	
	</tr>
</table>
<!-- 2emme ligne -->
<div class="dropdown-divider"></div>
<table>
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="#">
    <img src="https://cdn.iconscout.com/icon/premium/png-512-thumb/pet-medicament-2024286-1714494.png" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="400">Traitement & Conseil</td>	
	</tr>
</table>
<!-- 3emmme ligne -->
<div class="dropdown-divider"></div>
<table>
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="#">
    <img src="https://www.leachagency.com/wp-content/uploads/2017/08/Health-Icon.png" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="400">Prévention & Dépistage</td>	
	</tr>
</table>
<!-- 4eme ligne -->
<div class="dropdown-divider"></div>
<table>
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="#">
    <img src="https://fr.seaicons.com/wp-content/uploads/2015/08/laboratory-icon.png" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="400">Les essais cliniques & comité de patients</td>	
	</tr>
</table>
<!-- 5 -->
<div class="dropdown-divider"></div>
<table>
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="#">
    <img src="https://pngimage.net/wp-content/uploads/2018/06/image-livre-png-9.png" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="400">Brochures d'information</td>	
	</tr>
</table>
<!-- 6 -->
<div class="dropdown-divider"></div>
<table>
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="#">
    <img src="https://www.bellicon.com/_Resources/Persistent/4e07ce81453dcd52f968bb106908bb3fa61ebf40/le-plancher-pelvien-et-le-bellicon.png" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="400">Témoignage</td>	
	</tr>
</table>
<!-- 7 -->

    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="#">toujours: ecouter , aider , soutenir , parle.... </a>
  </div>

</div>

<!-- 3eme action -->
<div class="btn-group">
  <button type="button"   class="btn dropdown-toggle btn-lg border-0 rounded-0 "   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  style="width: 180px;">
    <p style="color:pink;">Je Trouve</p><p style="color:black;">de L'aide</p>
  </button>

<!-- 1eme -->
  <div class="dropdown-menu">
    <table >
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="#">
    <img src="https://img.icons8.com/plasticine/2x/service.png" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="400">L'offre de services de la ligue</td>	
	</tr>
</table>
<!-- 2emme ligne -->
<div class="dropdown-divider"></div>
<table>
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="#">
    <img src="https://i.pinimg.com/originals/61/9d/13/619d13147cd5de7c813be306b9d87981.png" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="400">La ligue prés de chez moi</td>	
	</tr>
</table>
<!-- 3emmme ligne -->
<div class="dropdown-divider"></div>
<table>
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="#">
    <img src="https://toppng.com/uploads/preview/telephone-rotary-dial-mobile-phone-icon-telephone-ico-11563192097gmwbskqmfp.png" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="400">Service Télephonique</td>	
	</tr>
</table>
<!-- 4eme ligne -->
<div class="dropdown-divider"></div>
<table>
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="http://laravel-tp1.test/forums">
    <img src="https://www.voice-global.org/media/3809825/transparent-kisspng-internet-forum-discussion-group-portable-network-g-jornal-o-dia-de-guarulhos-dizisports-com-5be743b8affc062244768215418828087208.png" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="400">Le Forum de discussion</td>	
	</tr>
</table>
<!-- 5 -->
<div class="dropdown-divider"></div>
<table>
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="#">
    <img src="https://cdn.pixabay.com/photo/2016/03/08/10/14/icon-1243679_960_720.png" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="400">Le Soutien psychologique et l'écoute</td>	
	</tr>
</table>
<!-- 6 -->
<div class="dropdown-divider"></div>
<table>
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="#">
    <img src="https://cdn3.iconfinder.com/data/icons/finance-68/32/Finance_money_Dollar_management_cash_savings_salary-512.png" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="400">Les aides Financiéres</td>	
	</tr>
</table>
<!-- 7 -->

    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="#">toujours: ecouter , aider , soutenir , parle.... </a>
  </div>

</div>

<!-- 4eme action -->

<div class="btn-group">
  <button type="button"   class="btn dropdown-toggle btn-lg border-0 rounded-0 "   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  style="width: 180px;">
    <p style="color:pink;">Je Soutiens</p><p style="color:black;">la ligue</p>
  </button>


  <div class="dropdown-menu">
    <table >
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="#">
    <img src="https://cdn1.iconfinder.com/data/icons/banking-filled-line-1/614/1550_-_Donation-512.png" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="400" >Faire un Don</td>	
	</tr>
</table>
<!-- 2emme ligne -->
<div class="dropdown-divider"></div>
<table>
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="#">
    <img src="https://www.freeiconspng.com/uploads/financial-icon-png-don-8.png" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="400" >Organiser une collecte</td>	
	</tr>
</table>
<!-- 3emmme ligne -->
<div class="dropdown-divider"></div>
<table>
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="#">
    <img src="https://i7.pngflow.com/pngimage/699/368/png-community-volunteering-organization-business-voluntary-association-icon-friendship-community-volunteering-organization-business-clipart.png" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="400" >Devenir bénévole et solidaire</td>	
	</tr>
</table>
<!-- 4eme ligne -->
<div class="dropdown-divider"></div>
<table>
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="#">
    <img src="https://impact.canada.ca/sites/default/files/sm-handshake_0.png" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="400" >Devenir une entreprise partenaire</td>	
	</tr>
</table>
<!-- 5 -->
<div class="dropdown-divider"></div>
<table>
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="#">
    <img src="https://w0.pngwave.com/png/748/537/delivery-courier-ludhiana-information-icon-png-clip-art.png" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="400" >Courir contre le cancer</td>	
	</tr>
</table>
<!-- 6 -->
<div class="dropdown-divider"></div>
<table>
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="#">
    <img src="https://image.flaticon.com/icons/png/128/942/942781.png" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="400" >Faire du mécénat de compétences</td>	
	</tr>
</table>
<!-- 7 -->

    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="#">toujours: ecouter , aider , soutenir , parler.. </a>
  </div>

</div>

<!-- action -->

<div class="btn-group">
  <button type="button"   class="btn dropdown-toggle btn-lg border-0 rounded-0 "   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 180px;">
    <p style="color:pink;">Je Découvre</p><p style="color:black;">la ligue</p>
  </button>


  <div class="dropdown-menu">
    <table >
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="#">
    <img src="https://lh3.googleusercontent.com/proxy/3vNH7GxmosIlt-31weK6pQCPr05A_tXP29lP9SmCoRjyDlItYQg8Fd2BjZw8ypy3Ata847EuZ3PoXS7x-LrB_QCV3URh1iJbE49SxCn8ZsCyhSVLkVYwo0t1sFxUMDI" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="300" >Actualités</td>	
	</tr>
</table>
<!-- 2emme ligne -->
<div class="dropdown-divider"></div>
<table>
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="#">
    <img src="https://tabletopfarmer.com/wp-content/uploads/2017/07/DIY-potting-soil-icon-339x325.png" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="300" >Notre Soutien à la recherche</td>	
	</tr>
</table>
<!-- 3emmme ligne -->
<div class="dropdown-divider"></div>
<table>
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="#">
    <img src="https://img.icons8.com/carbon-copy/2x/domain.png" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="300" >Nos comptes</td>	
	</tr>
</table>
<!-- 4eme ligne -->
<div class="dropdown-divider"></div>
<table>
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="#">
    <img src="https://floracontigo.com/wp-content/uploads/2016/05/Icone-Evenement.png" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="300" >Evénements à venir</td>	
	</tr>
</table>
<!-- 5 -->
<div class="dropdown-divider"></div>
<table>
	<tr>
	  <td height="28" width="67"><a class="bg-primary  dropdown-item rounded-circle " href="#">
    <img src="https://img2.freepng.fr/20180421/uew/kisspng-computer-icons-check-mark-royalty-free-true-or-false-5adb4ad84b3f16.6675153715243209843082.jpg" class="rounded-circle" alt="..." width="50" height="60"></a>
		<td height="28" width="300" >Nos Action</td>	
	</tr>
</table>
<!-- 6 -->

    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="#">toujours: ecouter , aider , soutenir , parler </a>
  </div>

</div>

</div>
<a class="navbar-brand" href="#"><span ></span><p style="color:white;">Je Su</p></a>
</nav>

<!-- fin nav -->




	





@yield('content')

@include('includes._footer')

<script src="{{asset('js/jquery.min.js')}}"></script>
  <script src="{{asset('js/jquery-migrate-3.0.1.min.js')}}"></script>
  <script src="{{asset('js/popper.min.js')}}"></script>
  <script src="{{asset('js/bootstrap.min.js')}}"></script>
  <script src="{{asset('js/jquery.easing.1.3.js')}}"></script>
  <script src="{{asset('js/jquery.waypoints.min.js')}}"></script>
  <script src="{{asset('js/jquery.stellar.min.js')}}"></script>
  <script src="{{asset('js/owl.carousel.min.js')}}"></script>
  <script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
  <script src="{{asset('js/aos.js')}}"></script>
  <script src="{{asset('js/jquery.animateNumber.min.js')}}"></script>
  <script src="{{asset('js/bootstrap-datepicker.js')}}"></script>
  <script src="{{asset('js/jquery.timepicker.min.js')}}"></script>
  <script src="{{asset('js/scrollax.min.js')}}"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="{{asset('js/google-map.js')}}"></script>
  <script src="{{asset('js/main.js')}}"></script>
</body>
</html>