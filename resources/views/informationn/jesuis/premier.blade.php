@extends('layouts.master7')

@section('title')
create Forum
@endsection

@section('content')

<section class="hero-wrap hero-wrap-2" style="background-image: url('images/z1.jpg');" data-stellar-background-ratio="0.5">
<div class="overlay"></div>
      <div class="container">
      
      <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
          
            <h1 class="mb-3 bread"><span>
				  <img src="https://media.gettyimages.com/vectors/person-icon-vector-with-question-mark-glyph-pictogram-symbol-vector-id815915464"  height="100" alt="..."> 
				  </span>je suis</h1>
                  <div class="icon"></div>
            <p class="breadcrumbs"><span class="mr-2"><a href="index.html">une personne</a></span> <span>atteinte d'un cancer</span></p>
            
          </div>
        </div>
      </div>
    </section>
    <section class="hero-wrap js-fullheight" style="background-image: url('images/z3.jpg');" data-stellar-background-ratio="0.5">
    	<div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-12 heading-section text-center ftco-animate">
          	<h1 class="mb-1">Je suis atteint d'un cancer</h1>
            <h5 class="mb-1">Etre à vos côtés à chaque étape de la maladie et vous accompagner dans cette épreuve est notre mission. Nous sommes là pour vous aider</h5>
          </div>
        </div>
    <p class="ftco-animate"></p>
	          <ul class="mt-5 do-list">
	          	<li class="ftco-animate"><a href="http://laravel-tp1.test/chiffre#"><span class="ion-ios-checkmark-circle mr-3"></span><h style="color:white;">M'informer sur mon cancer</h></a></li>
	          	<li class="ftco-animate"><a href="{{route('forums.index')}}"><span class="ion-ios-checkmark-circle mr-3"></span><h style="color:white;">Rejoindre notre forum de discussion</h></a></li>
	          	<li class="ftco-animate"><a href="#"><span class="ion-ios-checkmark-circle mr-3"></span><h style="color:white;">Comprendre les essais cliniques</h></a></li>
	          	<li class="ftco-animate"><a href="#"><span class="ion-ios-checkmark-circle mr-3"></span><h style="color:white;">Découvrir les traitements et conseils adaptés</h></a></li>
	          	<li class="ftco-animate"><a href="#"><span class="ion-ios-checkmark-circle mr-3"></span><h style="color:white;">être conseillé pour emprunter (aidea)</h></a></li>
                  <li class="ftco-animate"><a href="#"><span class="ion-ios-checkmark-circle mr-3"></span><h style="color:white;">Contacter notre service d'écoute psychologique</h></a></li>
              </ul>
              </div>
    </section>     
    
    <section class="ftco-section">  
    <div class="row">
			
            <div class="col-lg-3 d-flex">
                <div class="coach align-items-stretch">
                    <div class="img" style="background-image: url(images/m_1.jpg);"></div>
                    <div class="text bg-white p-4 ftco-animate">
                        <span class="subheading">cancer</span>
                        <h3><a href="#">Elizabeth Nelson</a></h3>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                        <ul class="ftco-social-media d-flex mt-4">
                <li class="ftco-animate"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-instagram"></span></a></li>
              </ul>
                        <p></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 d-flex">
                <div class="coach align-items-stretch">
                    <div class="img" style="background-image: url(images/trainer-2.jpg);"></div>
                    <div class="text bg-white p-4 ftco-animate">
                        <span class="subheading">Owner / Head Coach</span>
                        <h3><a href="#">Scarlett Torres</a></h3>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                        <ul class="ftco-social-media d-flex mt-4">
                <li class="ftco-animate"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-instagram"></span></a></li>
              </ul>
                        <p></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 d-flex">
                <div class="coach align-items-stretch">
                    <div class="img" style="background-image: url(images/trainer-3.jpg);"></div>
                    <div class="text bg-white p-4 ftco-animate">
                        <span class="subheading">Owner / Head Coach</span>
                        <h3><a href="#">Victoria Wright</a></h3>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                        <ul class="ftco-social-media d-flex mt-4">
                <li class="ftco-animate"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-instagram"></span></a></li>
              </ul>
                        <p></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 d-flex">
                <div class="coach align-items-stretch">
                    <div class="img" style="background-image: url(images/trainer-4.jpg);"></div>
                    <div class="text bg-white p-4 ftco-animate">
                        <span class="subheading">Owner / Head Coach</span>
                        <h3><a href="#">Stella Perry</a></h3>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                        <ul class="ftco-social-media d-flex mt-4">
                <li class="ftco-animate"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-instagram"></span></a></li>
              </ul>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
    </section>   
	
@endsection