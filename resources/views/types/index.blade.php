@extends('layouts.app')

@section('content')
<section class="hero-wrap js-fullheight" style="background-image: url('images/z1.jpg');" data-stellar-background-ratio="0.5">


<div class="container" >
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Patient / Medecin / Administration</div>

                <div class="card-body">
                <table  width="380" height="47">
	<tr>
		<td height="47" width="127"><div class="card" style="width: 18rem;">
  <img src="..." class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title">Card with stretched link</h5>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    <a href="http://laravel-tp1.test/home" class="btn btn-primary stretched-link">Patient</a>
  </div>
</div></td>
		<td height="47" width="111"><div class="card" style="width: 18rem;">
  <img src="..." class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title">Card with stretched link</h5>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    <a href="http://laravel-tp1.test/medecin/pmedecin" class="btn btn-primary stretched-link">Medecin</a>
  </div>
</div></td>
		<td height="47" width="120"><div class="card" style="width: 18rem;">
  <img src="..." class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title">Card with stretched link</h5>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    <a href="http://laravel-tp1.test/admin/medecins" class="btn btn-primary stretched-link">Administration</a>
  </div>
</div></td>
	</tr>
</table>
                   
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection
