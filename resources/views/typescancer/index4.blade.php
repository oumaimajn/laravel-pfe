<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>les type de cancer</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/album/">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <!-- Bootstrap core CSS -->
<link href="/docs/4.5/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/4.5/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/4.5/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/4.5/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon.ico">
<meta name="msapplication-config" content="/docs/4.5/assets/img/favicons/browserconfig.xml">
<meta name="theme-color" content="#563d7c">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="album.css" rel="stylesheet">
  </head>
  <body>
    <header>
 
</header>

<main role="main">

  <section class="jumbotron text-center">
    <div class="container">
      <h1> cancer du thyroide</h1>
     
    </div>
  </section>
  <section class=" text-left">
    <div class="container">
      <h5>Définition:</h5>
      <p>Vous venez d'apprendre que vous avez un ou plusieurs nodules et peut-être un cancer de la thyroïde. Cette page, conçu pour vous et pour vos proches, rassemble des informations qui doivent vous permettre de mieux comprendre les examens utilisés pour le diagnostic initial, la nature des traitements qui sont mis en oeuvre en cas de cancer de la thyroïde, de même que les examens effectués lors de vos bilans de santé après traitement. Ces examens permettront de réagir efficacement en cas de récidive.

Le cancer de la thyroïde n'est pas très fréquent. Alors que l'on compte chaque année, en France, environ 40 000 cancers du sein et à peu près autant de cancers de la prostate, le nombre de cancers de la thyroïde est inférieur à 4 000.

Des progrès importants ont été accomplis, tant dans les techniques de diagnostic, beaucoup plus sensibles et plus précises qu'auparavant, que dans les techniques thérapeutiques. Il en est de même pour le suivi après le traitement initial.

Le traitement implique l'ablation de la thyroïde par une intervention chirurgicale souvent complétée par l'administration d'iode 131. Cette ablation impose par la suite de prendre tous les jours un médicament qui remplace la production naturelle des hormones thyroïdiennes.

Le taux de guérison dépasse 90 % et les récidives sont peu fréquentes.
      </p>
      <div class="jumbotron p-4 p-md-5 text-dark rounded bg-pink">
    <div class="col-md-10 px-0">
      <h6 class="display-4 font-italic">Mieux connaître le cancer de la thyroïde</h6> 
     <p>-Les trois organes concernés dans le cancer de la thyroïde
Pour comprendre comment fonctionne la thyroïde, il faut en fait décrire trois organes distincts qui travaillent en étroite coordination : la thyroïde, l'hypophyse et l'hypothalamus.

La thyroïde est située à la base du cou, sous le larynx et en avant de la trachée. Elle est formée par deux lobes, reliés par une partie plus fine, appelée isthme. Chez l'adulte son poids est de 15 à 20 grammes.

La thyroïde est une glande endocrine, ce qui signifie qu'elle secrète des substances particulières, des hormones. Les hormones thyroïdiennes interviennent chez l'embryon et le jeune enfant dans le développement du système nerveux et dans la croissance. À tous les âges de la vie, elles contribuent au bon fonctionnement de nombreux organes.

L'hypophyse est également une glande endocrine, située dans une petite cavité osseuse de la base du crâne. Elle secrète différentes hormones dont une agit directement sur le fonctionnement de la thyroïde.

L'hypothalamus, enfin, est la région du cerveau, proche de l'hypophyse, qui commande le fonctionnement de cette glande.

Dans un organisme en bonne santé, ces trois organes, thyroïde, hypophyse et hypothalamus fonctionnent harmonieusement pour que la production hormonale de la thyroïde soit parfaitement adaptée.</p>
     <p>-Les quatre hormones de base dans le fonctionnement de la thyroïde
Quatre hormones interviennent dans le fonctionnement de la thyroïde. Pour simplifier, nous les désignerons dans ce livret par leur sigle : T4, T3, TRH et TSH.

Les hormones T4 et T3 (leurs noms complets sont thyroxine pour T4 et triiodothyronine pour T3) sont produites par la thyroïde. Pour former ces hormones, qui sont riches en iode, la thyroïde concentre l'iode présent dans le sang et l'incorpore dans une protéine, la thyroglobuline. C'est dans cette protéine que les hormones T4 et T3 sont fabriquées. L'iode étant nécessaire à leur formation, notre alimentation doit apporter chaque jour plus d'un dixième de milligramme d'iode.

L'hormone TRH, produite par l'hypothalamus, agit sur l'hypophyse, qui secrète à son tour la TSH, dont le rôle est fondamental : cette hormone, dont le nom complet est thyréostimuline, règle le taux de sécrétion des hormones thyroïdiennes.</p>
     <p>-Un réglage subtil pour un bon fonctionnement de la thyroïde
La TSH agit de la façon suivante :

 si la production des hormones thyroïdiennes (T4 et T3) est insuffisante, il y en a peu dans le sang : l'hypothalamus et l'hypophyse réagissent alors et l'hypophyse secrète davantage de TSH, dont le taux dans le sang sera élevé ce qui va stimuler la thyroïde pour qu'elle produise une plus grande quantité d'hormones thyroïdiennes ;
 si la production de T4 et T3 est trop forte, il y en a trop dans le sang : l'hypothalamus et l'hypophyse vont réagir pour freiner la production de TSH, dont le taux dans le sang sera bas ce qui va ralentir la formation des hormones thyroïdiennes.</p>
     <p>Les dysfonctionnements de la thyroïde
Un mauvais fonctionnement de la thyroïde entraîne des troubles très variés, étant donné les multiples fonctions sur lesquelles agissent les hormones thyroïdiennes. Les anomalies courantes dans la production des hormones thyroïdiennes sont :

 l'hypothyroïdie, si la production d'hormones thyroïdiennes est insuffisante ; elle se traduit par une sorte de mise au ralenti de certaines fonctions : perte de mémoire, humeur dépressive, frilosité, constipation, ralentissement du rythme cardiaque ;
 l'hyperthyroïdie, si la production d'hormones thyroïdiennes est excessive ; elle provoque des phénomènes inverses : nervosité, agressivité, sueurs, diarrhées, palpitations et accélération du rythme cardiaque.
Les anomalies dans les dimensions de la thyroïde sont :

 le goitre, augmentation parfois très prononcée du volume de la thyroïde ;
 le nodule, augmentation localisée du volume thyroïdien sous forme de petites boules plus fermes.</p>
    
     </div>
        <div class="col-auto d-none d-lg-block">
        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRWyIfG0f2Nq411tcCo8kEXt_mhTFvf5t1DRsCyEO8itubUImPf&usqp=CAU">
    </div>
  