
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>les type de cancer</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/album/">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <!-- Bootstrap core CSS -->
<link href="/docs/4.5/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/4.5/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/4.5/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/4.5/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon.ico">
<meta name="msapplication-config" content="/docs/4.5/assets/img/favicons/browserconfig.xml">
<meta name="theme-color" content="#563d7c">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="album.css" rel="stylesheet">
  </head>
  <body>
    <header>
 
  
    </div>
  </div>
</header>

<main role="main">

  <section class="jumbotron text-center">
    
    <div class="text-center">
    
      <h1>Cancer du poumon</h1>
     
    </div>
  </section>
  <section class=" text-left">
    <div class="container">
      <h5>Définition:</h5>
      <p>Le cancer du poumon, appelé aussi cancer bronchique, est une des maladies des cellules des bronches ou, plus rarement, des cellules qui tapissent les alvéoles pulmonaires. Il se développe à partir d’une cellule initialement normale qui se transforme, sous l’effet d’agressions comme le tabac, et se multiplie de façon anarchique. Les informations proposées dans ce module décrivent les situations et les techniques les plus couramment rencontrées mais n’ont pas valeur d’avis médical. Ces informations sont destinées à faciliter vos échanges avec les différents soignants. Ce sont vos interlocuteurs privilégiés ; n’hésitez pas à leur poser des questions.
      </p>
      <div class="jumbotron p-4 p-md-5 text-dark rounded bg-pink">
    <div class="col-md-10 px-0">
      <h6 class="display-4 font-italic">LE CANCER DU POUMON EN CHIFFRES</h6> 
     <p>-37 000 nouveaux cas annuels (73% d'hommes, 27% de femmes)</p>
     <p>-4e rang des cancers en terme de fréquence (10% de l'ensemble des nouveaux cas de cancer)</p>
     <p>-28 700 décès annuels (73% d'hommes, 27% de femmes)</p>
     <p>-4% (garçons) et 5% (filles) des 12-14 ans fument</p>
     <p>-23% (garçons) et 24% (filles) des 15-19 ans fument</p>
    </div>
  </section>
  <section class=" text-left">
    <div class="container">
    &nbsp<h5>Les poumons</h5>
    <div class="col-md-6">
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
          
          <p class="mb-auto">Situés au niveau de la poitrine, les poumons assurent les échanges gazeux de l'organisme. Ils sont séparés par la région du médiastin qui contient le cœur, la trachée, l'œsophage et des ganglions lymphatiques.

Le poumon droit comporte trois lobes contre deux pour le poumon gauche. Lors de l’inspiration, l’air arrive par la trachée et se répartit dans les bronches, les bronchioles et les alvéoles. L’oxygène contenu dans l’air inspiré traverse les alvéoles pour passer dans le sang. Le sang distribue ensuite l’oxygène à toutes les cellules de l’organisme. Lors de l’expiration, le sang ramène dans les poumons le gaz carbonique rejeté par toutes les cellules du corps. Il traverse la paroi des alvéoles et passe dans les bronches. Il est ensuite rejeté par la trachée, le nez et la bouche.</p>
          </div>
        <div class="col-auto d-none d-lg-block">
        <img src="https://www.ligue-cancer.net/sites/default/files/images/localisation/img_poumons1.gif" height="225" width="225">