
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>les type de cancer</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/album/">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <!-- Bootstrap core CSS -->
<link href="/docs/4.5/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/4.5/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/4.5/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/4.5/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon.ico">
<meta name="msapplication-config" content="/docs/4.5/assets/img/favicons/browserconfig.xml">
<meta name="theme-color" content="#563d7c">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="album.css" rel="stylesheet">
  </head>
  <body>
    <header>
 
  
</header>

<main role="main">

  <section class="jumbotron text-center">
    <div class="container">
      <h1>Cancer Leucémie</h1>
     
    </div>
  </section>
  <section class=" text-left">
    <div class="container">
      <h5>Définition:</h5>
      <p>Les leucémies sont des cancers des cellules qui, dans la moelle osseuse, produisent les globules blancs du sang.

Les cellules du sang (globules rouges ou blancs et plaquettes) ont une origine commune : la cellule souche qui se trouve dans la moelle osseuse, donnant deux lignées : la lignée lymphoïde, à l’origine d’un certain type de globules blancs, les lymphocytes, la lignée myéloïde à l’origine d’autres globules blancs, les polynucléaires et les monocytes, mais aussi des globules rouges et des plaquettes. Selon la lignée concernée, il existe des leucémies lymphoïdes ou myéloïdes, chroniques ou aiguës.
      </p>
      <div class="jumbotron p-4 p-md-5 text-dark rounded bg-pink">
    <div class="col-md-10 px-0">
      <h6 class="display-4 font-italic">Types de cancers</h6> 
     <p>La leucémie lymphoïde chronique (LLC) se caractérise par une accumulation de lymphocytes dans le sang. Elle survient le plus souvent après l’âge de 60 ans. Son pronostic est en général bon. La découverte se fait soit lors d’un examen sanguin de routine (la numération formule sanguine ou NFS), soit devant des signes variables tels que fatigue, ganglions, gros foie, grosse rate. Des examens précisent le degré d’extension. Quand il n’y a pas de symptômes, une simple surveillance peut suffire. Dans les formes sévères, une chimiothérapie adaptée à la maladie et au malade est administrée. Pendant la chimiothérapie, les défenses immunitaires et les autres familles de globules sanguins  peuvent être fragilisées et une surveillance rigoureuse est donc nécessaire. Mais dans la plupart des cas, une LLC reste longtemps compatible avec une vie normale.

La leucémie myéloïde chronique (LMC) est une prolifération de polynucléaires marqués par une anomalie chromosomique, le chromosome de Philadelphie. C’est comme pour la LLC sur la NFS, associée à l’étude des chromosomes (caryotype) à la recherche de cette anomalie, que se fait le diagnostic. La maladie évolue toujours, mais plus ou moins vite, vers l’aggravation avec fatigue, fièvre… Tous les malades avec une LMC doivent donc être traités.

La leucémie aiguë lymphoblastique (LAL) se développe à partir de la famille des lymphocytes, avec des formes « jeunes » (les blastes) de cellules anormales. La LAL atteint l’enfant dans 80 % des cas, provoquant fatigue, fièvre, amaigrissement et tendance aux saignements. Une NFS et une ponction de moelle osseuse sont indispensables au diagnostic. Le traitement consiste en une chimiothérapie adaptée, en quatre phases sur 18 mois. Le taux de guérison dépasse 80 %. Les LAL de l’adulte (20% des cas) ont un moins bon pronostic que les LAL de l’enfant.

La leucémie myéloïde aiguë (LMA) est plus rare chez l’enfant que chez l’adulte.</p>
     
     <img src="https://www.frenchweb.fr/wp-content/uploads/2018/01/leucemie-Revlimid-celgene.gif" >