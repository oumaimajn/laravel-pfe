
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>les type de cancer</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/album/">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <!-- Bootstrap core CSS -->
<link href="/docs/4.5/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/4.5/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/4.5/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/4.5/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon.ico">
<meta name="msapplication-config" content="/docs/4.5/assets/img/favicons/browserconfig.xml">
<meta name="theme-color" content="#563d7c">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="album.css" rel="stylesheet">
  </head>
  <body>
    <header>
 
  
</header>

<main role="main">

  <section class="jumbotron text-center">
    <div class="container">
      <h1> cancer du côlon</h1>
     
    </div>
  </section>
  <section class=" text-left">
    <div class="container">
      <h5>Définition:</h5>
      <p>Le cancer du côlon est une maladie des cellules qui tapissent l'intérieur du côlon ou du rectum. Il se développe à partir d'une cellule initialement normale qui se transforme et se multiplie de façon anarchique, à la suite d'une mutation. Il met du temps à se développer et peut être efficacement pris en charge grâce à un dépistage précoce. Dépisté à temps, il peut être guéri dans 9 cas sur 10.
      </p>
      <div class="jumbotron p-4 p-md-5 text-dark rounded bg-pink">
    <div class="col-md-10 px-0">
      <h6 class="display-4 font-italic">Le cancer colorectal en chiffres</h6> 
     <p>-43 336 nouveaux cas estimés en 2018 (24 035 hommes, 20 837 femmes)</p>
     <p>-</p>
     <p>-Âge médian au diagnostic : 71 ans chez l'homme, 75 ans chez la femme (chiffres 2015)</p>
     <p>-Le cancer colorectal est le 3ème cancer le plus fréquent chez l'homme, après ceux de la prostate et du poumon. Il représente 11.2% de l'ensemble des nouveaux cas de cancers masculins. Chez la femme, ce cancer est le deuxième plus fréquent après le cancer du sein (11.3% de l'ensemble des nouveaux cas de cancers féminins)</p>
     <p>-Diminution du taux de mortalité : -1.5% par an en moyenne chez l'homme et -1.1% par an en moyenne chez la femme entre 2005 et 2012</p>
     </div>
        <div class="col-auto d-none d-lg-block">
        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQwqt0t3-hfB2TMi5KjVCzEZZmcKPK6vuYrZx4x_A-5IF1BA_45&usqp=CAU">
    </div>
  
         
        