
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>les type de cancer</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/album/">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <!-- Bootstrap core CSS -->
<link href="/docs/4.5/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/4.5/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/4.5/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/4.5/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon.ico">
<meta name="msapplication-config" content="/docs/4.5/assets/img/favicons/browserconfig.xml">
<meta name="theme-color" content="#563d7c">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="album.css" rel="stylesheet">
  </head>
  <body>
    <header>
 
 
</header>

<main role="main">

  <section class="jumbotron text-center">
    <div class="container">
      <h1>Cancer du cerveau</h1>
     
    </div>
  </section>
  <section class=" text-left">
    <div class="container">
      <h5>Définition:</h5>
      <p>Une tumeur cancéreuse cérébrale est un amas de cellules cancéreuses au niveau du cerveau.

Il y a différents types de cancers du cerveau :

tumeur primaire ou secondaire (métastase) ;
selon l'origine de la cellule cancéreuse (cerveau, tronc cérébral ou méninges).
Les causes des tumeurs cancéreuses cérébrales restent aujourd'hui inconnues.

Les signes cliniques dépendent essentiellement de la zone du cerveau atteinte. Aucun n'est spécifique d'un cancer : c'est leur persistance qui doit alerter.

Le diagnostic repose sur les examens radiologiques (scanner cérébral et IRM) et sur la biopsie de la tumeur qui permet l'examen histologique des cellules tumorales.

Les traitements font appel à la chirurgie, la radiothérapie et la chimiothérapie. Le choix des traitements dépend de la nature de la tumeur et de sa localisation dans le cerveau.

Une surveillance prolongée est nécessaire pour prendre en charge les effets secondaires des traitements et pour détecter une éventuelle récidive. Dans ce cas, le traitement va dépendre du traitement initial, de la durée de la rémission et de la localisation de la récidive.

Le pronostic est essentiellement lié au type de tumeur, à sa taille et aux possibilités de traitement. L'âge et l'état de santé du patient sont également des paramètres importants à prendre en compte.
      </p>
      <div class="jumbotron p-4 p-md-5 text-dark rounded bg-pink">
    <div class="col-md-10 px-0">
      <h6 class="display-4 font-italic">Mieux connaître</h6> 
     <p>Le cerveau est contenu et protégé par la boîte crânienne. Il est constitué de deux moitiés appelées "hémisphères".

Le cerveau commande la plupart des fonctions du corps : le fonctionnement des muscles, des organes, etc. Il collecte également les informations reçues par l'extérieur avec les cinq sens (la vision, l'ouïe, le toucher, l'odorat et le goût), il les enregistre (fonction de mémoire et d'apprentissage). Le cerveau est aussi le siège des émotions.

En plus des hémisphères cérébraux, on distingue :

 le cervelet présent sous les hémisphères et à l'arrière de la boîte crânienne : il assure l'équilibre (pour se tenir debout) ;
 le tronc cérébral situé entre les hémisphères et la moelle épinière ;
 enfin la moelle épinière, contenue à l'intérieur de la colonne vertébrale, prolongement du tronc cérébral.
L'ensemble de ces différentes parties est appelé système nerveux central (par opposition au système nerveux périphérique constitué par les nerfs). Ce système nerveux central est entouré par des membranes appelées les méninges ; entre les méninges et le cerveau circule un liquide appelé liquide céphalo-rachidien.

Le cerveau est constitué de cellules nerveuses que les médecins appellent névroglies (d'où le nom de certaines tumeurs du cerveau : "les gliomes").</p>
     
     <img src="https://www.sante-sur-le-net.com/wp-content/uploads/2016/10/tumeur-cerveau.jpg" >