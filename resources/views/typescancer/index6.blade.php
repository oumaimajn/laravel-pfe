
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>les type de cancer</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/album/">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <!-- Bootstrap core CSS -->
<link href="/docs/4.5/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/4.5/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/4.5/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/4.5/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon.ico">
<meta name="msapplication-config" content="/docs/4.5/assets/img/favicons/browserconfig.xml">
<meta name="theme-color" content="#563d7c">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="album.css" rel="stylesheet">
  </head>
  <body>
    <header>
 
  
</header>

<main role="main">

  <section class="jumbotron text-center">
    <div class="container">
      <h1>Cancer de la lèvre, de la bouche et du larynx</h1>
     
    </div>
  </section>
  <section class=" text-left">
    <div class="container">
      <h5>Définition:</h5>
      <p>La bouche, le pharynx et le larynx constituent les "voies aéro-digestives supérieures" (VADS), car ce sont des conduits qui permettent le passage d'une part, de l'air et d'autre part, des aliments.

Les cancers des voies aéro-digestives supérieures (VADS) sont fréquents en France : 21 000 nouveaux cas par an.

Tabac et alcool sont responsables de 90 % des cancers des voies aéro-digestives supérieures. La suppression du tabagisme et une consommation modérée de boissons alcoolisées (moins d'1/2 litre de vin par jour) permettraient d'éviter environ 90 % des cancers des voies aéro-digestives supérieures.

Le diagnostic se fait par l'examen des biopsies obtenues lors d'un examen endoscopique.

Le traitement chirurgical est fonction de la localisation et du volume de la tumeur.

La radiothérapie complète souvent la chirurgie. Elle peut être le seul traitement local dans certains cas, lorsque la chimiothérapie a permis une disparition totale ou presque de la tumeur.

Plus le cancer de la lèvre, de la bouche ou du larynx est détecté précocement, plus les chances de guérir sont élevées.

Une surveillance est nécessaire pour dépister une éventuelle récidive de la maladie.
      </p>
      <div class="jumbotron p-4 p-md-5 text-dark rounded bg-pink">
    <div class="col-md-10 px-0">
      <h6 class="display-4 font-italic">Mieux connaitre les cancers de la lèvre, de la bouche et du larynx</h6> 
     <p>On désigne la bouche, le pharynx et le larynx sous le terme de "voies aéro-digestives supérieures" (VADS), car ce sont des conduits qui permettent le passage d'une part, de l'air et d'autre part, des aliments.

La bouche, limitée notamment par les lèvres, les arcades dentaires et le plancher buccal (sous la langue), est en communication avec le pharynx. Elle contient également des glandes salivaires, qui participent aux premières étapes de la digestion.

Au fond de la gorge, il existe un carrefour : le pharynx, où arrivent, d'une part l'air inspiré vers le larynx, et d'autre part les aliments provenant de la bouche. Il communique donc en haut avec les fosses nasales, en avant avec l'oropharynx qui contient les amygdales, le voile du palais, la base de la langue, et s'ouvre sur la cavité buccale. En bas, il se poursuit par un conduit où passent les aliments : c'est l'hypopharynx qui se continue par l'œsophage.

Le larynx est un court conduit faisant suite au pharynx en haut et continuant vers le bas par la trachée. Il contient des formations cartilagineuses et les cordes vocales dont la vibration va permettre la voix. Entre les deux cordes vocales, se situe l'épiglotte qui se présente comme une membrane cartilagineuse souple. L'épiglotte est un véritable clapet qui, lors de la déglutition alimentaire, dirigera les aliments vers l'hypopharynx et protégera ainsi le larynx, en empêchant les fausses routes. Le larynx a donc une fonction respiratoire puisqu'il permet le passage de l'air, et une fonction phonatoire, puisqu'il permet celui de la parole grâce aux cordes vocales.</p>
     
     <img src="https://www.e-cancer.fr/var/inca/storage/images/media/joomla/images/stories/schema_cav_bucc/508668-1-fre-FR/schema_cav_bucc.jpg" >