
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>les type de cancer</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/album/">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <!-- Bootstrap core CSS -->
<link href="/docs/4.5/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/4.5/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/4.5/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/4.5/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon.ico">
<meta name="msapplication-config" content="/docs/4.5/assets/img/favicons/browserconfig.xml">
<meta name="theme-color" content="#563d7c">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="album.css" rel="stylesheet">
  </head>
  <body>
    <header>
 
  
</header>

<main role="main">

  <section class="jumbotron text-center">
    <div class="container">
      <h1>Cancer du col de l'utérus</h1>
     
    </div>
  </section>
  <section class=" text-left">
    <div class="container">
      <h5>Définition:</h5>
      <p>L’appareil génital de la femme comprend la vulve, le vagin, le col de l’utérus qui se continue par le corps de l’utérus, enfin les ovaires. C’est au niveau du col et du corps utérins ainsi que des ovaires que surviennent les cancers les plus fréquents.

Le cancer du col de l’utérus, dont on recense encore 3 300 nouveaux cas par an en France, recule grâce aux stratégies de dépistage. La vaccination des jeunes filles devrait accroître encore ce recul.

Le cancer du corps de l’utérus ou cancer de l’endomètre, dont on dénombre plus de 5 000 nouveaux cas par an, est très différent. Il est favorisé par l’obésité et par la prise d’estrogènes sans progestérone associée.

Le cancer de l’ovaire rencense quant à lui environ 4 400 cas par an.
      </p>
      <div class="jumbotron p-4 p-md-5 text-dark rounded bg-pink">
    <div class="col-md-10 px-0">
      <h6 class="display-4 font-italic">Types de cancers</h6> 
      <h6>Le col de l’utérus est la partie de l’utérus palpable (toucher vaginal) et visible (examen au spéculum) au fond du vagin</h6>
     
     
     <p>Le cancer survenant sur cette partie de l’utérus pourrait presque toujours être prévenu grâce aux frottis cervico-vaginaux, et maintenant aussi grâce à la vaccination anti-HPV.</p>
    <h6>Le cancer du corps de l’utérus survient en général après la ménopause et se manifeste surtout par des saignements ou des pertes troubles</h6>
     <p>Le diagnostic est fait sur une biopsie. Le traitement est avant tout chirurgical (ablation de l’utérus et des ovaires), acte parfois pratiqué par les voies naturelles. Dans certains cas, une radiothérapie est décidée. Dans l’ensemble, la guérison est obtenue dans 70 à 80 % des cas.</p>
    <h6>Le cancer de l’ovaire</h6>
     <p>se développe lentement. Comme les ovaires sont situés profondément dans le bas-ventre, le diagnostic est souvent fait tardivement, d’autant qu’il n’existe pas d’examen de dépistage systématique. Les signes d’alerte sont discrets et banals : douleurs du bas-ventre, troubles des</p>
     <img src="https://www.e-cancer.fr/var/inca/storage/images/media/joomla/images/stories/cancerinfo/endometre/fig1_v3/511411-1-fre-FR/fig1_v3.jpg">