
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>les type de cancer</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/album/">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <!-- Bootstrap core CSS -->
<link href="/docs/4.5/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/4.5/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/4.5/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/4.5/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon.ico">
<meta name="msapplication-config" content="/docs/4.5/assets/img/favicons/browserconfig.xml">
<meta name="theme-color" content="#563d7c">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="album.css" rel="stylesheet">
  </head>
  <body>
    <header>
 
  
</header>

<main role="main">

  <section class="jumbotron text-center">
    <div class="container">
      <h1>Cancer des os</h1>
     
    </div>
  </section>
  <section class=" text-left">
    <div class="container">
      <h5>Définition:</h5>
      <p>Les cancers des os sont le plus souvent des métastases, les cancers qui métastasent aux os étant surtout les cancers du sein, du rein, de la prostate, de la thyroïde et du poumon.

Les os peuvent aussi être le siège de cancers primitifs, les plus fréquents étant les ostéosarcomes (ou sarcomes ostéogéniques). Ils surviennent surtout chez les enfants ou les adultes jeunes, peuvent toucher n'importe quel os mais dans un cas sur deux sont situés à proximité du genou.

Le diagnostic est généralement assuré par les radiographies standard ou par la scintigraphie osseuse.

Le pronostic des ostéosarcomes s'est considérablement amélioré au cours des dernières années et la guérison est souvent possible sans devoir recourir à l'amputation.

Le traitement repose avant tout sur la chimiothérapie mais aussi sur la radiothérapie et l'hormonothérapie.
      </p>
      <div class="jumbotron p-4 p-md-5 text-dark rounded bg-pink">
    <div class="col-md-10 px-0">
      <h6 class="display-4 font-italic">Mieux connaitre</h6> 
     <p>Tous les os peuvent être le siège d'une tumeur maligne, notamment s'il s'agit de tumeurs secondaires (métastases) d'un cancer, par exemple du poumon, du sein, du rein, de la thyroïde ou de la prostate.

Les cancers primitifs des os sont peu fréquents. En revanche, l'os est souvent le siège de métastases de certains cancers, ceux qui ont le plus tendance à métastaser vers les os étant les cancers du sein, du rein, du poumon, de la thyroïde ou de la prostate. Les métastases peuvent toucher tous les os, y compris ceux du crâne. En revanche, les localisations sur les extrêmités des membres sont rares.</p>
     
     <img src="https://sante.lefigaro.fr/sites/default/files/img/2012/02/28/cancer-os.jpg" >