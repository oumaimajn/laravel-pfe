
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>les type de cancer</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/album/">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <!-- Bootstrap core CSS -->
<link href="/docs/4.5/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/4.5/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/4.5/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/4.5/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon.ico">
<meta name="msapplication-config" content="/docs/4.5/assets/img/favicons/browserconfig.xml">
<meta name="theme-color" content="#563d7c">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="album.css" rel="stylesheet">
  </head>
  <body>
    <header>
 
 
</header>

<main role="main">

  <section class="jumbotron text-center">
    <div class="container">
      <h1>Cancer du prostat</h1>
     
    </div>
  </section>
  <section class=" text-left">
    <div class="container">
      <h5>Définition:</h5>
      <p>Dans la majorité des cas, une évolution lente n'a que peu d'incidences sur la vie de l'homme concerné. Toutefois, certaines formes de cancer de la prostate ou certains traitements peuvent avoir un impact majeur sur la vie des hommes malheureusement touchés par ces formes les plus graves. C'est en tenant compte de tous les paramètres prédictifs, pronostics, cliniques ou évolutifs qu'il convient d'envisager la prise en charge la mieux adaptée.
      </p>
      <div class="jumbotron p-4 p-md-5 text-dark rounded bg-pink">
    <div class="col-md-10 px-0">
      <h6 class="display-4 font-italic">Le cancer de la prostate en chiffres</h6> 
     <p>-71 000 nouveaux cas annuels</p>
     <p>-1er rang des cancers en termes de fréquence chez l'homme (34% de l'ensemble des nouveaux cas de cancer) ;</p>
     <p>-8 700 décès annuels (10 % des décès liés à un cancer chez l'homme)</p>
     <p>-69 % des cancers de la prostate surviennent après 65 ans</p>
     <p>-Âge moyen au diagnostic : 71 ans</p>
    </div>
  </section>
  <section class=" text-left">
    <div class="container">
    <h5>La prostate</h5>
    <div class="col-md-6">
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
          
          <p class="mb-auto">La prostate est une glande de la taille d'une noix, située sous la vessie, à la base du pénis et près du rectum. Abritée au sein d'une capsule, elle entoure le canal qui transporte l'urine et le sperme jusqu'au bout du pénis (urètre).
 
 Sous la dépendance de la testostérone sécrétée par les testicules, la prostate a pour principales fonctions de sécréter une composante de l'éjaculat (le liquide prostatique) et de se contracter pour permettre l'éjaculation.
 La prostate se divise en 3 zones :
 
  une zone périphérique, proche du rectum. Cette proximité la rend facile à palper au cours d'un toucher rectal. Elle constitue la plus grande zone de la prostate. La majorité des tumeurs (environ 75 %) surviennent dans cette zone périphérique.
  une zone de transition, située au milieu de la prostate. Elle entoure l'urètre et représente environ 5 % de la prostate jusqu'à l'âge de 40 ans. Avec le vieillissement, cette zone grossit pour devenir la partie la plus importante de la prostate. C'est ce qu'on appelle un adénome prostatique (ou hypertrophie bénigne de la prostate) qui survient très fréquemment chez les hommes de plus de 70 ans.
  une zone centrale, située à la base et entourant les canaux éjaculateurs. Elle représente 20% de la prostate.</p>
          </div>
        <div class="col-auto d-none d-lg-block">
        <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUSExMWFRUXGBUWFxcYGR4YGhoYFh0YGRcVGxcYHiggGB0lHRUXITEhJSkrLi4uGB81ODMtNygtLisBCgoKDg0OGhAQGzImICYtLS0wLS01Ly8wLTU1Ky0vLS0wLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAK8BIAMBIgACEQEDEQH/xAAbAAACAgMBAAAAAAAAAAAAAAAABQMEAQIGB//EAEcQAAEDAgMEBQYKCQQCAwAAAAEAAhEDIQQSMQVBUWETInGBkTKTobHB0gYUFUJSU2JjctEWI0NUgpKi4fAzstPxg8IkNKP/xAAaAQEAAwEBAQAAAAAAAAAAAAAAAwQFAgEG/8QALxEAAgIBAwIEBAYDAQAAAAAAAAECEQMEEiExURQiQYETMmHwQlJxkbHRI2KhBf/aAAwDAQACEQMRAD8A9ixOPLC9pFwGln2sxyx3GPFRDbLJeD82IiTNw13YA5wVyvhmPe1zvKZdt/ZvVanhaJBcCQHTvLfnAmAdLgaIDYbUbJsT5MACTcEn1LXEbVaGy0EmXNANrsMOWPiNEdUSOYcRpIPWBtqVs7Z1GZ4yQMxi9yQJ3wgMO21SvqYMaa3I14SFepVA5ocNCAR3qjRwdFzZbMO6w6xGhmQJtdZNc0wGUqD6jAAAWupx2dd4MoBgkW0qeWoftAOHqKuHaFX90q/zUv8AkVDauJqlod8VqjLr16Wh1/adhUWaO6BJjlUjn/hPSlgd9Ewew/3hLNh1M2ekfndZv4mgyO9vqTzFve9rmHD1Lgjy6Xvrhm4qtTqSKL8zTHlU7Ob/ABLMad2bWHblwyxP+Gel7Hxuenc9Ztjz4FXOmC47D7VqAis3Dvyvs5ofT8re3y7cU7GNqkAjDvIOh6Slf+tWoO0ZUVVxl1Q2NYLIqBKRi6v7s/zlL31Trbdq3DcM9xGpNSllHMnPHpXvJ63FD01ihtYjmuXdtTEnWmR+B7D6Rb0rHyjX+jU/mZ7y92y7HvmfSDOqNZYDjHNcuNpYjc13e5sesqfC7Wrl2RzaYdEgF7esN5BkabxrovHGVdBbXWLXsMNo7MFQ52jK8GeU8UoBc1xBHW3t0vxbz5Jq3apbapTczcSLj/OyVJjcOys2WOBI0cLxyPJV8mOM+DmMlfl90KxVBsJnnYrVwkQVC5xghwIcw37tVZc1ZmSGx0ScUnEMJinsda53jQOA3jgVeNawDLg+SeG8tI4hUHMkX7iNRzWtCoQdYuA7gDud6fAqWE9ypkjXxV9S++kfK32gTOm4k8ZNgpG1YLTud/2Pahjp7QYI4FQVXQHN3th47Jn81JiyOSlCX3RHjSpxLuHMMyfRkTxjT0QujwdYPY1w3gLmsL5MzMku8TI9EJzsAxSy8CfA3WhpJc0+xxnXFjJCEK+VQQhCAEIQgBCEIBVi9kl9Rzw4NzCDa+kf5Ch+QurBLfJqjSRL2saDfTyPSnaq47EFmR1sucNfyDrAj+ItnlKAXv2HLwZblDg7LFvmyI03LWpsNxygOaA0mLaS9zxHcQI5KWltm8FpMmRlGjSYbN7zBPeFkbcblByO1giW2tMm9uEHggI27DMglwNo32sW25GfWruysGaTS0kGTIgaWA79FXG2WggQ4yToBYZsoMTe/BT46niCR0NSi1sXD6bnme1tRtu5AXlq9sgg7xCVdDjvrsN5ip/zo6HHfXYbzFT/AJ0AvewtJadWmPyPeIK5T4UYXLUFQaP1/EPzHtXS7Ww2MDw7pcMcw+oqfN/8/P0JVjsDiqrCx1TDQeFGpII0P+sfUsvNjSk1Zp6XO4SUv3EmyscGEtd5DrO4g7ndy6OjVqUvJGembjv3iNFwjqdcEtc6kCDB6j5kf+RM9l7SxVLq56JYTaadR0cwBVB7lHC10Zb1um+J/kjw/b+zp8VjnuEEZG/ON5jgOJOkKCXEANYSAbACQO06OPNRVKeKqiRUwxcLtHRVBNjxrWkFVqGJxtMnLUoc2OpVLd3TWVqO9RuFOX1KWGDVySuX1HVPZlV0ZiGjxIUx2J9s+AVfCVsZUEivhZ3joKlv/wB7qhX2btQ46hV+M0vizQekpsYWA6/Nc5xcTa82jxzJ6nO5NSmlX6/0V56rOpU3Q3GxY0qHwVXG7CqPaAHjMDma6IyuGh58CN4JXQIVZa/OvxEXjc3qzlcJWqAGWZXMOWo1psDrMb2kXBjRS07S9kNPgOMOHDmE12jhiCK1MS9ohzR+0ZvYeY1HPtSXD1WuJdT8gk5RG7eI3b7LW0mpeo4kvv76FzDNahNSXK9TGMqdI+cpBiKg9BKC97YBg3A5nmrbq+VhY4fNif4bKtQbJk/NAHe4ST4R4qpqUvUQxqEHu5Kvwj2g/D4d9anSNZzYhjdTJibXgclUwO1H1GBz8NVY57GksgSCZ4unxTwmFWe6Xg8AZ9ir4pLpXueYfnMnGPIYRSrDM0B1m3IFo62sSO4LduJgj9RWvLTIbed/lK+ylNEDflB79QfFGbM0ERJAI7dYViU3CSZzKKUtxXwGPd0bf1FawymzdW9U/O5JlsbaTg6OgrQW8G7jr5fNQbOeHMLhoX1CO8lX8CcrqRvrlt9oH2gK3CSjkjR5lVov/Kjv3ev/ACt99Xqh6pPIrdC1Cmc1hcZVYM7i4/qswDnZg4ki4gdWLk8irtParjlByCc1yZDoIADY3305JvCMo4IBFgtsOytDi0uLqQ1uc7i11uS3o7WqEtkMg5NJmHkt9idZRwRCAyhCEALStSa9pa4S0iCDvC3QgIPibJaco6oAHYNAtPk6lEZB6eEeEblaQgK/xGnY5RYyPGfWrCEIAQhCAX7ab1AZ0cPTb2pOn+0KWam4b4kdouufBVDVR8yZawPy0c/8I9mSelaOTx6nJt8HcM1mHFSBmcCSYvG4T2K0RNjoq1D9VTdSGnWNPsJks7QSe6E0bSk77EuqyTnijDsy5h6LXUwCNb8xzncVVxWHuGvv9CoNfwnn6Cr2FHUb2BbVaYc0tIsf8ntWlLCpQXczI6iUMjf1EFWi+mcw3XDh7RqPUmWC2oD1Xw08dx/JZpy2WuuRv4jcVTxGFFy0a3LefEcDyWPmxRyvZlVPuaz2Z4+b9x8hczTqvZZj45Hlugq/Q2wcpzM6wiwOsmO5Zub/AM/Lj5XK+hUyaHJHmPKLm08V0bDHlGzfzXFbTx7ME04ioCaNjUaNQ8mGEfidqOU8U4xFYu67+sdwbfuHALIwzKjcr2h7HQKsgOaJ3Fptr1fHitTS6fw2Pn5mW4Ylhx1+JkWxsU3ENZVE5KozsJ1LSCJLd15Hcs4SxLTvAPe3qH1BXMIGsaXwGsaDA0EDSOAiAAqWDObr7oyg8by4+Kp6lqg/kdk9UmPaqrXQ0gggwdd533V0qvjAIHf6iquOVOjnFOpUOKVmidwHqVOhhpDTa9/J4+1Sl00mje4NaO/+y2qWAa3U2HLmrOSck0onGSKdIzhxFIAACc0Rpdxg+1XwQMp4OZ4SB7VXa0DK0aD1Af8AS3qeSeyfC/sUzdSVnMl0R0yFhpWVtFQEIQgBCEIAQhCAp7TxhpNBGW7g2XuytE7yYKhwu2GOBzWLQXEtl7cuZzQ4OAvJYYGqu18O1+WfmkOHaFHWwLHFxM9drWHsYXFscDLz6EBG3alMkNGaTuyOkRxEWUVHbLC1hIc0vEgFp5wJiJstzspkgkuJDs0ki7pBk2toNIWlPYtMEGXWiJI+bMDSYubIDfDbWpuyi4c6IBBsTcAmIkhXiVSobKY1wcJkRrF4sJtNvYjHbIoViHVKYcQIBM6IC7mHFGYcUq/RrCfUN9P5o/RrCfUN9P5oBrmC53EUcrnN3TI7DdXf0awn1DfT+aUbY+D2FD2xQZdp47iOfNV9TFOFslwvzE0KLE05aRCWv2LhQY6BpPAST60H4P0SP/rUx2k/mqkMUpcxTLfIwwtR1NoBlzANY6zY4/SHZftV9jwRIIIO9c/R2Jh4g4dh0Np/NYqbHwv1TWHmCBfjeFfx58kVU067lPLpNzuI6x1IkZm+U24HEb29/rhVWPkBw0IBCoO2XQDSRhabzuyuIB8TZc5g8PRpvLa1AAEk3nqzwv5Kr6yeOVNPkn0ODL5lXT74Op2g9l+lIbTax1R7jYQ0gXduGpKrYSnRqt6Sg4PY6m8gtJLTlLSPVCr434O4V7P9FpaR1gJ6zTrF9RqOxMPg9sylh206NAZaYa8NvOpBJJOpKbl4WTj6EmRzimTGoGidx0AVYU3ZmlxIa90OaLXglltbQfFZwmH+dJOmUHc0eTpvhb16nzhcU5d/FBDWjvMnuV3O08TcuxIjXaNRpGSwY3yuE7gq+GtIAOXUSIMkkkRwusUGTc3iw/Fvd2yp3lfMZZ26Ockl8qMkqpiQXOyt4R3u/stqtXdN413AcSrWz8MGNzusBJE8N7jzKYsduxjjXmZZNnAbmM75NtOwHxW+Fpm7nanTk3cPzSzZYNWrUrHyDDWjjkmCRyk955J01WYx81nU4bZUwp+UeUD2n2LeqDldGuV0dsWWuH8kH6RLvE29EKUmx7Cj6laT5HuF8hv4W+pSqPDCGNH2R6lItxFVghCF6AQhCAEIQgBYc4ASdBdZUeIZLXAakEeIQENDaFJ4Ja8GBmPIcexWA8RM2ie5KXbJd0QAeS8BjTJtDYloMWH5LRmyqgvmuOiiXEwGl2cacHAc4QDprgRIWUo2fgKjKgcSCMsHrE3gaA6XCu4vD1HEFlYsHANa7v6yAtIS74lX/eT5tn5I+JV/3k+bZ+SAYOMXK5faGKNSocttBJ+a3WY+kdw71PtulWayDiXQZn9WzQbtOMBcVtnYGIqVcPVGNextJ2eo0ADNcE+TE2tebLnbvdPoixhjXmo6fpQwWtxJuTzJVb5RE2zHsFlSxFcuuRxLW7mjiVXBcTEFx4ASeUgWapJZa4iX1j7jNmNi/WFyPaCYnipaeOnQg98pQ9z2hxcwsgB3WBAtYnMO0LXpZgwD6fAi/rXEc1eg2J9B7LDfLDuLbFa4nDNeIcA8f1DnG/uSqjW4OI/rG/UajwVynizwDvwlezjiyrk8UZJ3EWVXVcMQWEPpHSd3Ixp2+hb4fbgD2wxw6wMWgTYw76JBNuKYVXtdILXQ4XFtREEX1/Jc2KZzlhGkmN8NuDbjYdpWZkxzxNpcp8fqW4xhli/iLn+R/szGCr1CcpaAC0aui05uHIXV/HtApEDQRYcAQuVpYRraLqlR+SsHENZmG6L27T4K1gtvloioM7Yjn/dcZsmSXE2R+HvnFzTGOGfYjQgn0mQVis7hcmwVCnjWF/VJ7DYkcO0epT4moAZJgZbH8R18As54/OV3hayU11L+zsKD1jcA2kakfPPsS3bO0jVcKNLQmJ+keA+yFptHa+ZvR0xDIjmfyC3+DOzzPTOneGjdwlW10pFuOP4a+LP2Q9wFAsptYYkAAxpO9T1JymOEeKyiJIHee7T0rpFCUrbbJmN0HAR4LXEA5Xdh8TYDxK3aFIxmZzGay4E9jesT6AO9cwjukkVrHrRYLKELcK4IQhACEIQAhCEAIQhACEIQAhCEAIQhAc3t581I3AtHgMx9ngk20HWjiQO7UjwCZ48k1XfiqegMCVY43HaT4Ar3F8jf1Zo4Y/KipkLnNAN3mx4AWHpDj3BPKFIMblAgeknieJSjAf6ot5NP/wBQfW4ppTcTry9O6OVws3UTdpEsm2ZrmRpy8bKhW2ax3Xp/q3G9h1T2t9oV9+np8FC18COEj02UCm4co8X0FDwQclRsO8QRxBUjW6CZA0DhmI7HTmHir9VrXjK6DyPrB3FVamCeDLDnH0TAI7HGAe+Fcx6iE/m6k6mnxM1FJ30iO+f9wK0rUnTYAWEuklxI0JkRbcIgLU4oNOVxyu+i6x9OvaFmti2jynAd8f8AasbYkmyL9f8ApBVoDIW7oIngBcD/ADfJSdryW34CPb/hVrGbQzS1oOXedCeQB3c1Xp0nfNY48MoJ9MKjqckZOolvFBRV9ER2teD7e5W6laafIZe2JM+k6obgqv1dT+VQ1/gbUxbmVRXqUTRJGTL5U9b+G51gqqopvng41OSMUprmmb4bDmo9rAYzbxuG8+C7ai0NAaBYCABwXLbE2NUa9+etVY5th5AN9dWXGicnAn97rk8BkJ8AxdJLuU9Xm3ypdENJ3lbUBvOp9W4JbT2TUN3YmuBuE057+pA7Lqf5Mf8AveI8af8Axrx10sz5y9EM2q9sqjrUIuRA/CPzPsSTC7FqVTPxnEBm8yzrHgOpoOKaDY1T98r/ANHuK7pcNedlaTHCEsw+y3tcHHFVnAfNdkg8jDEzKvHBWZjWFzmzGTUmw8VKMQ03Dhv38LH1pXU2HLbPOY5s5MkHMZPVm1wFtW2OSXFrwM2eRlmA7IYEEXmn6UAzZVaZAIMawdFuqGz9nGk5xzAg7gI3kzqeO6FfQAhCEAIQhACEIQAhCEAIQtarw0Fx0AJ8EBy20DGIc2Nc5/2E+tKNoHrN3eV6lNjMYOmbmPWvmtYdLeCdBENsq20neMPjwTFK8X33NTEqcbNtnf6jm/YaP9oTJo6z+3/PWluzD+uceTPU1Mc13ni8+JgBZWbma/QXz7G5VdjQTB0JE99j6Qr5wFTizsv6/wCypVGkOIIyn26g89DdcSg0uQpJ9BlUY1o0twifBV3YcEw2WHgbgqao6Wh3Np9IUmljdQ2yspNC+tT3PaCL6jMI9irN2ZQ3U2dycFy1c0HUA9wXW5+pJHO0UKWDY3yabR2BTAR/dTCi36I9KyKbfohNweayBpJ8kTz3KxTbAj08VssLluyOU7MOaDqAe0SstAGgA7BCFkBDizVz4V3BbPztDqmhuGaCPtcVrgcH0kOPkA6fSI4/ZHpTpX9Np/xy9jiUvRGAIWUIV8iBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAUGOpl1NzRqQYU6EfIR57WpZnVT9r0FrYVStQe8spb5LQeR39wBPcn222No1y50Bjxck24jlqSO8LldrbexFHEUDhsIa1ImKlR0iGuIBy3tYakEWhQOUljaj16Gq8y+Hfr6DegQMRUA0AcB/DlHrCZYSnmMTcumeEQUvwFDrPPEkfxE5ndwsE52WOseQHpj8lSkv8ij2RzPhexeNERqQeM3S3HmZBs9sX4g6Ed4Wnwu+ELcBhnYl9N9QNLRlZr1t5J0HNVW/CPD1aLKuWoxz2NcA6k6RmAdlNosrDjasrRkky/hBLC2OzvuFI10gHx7d6U4LbVL7zT6t+49ilp7ZpX/ANTU/sn778FnOLvoezatjJCX/LNL7zzT/dR8s0vvPNP91Nr7EdoYIS/5Zpfeeaf7qPlml955p/uptfYWhghL/lml955p/urPyzS+880/3U2vsLRfWYsUvG2KX3nmn+6s1dtUcpvU81U91eqLsNnW4VmVjRwAUqTN+EdCP2nmn+6s/pJQ+880/wB1baIBwhJ/0kofeeaf7qP0kofeeaf7q9A4Qk/6SUPvPNP91NXNDgQdCOY15i4QG6FzbalSkP1YNzUu8ucDlPVbc2mdQpq+NqkvbMQ6xa02aHgXMz5PLssgHyEhp4+qGCIByS1uVxLjlcZmbQQNfaFittCoXFzBIa0xYwZDZJG+JPggH6FU2ZWc9kviZIkWkA2MK2gBR1a7WloOr3ZW8yGud3WYVIquPw7nZHNIzU35xOhlrmEGOTygJaeJaZg6Eg8ZGtt61bjGHKMwlzS8A2OURJg6eULFLnbHJzFzm5nNcJA0LnSYndAhRHYXk+R8+QRPlOY61vsH+YoB4HDispbh9mZaxqSIOYjj1o6vYIVrF4KnVAD2hwFx/gQE5KVu2qahy4ZoqbjUNqQ7HftDyb3kLf5Aw2+i09sn0Epi1oAgCANAEAix+wOkbmqONWqLgmzRxDGaDTUyeaRVhVzhpboADZ0ng8tjdJtxXdoUcoX0ZLjyuHByWCoNAtoAQCd5OrlJhHZXNJ0c3L3tt+SkZoO/1laVKYvax8QeIHrWRvalZY3W3YyewEQQCOBuPBVtouAa2ZgOFhew3Qqra1RosZG4xP8AdR5XvMk95sAOxWJZotCMKd2SYRxzEzFvCTMdwhSUnmJk3k+OnoXNfCXZmNq1MP8AEsQKVJhPTBws+SDpq8QIi2qdFmK+nQ82/wB5VpJPmziUuS9nPE+KM54nxVIUcV9ZQ82/31nocT9ZQ82/315RxZdDjxK2vxKo9HivraHm3+8s9HivraHm3+8lHll8OPErOc8Sl/R4r62h5t/vI6PFfW0PNv8AeSjwYZzxK0rvMand6wqXR4r62h5t/vLStRxREdLQ3fs38fxL2K5QOuCylAo4362h5t3vI6HG/W0P5He8twiG6Eo6HG/W0P5He8jocb9bQ/kd7yAbrBMXKVCjjfraP8jveTGuwuY5tiSCL6d6AxhsXTqSWPa6NcpB9SmSVuCqxcWBbLc9yBmloeIOWSDB4c1gbPqw4mSS5py9IbtFNjYzcngnn3oB0HAyARbXlvv3FZSGns6s1xcLk6nNEzTDL9jhPetvkyoYknQA9dw0ZG4/SugHiFFhWkMaHeUGgHti6lQAhCEAIQhACEIQAhCEAIVcY2mapo5x0gYKhZNwwktDo4S0juVhAc7TpOg2NnOH9RWeidwK6FCovRJvqSfEOcdh5+b6/YsDDfY8ZPrTzHY6nRaX1HZWta5xcQYDWAucSRoAASrC88D/ALHvxWIBRPBbimeBTxC98Eu5zvYjyHgUZDwKeKJmIaXOYDLmxmHDNcJ4JfmPNwoyHgUZDwKeKOjWa8S0giSJHFpII7iCE8EvzDcJ8h4FGQ8CniE8EvzDcI8h4FHRmQIOo9aeIXsdGk7sbgQhYc6ASdBdXTkyhV8HjGVQHU3ZmkNcDBgteMzSCdZBVhACEIQAhCEAIQhACEIQAhCEAIQhACEIQAhCEBxOHwOLFT464N69V7jSDXdK2i4Ckxpdny2YynULQ2zs19SeerYdwwbqtKnXZNB4rvzuBqvdVpdHkcXXdHSQ4RlDotML1chQnCsydHlGS3Vi1ri3agODxuzHEFtLDPbhi8EMqMqVAHBhDnfF21GkhxLfKIEgujeq+C2Fin0KjnMqCu3CYanSLy6WumsK4aM9nlmQE5p8nrb16UhAeZYzYuJOGr06bHlr6eLa2mKTqLZfhnsaG06lV5EvIFyBN43rtNv7O6d+Ha5pcxtRznCSG2pvyZgCMwzEWNphOUIDynG7OxNKjiKjuka4YfECqWMezO4luSazq7g929rmtGUSOqDCd0th56rWNoVWYQ1AXUnlwkim7M4tLpylxbrqRPNdxWpNe0tcA5psQbgrdAedHZFeIp0qzcRGI+M1c7gKrSHhrWuzQ4lxYWR5AaR1dFNs/BGjiOmo4avToCswuZDi4joHsLhTJJLQ8tsN943rv0IDgKOBqEh+Iw+IfLsQ6i0OOam92KrPElroYTTdSh2gDSORoVdgVm5Wik8Umvxhc006lb9bUqNdTqgMrMM5LNqSQ05tCZXpyEBxFOnimDonMrve6rg3Z4tkb0fSlzg4gEZXSJvfVXdm7IdT2Xkax7cRUoNNW5NQ1iwZiXEzmm2toXVIQHBv+DZY4vp0qgcyts5zCHOsOlpfGnAF0XZ0mfiJmUsobLxJe8mjVp9JTxHS9G1+bM5wLWuq1Kv/AMg6xDWiLCLBenoQHnOF2VUik3EYZ78Mz40G06QqMmo40TRq9C55dSsKzReGkyIzSGVUV4FLoa89OHknrNFM0ovUnrdaxHG67RCA4DY2warxh6VenU6NrcMHtJIb1cI5jmuANx0hAI0mF1fwZoPZhmMeHAtzABxJIaHHKJNzaE0QgBCEIAQhCAEIQgBCEID/2Q==">