@extends('layouts.master3')


@section('title')
  Home page
@endsection

@section('content')
  

    <section class="hero-wrap js-fullheight" style="background-image: url('images/z1.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
          <div class="col-md-10 ftco-animate text-center">
          	<div class="icon">
	          	<span>
				  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/Pink_ribbon.svg/1200px-Pink_ribbon.svg.png"  height="200" alt="..."> 
				  </span>
          	</div>
            <h1> &amp; Solution Cancer &amp;</h1>
            <div class="row justify-content-center">
	            <div class="col-md-7 mb-3">
	            	
	            </div>
	          </div>
            <p>
            	
            </p>
          </div>
        </div>
      </div>
    </section>
		
	
	   
	   <section class="ftco-section ftco-intro" style="background-image: url(images/intro.jpg);">
		   <div class="container">
			   <div class="row justify-content-end">
				   <div class="col-md-6">
					   <div class="heading-section ftco-animate">
			   
	            <h2 class="mb-4"> Journée mondiale contre le cancer 2020</h2>
				

	          </div>
	          <p class="ftco-animate"></p>
	          <ul class="mt-5 do-list">
	          	<li class="ftco-animate"><a href="#"><span class="ion-ios-checkmark-circle mr-3"></span>Journee national de cancer</a></li>
	          	<li class="ftco-animate"><a href="http://laravel-tp1.test/informations"><span class="ion-ios-checkmark-circle mr-3"></span>voir les information</a></li>
	          	<li class="ftco-animate"><a href="http://laravel-tp1.test/medecin"><span class="ion-ios-checkmark-circle mr-3"></span>voir des medecins</a></li>
	          	<li class="ftco-animate"><a href="#"><span class="ion-ios-checkmark-circle mr-3"></span>prendre rendez_vous</a></li>
	          	
	          </ul>
					</div>
				</div>
			</div>
		</section>

		<section class="ftco-section ftco-no-pt ftco-no-pb">
			<div class="container">
				<div class="row no-gutters">
					<div class="col-md-4 d-flex align-items-stretch">
						<div class="offer-deal text-center px-2 px-lg-5">
							<div class="img" style="background-image: url(images/d_1.jpg);"></div>
							<div class="text mt-4">
								<h3 class="mb-4">creer un collect</h3>
								<p class="mb-5">un collecte c'est pour un personne attiendre la maladie est besoin d'argent .</p>
								<p><a href="http://laravel-tp1.test/collecte1" class="btn btn-white px-4 py-3"> voir <span class="ion-ios-arrow-round-forward"></span></a></p>
							</div>
						</div>
					</div>
					<div class="col-md-4 d-flex align-items-stretch">
						<div class="offer-deal active text-center px-2 px-lg-5">
							<div class="img" style="background-image: url(images/d_2.jpg);"></div>
							<div class="text mt-4">
								<h3 class="mb-4">bénévoles</h3>
								<p class="mb-5">Rejoignez les 13 800 bénévoles de notre mouvement associatif qui donnent chaque jour de leur temps</p>
								<p><a href="http://laravel-tp1.test/benevol" class="btn btn-white px-4 py-3"> Voir <span class="ion-ios-arrow-round-forward"></span></a></p>
							</div>
						</div>
					</div>
					<div class="col-md-4 d-flex align-items-stretch">
						<div class="offer-deal text-center px-2 px-lg-5">
							<div class="img" style="background-image: url(images/d_3.jpg);"></div>
							<div class="text mt-4">
								<h3 class="mb-4">Médicament</h3>
								<p class="mb-5"> peuvent être pris des médicaments gratuits Avec le soutien de l'Etat tunisien.</p>
								<p><a href="#" class="btn btn-white px-4 py-3"> voir <span class="ion-ios-arrow-round-forward"></span></a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
    
    <section class="ftco-section ftco-section-services bg-light">
    	<div class="container-fluid px-md-5">
    		<div class="row">
    			<div class="col-md-6 col-lg-3">
						<div class="services text-center ftco-animate">
							<div class="icon d-flex justify-content-center align-items-center">
								<span class="flaticon-candle"></span>
							</div>
							<div class="text mt-3">
								<h3>les aides financiers</h3>
								<p>les aides financiers dans les site , ce divise a deux choix : 
								soit un done direct ou bien un donne a travers un collecte </p>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-lg-3">
						<div class="services text-center ftco-animate">
							<div class="icon d-flex justify-content-center align-items-center">
								<span class="flaticon-stone"></span>
							</div>
							<div class="text mt-3">
								<h3>le forum de duscusion</h3>
								<p>le forum de duscusion c'est un espace qui poser une question(commentaire ,réponse)</p>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-lg-3">
						<div class="services text-center ftco-animate">
							<div class="icon d-flex justify-content-center align-items-center">
								<span class="flaticon-stone"></span>
							</div>
							<div class="text mt-3">
								<h3>Traitement & conseil</h3>
								<p>Quelques conseils pour pallier les effets secondaires du traitement du cancer</p>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-lg-3">
						<div class="services text-center ftco-animate">
							<div class="icon d-flex justify-content-center align-items-center">
								<span class="flaticon-relax"></span>
							</div>
							<div class="text mt-3">
								<h3>Brochures d'information</h3>
								<p>trouve des livre qui parle de  la maladie</p>
							</div>
						</div>
					</div>
				</div>
    	</div>
    </section>

	  
    
	          


    <section class="ftco-section testimony-section">
      <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
          <div class="col-md-10 heading-section ftco-animate text-center">
            <h3 class="subheading">Solution Cancer</h3>
            <h2 class="mb-1">Type de cancer</h2>
          </div>
        </div>
        <div class="row ftco-animate">
          <div class="col-md-12">
            <div class="carousel-testimony owl-carousel">
              <div class="item">
                <div class="testimony-wrap p-4 pb-5">
                  <div class="text">
                  	<div class="line pl-5">
	                    <p class="mb-4 pb-1">Le cancer du poumon, appelé aussi cancer bronchique, est une des maladies des cellules des bronches ou, plus rarement, des cellules qui tapissent les alvéoles pulmonaires.</p>
	                    <span class="quote d-flex align-items-center justify-content-center">
	                      <i class="icon-quote-left"></i>
	                    </span>
	                  </div>
                    <div class="d-flex align-items-center">
                    	<div class="user-img" style="background-image: url(images/c_1.jpg)">
		                  </div>
		                  <div class="ml-4">
		                  	<p class="name">Cancer du poumon</p>
		                    <span class="position">Cancer</span>
		                  </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap p-4 pb-5">
                  <div class="text">
                    <div class="line pl-5">
	                    <p class="mb-4 pb-1">le cancer du sein est le plus répandu des cancers féminins. Près d'une femme sur neuf sera concernée au cours de sa vie, le risque augmentant avec l'âge, surviennent avant 40 ans.</p>
	                    <span class="quote d-flex align-items-center justify-content-center">
	                      <i class="icon-quote-left"></i>
	                    </span>
	                  </div>

                    <div class="d-flex align-items-center">
                    	<div class="user-img" style="background-image: url(images/c_2.jpg)">
		                  </div>
		                  <div class="ml-4">
		                  	<p class="name">Cancer du sein</p>
		                    <span class="position">Cancer</span>
		                  </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap p-4 pb-5">
                  <div class="text">
                    <div class="line pl-5">
	                    <p class="mb-4 pb-1">certaines formes de cancer de la prostate ou certains traitements peuvent avoir un impact majeur sur la vie des hommes malheureusement touchés par ces formes les plus graves.</p>
	                    <span class="quote d-flex align-items-center justify-content-center">
	                      <i class="icon-quote-left"></i>
	                    </span>
	                  </div>

                    <div class="d-flex align-items-center">
                    	<div class="user-img" style="background-image: url(images/c_3.jpg)">
		                  </div>
		                  <div class="ml-4">
		                  	<p class="name">Cancer de la prostate</p>
		                    <span class="position">Cancer</span>
		                  </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap p-4 pb-5">
                  <div class="text">
                    <div class="line pl-5">
	                    <p class="mb-4 pb-1">Le cancer du côlon est une maladie des cellules qui tapissent l'intérieur du côlon ou du rectum. Il se développe à partir d'une cellule initialement normale qui se transforme et se multiplie.</p>
	                    <span class="quote d-flex align-items-center justify-content-center">
	                      <i class="icon-quote-left"></i>
	                    </span>
	                  </div>

                    <div class="d-flex align-items-center">
                    	<div class="user-img" style="background-image: url(images/c_4.jpg)">
		                  </div>
		                  <div class="ml-4">
		                  	<p class="name">Cancer du côlon et du rectum</p>
		                    <span class="position">Cancer</span>
		                  </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap p-4 pb-5">
                  <div class="text">
                    <div class="line pl-5">
	                    <p class="mb-4 pb-1">Une tumeur cancéreuse cérébrale est un amas de cellules cancéreuses au niveau du cerveau.

Il y a différents types de cancers du cerveau :

tumeur primaire ou secondaire (métastase).</p>
	                    <span class="quote d-flex align-items-center justify-content-center">
	                      <i class="icon-quote-left"></i>
	                    </span>
	                  </div>

                    <div class="d-flex align-items-center">
                    	<div class="user-img" style="background-image: url(images/c_5.jpg)">
		                  </div>
		                  <div class="ml-4">
		                  	<p class="name">Cancer du cerveau</p>
		                    <span class="position">Cancer</span>
		                  </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

   

    
		

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  @endsection 
  