@extends('layouts.app')


@section('content')
 <div class="container">
 <section class="hero-wrap js-fullheight" style="background-image: url('images/z1.jpg');" data-stellar-background-ratio="0.5">
   <h1>{{ $topic->title}}</h1>
   <hr>
   <!-- envoie topics.update et retour $topic -->
   <form action="{{ route('topics.update', $topic) }}" method="post">
   <!-- pour protege le file web -->
   @csrf
   @method('PATCH')
   <div class="form-group">
   
    <label for="title">Titre du Topic</label>
    <input type="text" class="form-control @error('title') is-invalid @enderror" 
     name="title" id="title" value="{{ $topic->title }}">
    <!-- si ilya une error affiche error -->
    @error('title')
    <div class="invalid-feedback">{{ $errors->first('title') }}</div>
    @enderror
    </div>
   <div class="form-group">
   <label for="content">votre sujet</label>
   <textarea name="content" id="content" class="form-control @error('content') is-invalid @enderror"
    rows="5">{{ $topic->content }}</textarea>
   @error('content')
    <div class="invalid-feedback">{{ $errors->first('content') }}</div>
    @enderror

   </div>
   <button type="submit" class="btn btn-primary">modifier mon topic</button>
   </form>
 </div>
 </section>
@endsection