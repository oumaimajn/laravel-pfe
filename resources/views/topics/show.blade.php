@extends('layouts.app')

@section('extra-js')
<script>
  function toggleReplyComment(id)
  {
     let element = document.getElementById('replyComment-' + id);
      element.classList.toggle('d-none');
  }
</script>
@endsection

@section('content')
<section class="hero-wrap js-fullheight" style="background-image: url('images/z1.jpg');" data-stellar-background-ratio="0.5">
<div class="container">
<br><br><br>
 <div class="card">
  <div class="card-body">
   <h5 class="card-title">{{ $topic->title}}</h5>
    <p>{{ $topic->content }}</p>
    <div class="d-flex justify-content-between align-items-center">
         <Medium>Posté le {{ $topic->created_at->format('d/m/Y a H:m') }}</Medium>
         <span class="badge badge-primary"><h6>{{ $topic->user->name }}</h6></span>
    </div>
    <div class="d-flex justify-content-between align-items-center mt-3">
    <!-- can pour protege le formation de l'autheur update et delete -->
    @can('update', $topic)
     <a href="{{ route('topics.edit', $topic) }}" class="btn btn-warning">editer topic</a>
     @endcan
     @can('delete', $topic)
      <form action="{{ route('topics.destroy', $topic) }}" method="post">
      @csrf 
      @method('DELETE')
      <button type="submit" class="btn btn-danger">Supprimer</button>
      </form>
      @endcan

    </div>
 </div>

</div>

<hr>
<h5>Commentaires</h5>
 @forelse ($topic->comments as $comment)
  <div class="card mb-2">
  <div class="card-body">
  <h2>{{ $comment->content }}</h2> 
   <div class="d-flex justify-content-between align-items-center">
         <Medium>Posté le {{ $comment->created_at->format('d/m/Y') }}</Medium>
         <span class="badge badge-primary"><h6>{{ $comment->user->name }}</h6></span>
        </div>
  </div>
  </div>
   <!-- rep de comtr -->
  @foreach ($comment->comments as $replyComment)
  <div class="card mb-2 ml-5">
  <div class="card-body">
  <h2>{{ $replyComment->content }}</h2> 
   <div class="d-flex justify-content-between align-items-center">
         <Medium>Posté le {{ $replyComment->created_at->format('d/m/Y') }}</Medium>
         <span class="badge badge-primary"><h6>{{ $replyComment->user->name }}</h6></span>
        </div>
  </div>
  </div>
  @endforeach

  @auth
   <button  class="btn btn-info mb-3" onclick="toggleReplyComment({{ $comment->id }})">Répondre</button>
    <form action="{{ route('comments.storeReply', $comment) }}"
     method="post" class="mb-3 ml-5 d-none" id="replyComment-{{ $comment->id }}">
     @csrf
      <div class="form-group">
       <label for="replyComment">Ma réponce</label>
        <textarea name="replyComment"  class="form-control @error('replyComment') 
        is-invalid @enderror" id="replyComment" rows="5"></textarea>
        @error('replyComment')
        <div class="invalid-feedback">{{ $errors->first('replyComment') }}</div>
        @enderror

      </div>
      <button type="submit" class="btn btn-primary">Repondre a ce commentaire</button>
    </form>
    @endauth
  @empty
   <div class="alert alert-info">Aucun commentaire pour ce topic</div>
   @endforelse
 <form action="{{ route('comments.store', $topic) }}" method="post" class="mt-3">
  @csrf 

   <div class="form-group">
     <label for="content">Votre commentaire</label>
     <textarea class="form-control @error('content') is-invalid @enderror" name="content" id="content" rows="5">
     </textarea>
     
     @error('content')
   <div class="invalid-feedback">{{ $errors->first('content') }}</div>
   @enderror
   </div>
  
    
    <button type="submit" class="btn btn-primary">Soumettre mon commentaire</button><br><hr>
    <button type="submit" class="btn btn-dark"><a href="{{route('topics.index')}}">retourne au sujet</a></button>
 </form>
</div>
</section>
@endsection