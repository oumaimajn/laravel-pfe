@extends('layouts.app')


@section('content')
<section class="hero-wrap js-fullheight" style="background-image: url('images/z1.jpg');" data-stellar-background-ratio="0.5">
 <div class="container">
 <button type="button" class="btn btn-outline-primary "><a href="{{ route('topics.create') }}" class="dropdown-item">creér un forum</a></button>
 
   <div class="list-group">
<!-- list--group-item ou bien card  , mettre h4 en tant que lien , mettre l'heure et nom de louteur -->
     @foreach ($topics as $topic)
      <div class="card">
        <h4><a href="{{ route('topics.show' , $topic) }}">{{ $topic->title }}</a></h4>
        <p>{{ $topic->content }}</p>
        <div class="d-flex justify-content-between align-items-center">
         <Medium>Posté le {{ $topic->created_at->format('d/m/Y a H:m') }}</Medium>
         <span class="badge badge-primary"><h6>{{ $topic->user->name }}</h6></span>
        </div>
      </div>

     @endforeach
   
   </div>
   <!-- pour le nombre de page  , mt= marge top 3 -->
   <div class="d-flex justify-content-center mt-3"> {{ $topics->links() }} </div> 
 </div>
 </section>
@endsection