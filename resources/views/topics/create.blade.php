@extends('layouts.master3')

@section('extra-js')
{!! NoCaptcha::renderJs() !!}


@section('content')
 <div class="container">
 <br><br><br>
   <h1>Creér un Sujet</h1>
   <hr>
   <form action="{{  route('topics.store') }}" method="post">
   <!-- pour protege le file web -->
   @csrf
   <div class="form-group">
   
    <label for="title">Titre de Sujet</label>
    <input type="text" class="form-control @error('title') is-invalid @enderror"  name="title" id="title">
    <!-- si ilya une error affiche error -->
    @error('title')
    <div class="invalid-feedback">{{ $errors->first('title') }}</div>
    @enderror
    </div>
   <div class="form-group">
   <label for="content">votre sujet</label>
   <textarea name="content" id="content" class="form-control @error('content') is-invalid @enderror" rows="5"></textarea>
   
   @error('content')
    <div class="invalid-feedback">{{ $errors->first('content') }}</div>
    @enderror

   </div>
   
   <button type="submit" class="btn btn-primary">Creér mon Sujet</button>
   </form>
 </div>
@endsection