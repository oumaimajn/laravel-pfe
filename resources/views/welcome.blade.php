<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
            <h1>+216 00 000 000</h1>
            <h1>écoute, aide, soutien</h1>
            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/Pink_ribbon.svg/1200px-Pink_ribbon.svg.png" class="card-img-top" width="150" height="150" alt="...">
            
                <div class="title m-b-md">
                    Solution Cancer
                </div>

                <div class="links">
                    
                    <a href="https://laravel.com/docs">Acceuil</a>
                    <a href="https://laracasts.com">Historique</a>
                    
                    <a href="http://laravel-tp1.test/informations">information</a>
                    <a href="{{route('reclamations.create')}}" > Reclamation</a>
                    <a href="" >Médicament</a>
                    
                    <a href="http://laravel-tp1.test/posts/post">medcin</a>
                </div>
            </div>
        </div>
    </body>
</html>
