
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>les type de cancer</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/album/">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <!-- Bootstrap core CSS -->
<link href="/docs/4.5/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/4.5/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/4.5/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/4.5/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon.ico">
<meta name="msapplication-config" content="/docs/4.5/assets/img/favicons/browserconfig.xml">
<meta name="theme-color" content="#563d7c">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <style>.jumbotron {
  padding-top: 3rem;
  padding-bottom: 3rem;
  margin-bottom: 0;
  background-color: #fff;
}
@media (min-width: 768px) {
  .jumbotron {
    padding-top: 6rem;
    padding-bottom: 6rem;
  }
}

.jumbotron p:last-child {
  margin-bottom: 0;
}

.jumbotron h1 {
  font-weight: 300;
}

.jumbotron .container {
  max-width: 40rem;
}

footer {
  padding-top: 3rem;
  padding-bottom: 3rem;
}

footer p {
  margin-bottom: .25rem;
}
    </style>
    <!-- Custom styles for this template -->
    <link href="album.css" rel="stylesheet">
  </head>
  <body>
   

<main role="main">

  <section class="hero-wrap js-fullheight">
    <div class="container">
      <h1>Liste des types de cancers</h1>
     
    </div>
  </section>

  <div class="album py-5 bg-light">
    <div class="container">

      <div class="row">
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
           <img src="https://www.ligue-cancer.net/sites/default/files/anatomie-poumon_0.jpg" height="225">
            <div class="card-body">
              
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                <a href="{{ route('types.index')}}" class="list-group-item list-group-item-action">Cancer du poumon</a>
                  
                </div>
               
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
          <img src="https://www.ligue-cancer.net/sites/default/files/anatomie-sein.jpg" height="225">
            <div class="card-body">
              
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                <a href="{{ route('types.index1')}}" class="list-group-item list-group-item-action">Cancer du sein</a>
                  
                </div>
               
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
          <img src="https://www.ligue-cancer.net/sites/default/files/anatomie-prostate.jpg" height="225">
            <div class="card-body">
              
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <a href="{{ route('types.index2')}}" class="list-group-item list-group-item-action">Cancer de la prostate</a>
                
                </div>
              
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
          <img src="https://www.ligue-cancer.net/sites/default/files/anatomie-colon-rectum_0.jpg" height="225">
            <div class="card-body">
             
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <a href="{{ route('types.index3')}}" class="list-group-item list-group-item-action">Cancer du côlon et du rectum</a>
                 
                </div>
               
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
          <img src="https://www.ligue-cancer.net/sites/default/files/anatomie-thyroide_0.jpg" height="225">
            <div class="card-body">
             
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <a href="{{ route('types.index4')}}" class="list-group-item list-group-item-action">Cancer de la thyroïde</a>
                  
                </div>
               
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
          <img src="https://www.ligue-cancer.net/sites/default/files/anatomie-col-uterus.jpg" height="225">
            <div class="card-body">
              
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <a href="{{ route('types.index5')}}" class="list-group-item list-group-item-action">Cancers du col de l'utérus, de l'endomètre et des ovaires</a>
                 
                </div>
                
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
          <img src="https://www.ligue-cancer.net/sites/default/files/anatomie-levres-bouche-larynx.jpg" height="225">
            <div class="card-body">
              
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <a href="{{ route('types.index6')}}" class="list-group-item list-group-item-action">Cancer lèvre-bouche-larynx</a>
                  
                </div>
                
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
          <img src="https://www.ligue-cancer.net/sites/default/files/anatomie-rein.jpg" height="225">
            <div class="card-body">
             
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <a href="{{ route('types.index7')}}" class="list-group-item list-group-item-action">Cancer du rein</a>
                  
                </div>
                
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
          <img src="https://www.ligue-cancer.net/sites/default/files/anatomie-foie.jpg" height="225">
            <div class="card-body">
             
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <a href="{{ route('types.index8')}}" class="list-group-item list-group-item-action">Cancer du foie</a>
                 
                </div>
              
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
          <img src="https://www.ligue-cancer.net/sites/default/files/anatomie-cerveau.jpg" height="225">
            <div class="card-body">
             
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <a href="{{ route('types.index9')}}" class="list-group-item list-group-item-action">Cancer du cerveau (adulte)</a>
                 
                </div>
              
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
          <img src="https://www.ligue-cancer.net/sites/default/files/anatomie-testicules.jpg" height="225">
            <div class="card-body">
             
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <a href="{{ route('types.index10')}}" class="list-group-item list-group-item-action">Cancer des testicules</a>
                 
                </div>
              
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
          <img src="https://www.ligue-cancer.net/sites/default/files/anatomie-pancreas.jpg" height="225">
            <div class="card-body">
             
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <a href="{{ route('types.index11')}}" class="list-group-item list-group-item-action">Cancer du pancréas</a>
                 
                </div>
              
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
          <img src="https://www.ligue-cancer.net/sites/default/files/anatomie-os.jpg" height="225">
            <div class="card-body">
             
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <a href="{{ route('types.index12')}}" class="list-group-item list-group-item-action">Cancer des os</a>
                 
                </div>
              
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
          <img src="https://www.ligue-cancer.net/sites/default/files/anatomie-peau.jpg" height="225">
            <div class="card-body">
             
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <a href="{{ route('types.index13')}}" class="list-group-item list-group-item-action">Cancer de la peau</a>
                 
                </div>
              
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
          <img src="https://www.ligue-cancer.net/sites/default/files/anatomie-sang.jpg" height="225">
            <div class="card-body">
             
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <a href="{{ route('types.index14')}}" class="list-group-item list-group-item-action">Les leucémies</a>
                 
                </div>
              
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</main>

<footer class="text-muted">
  <div class="container">
    <p class="float-right">
      <a href="#">Back to top</a>
    </p>
 
  </div>
</footer>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="/docs/4.5/assets/js/vendor/jquery.slim.min.js"><\/script>')</script><script src="/docs/4.5/dist/js/bootstrap.bundle.min.js" integrity="sha384-1CmrxMRARb6aLqgBO7yyAxTOQE2AKb9GfXnEo760AUcUmFx3ibVJJAzGytlQcNXd" crossorigin="anonymous"></script></body>
</html>
