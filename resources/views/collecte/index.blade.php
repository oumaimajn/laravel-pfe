



@extends('layouts.collecte1')

@section('content')  
@if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

<main role="main">

 

  <div class="album py-5 bg-light">
    <div class="container">

      <div class="row">
       
      @foreach($products as $product)
       
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
          <img src="{{$product->productimg}}" alt="..."  height="270" width="381" >
            <div class="card-body">
            
              <p class="card-text">{{$product->titre}}</p>
              <p class="card-text">{{$product->date}}</p>
              <p class="card-text">{{$product->objectif}}</p>
              <p class="card-text">{{$product->type}}</p>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <button type="button" class="btn btn-sm btn-outline-secondary"><a href="{{ route('collectes.show',$product->id) }}" class="btn btn-primary">show</a></button>
                 
                </div>
                <small class="text-muted">{{$product->id}}</small>
              </div>
            </div>
          </div>
        </div>

       @endforeach



      </div>
    </div>
  </div>

</main>

@endsection 
