





 
@extends('layouts.collecte')

@section('content')
    
 

     <div class="container-fluid" id="grad1">
    <div class="row justify-content-center mt-0">
        <div class="col-11 col-sm-9 col-md-7 col-lg-6 text-center p-0 mt-3 mb-2">
            <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                <h2><strong>Review Details</strong></h2>
                
                <div class="row">
                    <div class="col-md-12 mx-0">

                        <form   action="/store" method="post"  id="msform">
                        {{ csrf_field() }}
                            <!-- progressbar -->
                            <ul id="progressbar">
                                <li id="payment"><strong>Collecte</strong></li>
                                <li id="personal"><strong>Description Personal</strong></li>
                                <li id="account"><strong>Image</strong></li>
                                <li  class="active"  id="confirm"><strong>Finish</strong></li>
                            </ul> <!-- fieldsets -->
                            <fieldset>
                                <div class="form-card">
                                <table class="table">
            <tr>
                <td>titre:</td>
                <td><strong>{{$collecte->titre}}</strong></td>
            </tr>

            <tr>
                <td>date:</td>
                <td><strong>{{$collecte->date}}</strong></td>
            </tr>

            <tr>
                <td>objectif:</td>
                <td><strong>{{$collecte->objectif}}</strong></td>
            </tr>

            <tr>
                <td>type:</td>
                <td><strong>{{$collecte->type}}</strong></td>
            </tr>

            <tr>
                <td>description 1:</td>
                <td><strong>{{$collecte->description}}</strong></td>
            </tr>

            <tr>
                <td>description 2:</td>
                <td><strong>{{$collecte->descriptionn}}</strong></td>
            </tr>

            <tr>
                <td>description 3:</td>
                <td><strong>{{$collecte->descriptionnn}}</strong></td>
            </tr>


            <tr>
                <td>Image:</td>
                <td><strong><img alt="Product Image" src="/storage/productimg/{{$collecte->productImg}}"/></strong></td>
            </tr>
        </table>


                                </div>
                                <a type="button" href="/collecte1" class="btn btn-warning">Back to Step 1</a>
                               <a type="button" href="/collecte2" class="btn btn-warning">Back to Step 2</a>
                            <br><br>
                             <button type="submit" class="btn btn-primary">Terminer</button>
                                
                                
                            </fieldset>
                            
                            
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection 