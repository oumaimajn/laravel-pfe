@extends('layouts.collecte')

@section('content')
   
     <div class="container-fluid" id="grad1">
    <div class="row justify-content-center mt-0">
        <div class="col-11 col-sm-9 col-md-7 col-lg-6 text-center p-0 mt-3 mb-2">
            <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                
                <p>Remplissez tous les champs du formulaire pour passer à l'étape suivante</p>
                <div class="row">
                    <div class="col-md-12 mx-0">

                    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif


                        <form  action="/collecte2" method="POST" id="msform">
                        @csrf
                            <!-- progressbar -->
                            <ul id="progressbar">
                                <li id="payment"><strong>Collecte</strong></li>
                                <li  class="active"  id="personal"><strong>Description Personal</strong></li>
                                <li id="account"><strong>Image</strong></li>
                                <li id="confirm"><strong>Finish</strong></li>
                            </ul> <!-- fieldsets -->
                            <fieldset>
                                <div class="form-card">
                                    <h2 class="fs-title">INITIALISATION DE LA PAGE DE COLLECTE</h2>
                                    <br>
                                    <strong>pour quoi ce projet vous tient - il a coeur ? Transmettez- nous votre engagement? *</strong>
<textarea name="description" class="form-control" id="description" cols="30" rows="10">{{ session()->get('collecte.description') }}</textarea>
<strong>Quel est votre objectif ? *</strong>
<textarea name="descriptionn" class="form-control" id="descriptionn" cols="30" rows="10">{{ session()->get('collecte.descriptionn') }}</textarea>
<strong>Quelque chose a ajouter une anecdote a partager ? *</strong>
<textarea name="descriptionnn" class="form-control" id="descriptionnn" cols="30" rows="10">{{ session()->get('collecte.descriptionnn') }}</textarea>

        
     <a type="button" href="/collecte1" class="btn btn-warning">Retour à l'étape 1</a>    
                                      
                                    


                                </div>
                                <button type="submit" class="btn btn-primary">Continue</button>
                                
                            </fieldset>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection 