@extends('layouts.collecte')

@section('content')

<div class="container-fluid" id="grad1">
<div class="row justify-content-center mt-0">
<div class="col-12 col-sm-9 col-md-9 col-lg-9  p-0 mt-6 mb-4">
            <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                <h2><strong>Créez votre Collecte</strong></h2>
                <p>Remplissez tous les champs du formulaire pour passer à l'étape suivante</p>
                <div class="row">
                    <div class="col-md-12 mx-0">

    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif


    
    <table class="table">
        <thead class="thead-dark">
        <tr>
            
            
            

            
        </tr>
        </thead>
        <tbody>
        
        <tr>
                
                <th class="bg-light" ><a href=""><div >&nbsp &nbsp &nbsp &nbsp &nbsp {{$products->titre}}</div></a></th>
                <th class="bg-light"></th>
                
            </tr>
            
            <tr>
                
                <td height="172" width="500"><img src="{{$products->productimg}}" alt="..." height="400" width="600" ></td>
                <td height="172" width="160">
                <div class="container"  >
                <header class="blog-header  py-3">
                
                
                <button type="button" class="btn btn-secondary" style="width: 300px; height: 100px"><a href="{{route('collectedons.create')}}">je soutiens cette  collecte</a> &nbsp;<i class="fa fa-gift fa-2x"></i></button><br>
                <button type="button" class="btn btn-light"  style="width: 300px; height: 100px">Merci pour votre don !</button>
               
                </headet>
                </div>
                </td>
                
            </tr>
        
        </tbody>
    </table>










    
        
            
            
           <button class="btn btn-sm btn-outline-secondary"  style="width: 200px; height: 60px"><a href="#" class="btn btn-primary">Le projet (collecte)</a></button> 
           <button class="btn btn-sm btn-outline-secondary" style="width: 200px; height: 60px"><a href="#" class="btn btn-primary">Les Contributeurs</a></button>  
           <button class="btn btn-sm btn-outline-secondary" style="width: 200px; height: 60px"><a href="#" class="btn btn-primary">Les Commentaires</a></button> 
        
        

    <table class="table">
       
        <tbody>

       

        <thead class="">
        <tr>
            <th scope="col">nombre de projet</th>
            
            
            
        </tr>
        </thead>

        <tr>
               
               <th scope="row">{{$products->id}}</th>
               
               
             
              
               
           </tr>


        <thead class="">
        <tr>
           
            
            <th scope="col"> titre de la page de collecte *</th>
            
        </tr>
        </thead>


        <tr>
               
                
                
                
                <td>{{$products->titre}}</td>
               
                
            </tr>


        <thead class="">
        <tr>
           
            <th scope="col">Date de fin*</th>
           
        </tr>
        </thead>

        <tr>
               
                
                <td>{{$products->date}}</td>
             
                
            </tr>


        <thead class="">
        <tr>
            
            <th scope="col">Objectif de collecte en dt*</th>
            
        </tr>
        </thead>

        <tr>
               
               
                <td>{{$products->objectif}}</td>
               
              
                
            </tr>


        <thead class="">
        <tr>
           
            <th scope="col">type de collecte*</th>
           
        </tr>
        </thead>


        <tr>
               
              
                <td>{{$products->type}}</td>
                
                
            </tr>


        <thead class="">
        <tr>
            
            <th scope="col">pour quoi ce projet vous tient - il a coeur ?<br> Transmettez- nous votre engagement?*</th>
            
        </tr>
        </thead>

        <tr>
               
               
                <td>{{$products->description}}</td>
               
                
            </tr>


        <thead class="">
        <tr>
          
         
            <th scope="col">Quel est votre objectif ?*</th>
            
        </tr>
        </thead>

        <tr>
               
              
                <td>{{$products->descriptionn}}</td>
               
                
            </tr>


        <thead class="">
        <tr>
           
            <th scope="col">Quelque chose a ajouter une anecdote a partager ?*</th>
        </tr>
        </thead>

        <tr>
               
                
                <td>{{$products->descriptionnn}}</td>
                
            </tr>








            
        
        </tbody>
    </table>
    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection 