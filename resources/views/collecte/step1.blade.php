 
@extends('layouts.collecte')

@section('content')
    
 

     <div class="container-fluid" id="grad1">
    <div class="row justify-content-center mt-0">
        <div class="col-11 col-sm-9 col-md-7 col-lg-6 text-center p-0 mt-3 mb-2">
            <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                <h2><strong>Créez votre Collecte</strong></h2>
                <p>Remplissez tous les champs du formulaire pour passer à l'étape suivante</p>
                <div class="row">
                    <div class="col-md-12 mx-0">


                    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif


                        <form  action="/collecte1" method="POST" id="msform">
                        @csrf
                            <!-- progressbar -->
                            <ul id="progressbar">
                                <li class="active"  id="payment"><strong>Collecte</strong></li>
                                <li id="personal"><strong>Description Personal</strong></li>
                                <li id="account"><strong>Image</strong></li>
                                <li id="confirm"><strong>Finish</strong></li>
                            </ul> <!-- fieldsets -->
                            <fieldset>
                                <div class="form-card">
                                    <h2 class="fs-title">INITIALISATION DE LA PAGE DE COLLECTE</h2>
                                    <br>
                                    <strong>Titre de la page de collecte *</strong>
                                    <input type="text" name="titre" class="form-controll" placeholder="Enter titre" value="{{ session()->get('collecte.titre') }}">
                                    <strong>Date de fin  *</strong>
                                    <input type="date" name="date" class="form-controll" value="{{ session()->get('collecte.date') }}">
                                    <strong>Objectif de collecte en DT *</strong>
                                    <input type="text" name="objectif" class="form-controll" placeholder="Enter votre objectif" value="{{ session()->get('collecte.objectif') }}">
                                    <strong>Type de collecte *</strong> 
                                    <div class="row">
                                        
                                        <div class="col-9"> <select class="list-dt" id="type" name="type" value="{{ session()->get('collecte.type') }}">
                                        <option selected>Individuel</option>
                                        <option>En équipe</option>
                                            
                                            </select> 
                                               
                                             </div>
                                    </div>


                                </div>
                                <button type="submit" class="btn btn-primary">Continue</button>
                                
                            </fieldset>
                            
                            
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection 