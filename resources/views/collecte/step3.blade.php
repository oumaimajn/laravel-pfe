
@extends('layouts.collecte')

@section('content')
   
     <div class="container-fluid" id="grad1">
    <div class="row justify-content-center mt-0">
        <div class="col-11 col-sm-9 col-md-7 col-lg-6 text-center p-0 mt-3 mb-2">
            <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                
                <p>Remplissez tous les champs du formulaire pour passer à l'étape suivante</p>
                <div class="row">
                    <div class="col-md-12 mx-0">

         

                    <hr>
    @if(isset($collecte->productImg))
    <img alt="Product Image" src="/storage/productimg/{{$collecte->productImg}}"/>
    @endif
                        <form  action="/collecte3" method="post" enctype="multipart/form-data" id="msform">
                        {{ csrf_field() }}
        <h3>Upload Image</h3><br/><br/>
                            <!-- progressbar -->
                            <ul id="progressbar">
                                <li id="payment"><strong>Collecte</strong></li>
                                <li id="personal"><strong>Description Personal</strong></li>
                                <li   class="active"  id="account"><strong>Image</strong></li>
                                <li id="confirm"><strong>Finish</strong></li>
                            </ul> <!-- fieldsets -->
                            <fieldset>
                                <div class="form-card">
                                    <h2 class="fs-title">INITIALISATION DE LA PAGE DE COLLECTE</h2>
                                    <div class="form-group">
            <input type="file" {{ (!empty($collecte->productImg)) ? "disabled" : ''}} class="form-control-file" name="productimg" id="productimg" aria-describedby="fileHelp">
            <small id="fileHelp" class="form-text text-muted">Veuillez télécharger un fichier image valide. La taille de l'image ne doit pas dépasser 2 Mo..</small>
        </div> 
                                      
                                    


                                </div>
                                <button type="submit" class="btn btn-primary">Review Details</button>
                                @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    <br/>
                                
                            </fieldset>
                            
                        </form><br>

                        @if(isset($collecte->productImg))
    <form action="/remove-image" method="post">
        {{ csrf_field() }}
    <button type="submit" class="btn btn-danger">Remove Image</button>
    </form>
    @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection 