@extends('layouts.master4')

@section('title')
update rendez-vous
@endsection

@section('content')


<h1>update rendez-vous </h1>

  <div>
   @if(session()->get('success'))
  <div class="alert alert-danger" role="alert">
  A simple danger alert—check it out!
 </div>
 @endif
  </div>
  
<form action="{{  route('rdv.update',$rdv->id) }}" method="post">
@csrf
@method('patch')
  <div class="form-row">
  
    
    <div class="form-group col-md-6">
      <label for="nom">nom</label>
      <input type="text" class="form-control " id="nom" name="nom" value="{{$rende->nom}}" >
    </div>
    </div>
  <div class="form-group">
    <label for="prenom">prenom</label>
    <input type="text" class="form-control " id="prenom" name="prenom" value= "{{$rende->prenom}}">
   
  </div>
  <div class="form-group">
    <label for="email">email</label>
    <input type="email" class="form-control" id="email" name="email" value=" {{$rende->email}}" >
    </div>
  <div class="form-group">
    <label for="date">date</label>
    <input type="date" class="form-control " id="date" name="date" value="{{$rende->date}}" >

    
  </div>
  
  </div>
  <button type="submit" class="btn btn-primary">update</button>
</form>

@endsection
