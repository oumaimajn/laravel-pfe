@extends('layouts.master4')

@section('title')
index reclamation
@endsection

@section('content')
<section class="hero-wrap hero-wrap-2" style="background-image: url('images/a1.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-3 bread">Create Rendez-vous</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Medecin</a></span> <span>Contact us</span></p>
          </div>
        </div>
      </div>
    </section>
<!-- flash sms 1 -->
<div>
 @if(session()->get('success'))
<div class="alert alert-success" role="alert">
{{ session()->get('success')}}
</div>
@endif
</div>


<!-- flash sms 2 -->
<div>
 @if(session()->get('error'))
<div class="alert alert-danger" role="alert">
{{ session()->get('error')}}
</div>
@endif
</div>
<br>
<div class="container">
<a href="{{route('rdv.create')}}" class="btn btn-primary"> new rendez-vous</a>

<div class="col-md-12 ftco-animate text-center">

     
      <table class="table">

<thead>
  <tr>
    
    
    <td>nom</td>
    <td>prenom</td>
    <td>email</td>
    <td>date</td>
    <td>user</td>
    <td>Show</td>
    <td>reponce medecin</td>
    <td>Update</td>
    <td>Remove</td>

  </tr>
</thead>
<tbody>
@foreach($rdvs as $rdv)
  <tr>
  
    <td>{{ $rdv->name }}</td>
    <td>{{ $rdv->prenom }}</td>
    <td>{{ $rdv->email }}</td>
    <td>{{ $rdv->date }}</td>
    

    
    <td><div class="d-flex justify-content-between align-items-center">
         <Medium>Posté le {{ $rdv->created_at->format('d/m/Y a H:m') }}</Medium>
        
    </div></td>

    <td><div class="d-flex justify-content-between align-items-center">
         
         <span class="badge badge-primary"><h6>{{ $rdv->user->name }}</h6></span>
    </div></td>

   
  <td><a href="{{ route('rdv.show',$rdv->id) }}" class="btn btn-primary">show</a></td>
  


  <td><a href="#" class="btn btn-primary">reponce medecin</a></td>
  


  <td><a href="{{ route('rdv.edit',$rdv->id) }}" class="btn btn-success">edit</a></td>
 
  <!--delete -->
 
  <td>
  <form action="{{  route('rdv.destroy',$rdv->id) }}" method="post">
  @csrf
  @method('DELETE')
  <button type="submit" class="btn btn-danger">delete</button>
  </from>
  
  </td>
  
   <!-- end delete -->
  
  </tr>
  @endforeach
</tbody>

</table>
 
   

 </div>
 <div class="d-flex justify-content-center mt-3"> {{ $rdvs ->links() }} </div>
</div>

@endsection

