@section('content')
@extends('layouts.master3')
@section('content')
<br><br><br>
<th scope="col"></th>
<strong>prendre rendez vous</strong>
<nav class="navbar navbar-light justify-content-center">
<span class="navbar-text">
<div class="modal-body">
<form action="{{ route('rdv.store') }}" method="post" enctype="multipart/form-data">
@csrf

<div class="form-group">
    <label for="name">Name</label>
    <input type="text" class="form-control  @error('name') is-invalid @enderror" id="name" name="name" placeholder="entre votre nom">
    @error('name')
    <span class="invalid-feedback" role="alert">
         <strong>{{ $message }}</strong>
      </span>
      @enderror
  </div>
  <div class="form-group">
    <label for="prenom">prenom</label>
    <input type="text" class="form-control @error('prenom') is-invalid @enderror" id="prenom" name="prenom" placeholder="entre votre prenom">
    @error('prenom')
    <span class="invalid-feedback" role="alert">
         <strong>{{ $message }}</strong>
      </span>
      @enderror
  </div>
  <div class="form-group">
    <label for="email">email</label>
    <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="entre votre @email">
    @error('email')
    <span class="invalid-feedback" role="alert">
         <strong>{{ $message }}</strong>
      </span>
      @enderror
  </div>
  <div class="form-group">
    <label for="date">date</label>
    <input type="date" class="form-control  @error('date') is-invalid @enderror" id="date" name="date" placeholder="entre date">
    @error('date')
    <span class="invalid-feedback" role="alert">
         <strong>{{ $message }}</strong>
      </span>
      @enderror
  </div>
  <button type="submit" class="btn btn-primary">valider</button>
  <button type="reset" class="btn btn-danger">annuler</button>
  @endsection