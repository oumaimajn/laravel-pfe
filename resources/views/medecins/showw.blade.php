


    @extends('layouts.master2')

@section('content')
 
 
<div class="container"  >
  <header class="blog-header py-3">
    <div class="row flex-nowrap justify-content-between align-items-center">
      
      <div class="col-4 text-center">
        <a class="blog-header-logo text-dark" href="http://laravel-tp1.test/medecin">medecins</a>
      </div>
      <div class="col-4 d-flex justify-content-end align-items-center">
        <a class="text-muted" href="#" aria-label="Search">
          <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="mx-3" role="img" viewBox="0 0 24 24" focusable="false"><title>Search</title><circle cx="10.5" cy="10.5" r="7.5"/><path d="M21 21l-5.2-5.2"/></svg>
        </a>
        <a class="btn btn-sm btn-outline-secondary" href="#">Sign up</a>
      </div>
    </div>
  </header>

 

 

  <div class="row mb-2">
  <div class="col-md-12">
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
          <strong class="d-inline-block mb-2 text-success">Design</strong>
          <strong>{{ $medecin->name }}</strong>
          <div class="mb-1 text-muted">{{ $medecin->created_at->format('d/m/y') }}</div>
          <h5 class="ftco-heading-2">{{ $medecin->description }}</h5>
          
        </div>
        <div class="col-auto d-none d-lg-block">
        <img src="{{ $medecin->image }}" alt="...">
        </div>
      </div>
    </div>
    <a href="{{route('reclamation.create')}}" class="stretched-link btn btn-outline-info">reclamation medecin</a>
    
  </div>
</div>





@endsection