@extends('layouts.master4')

@section('title')
index appointement
@endsection

@section('content')
<br>
<br><br>
<strong>liste de patient qui prendre rendez-vous</strong>


<!-- flash sms 1 -->
<div>
 @if(session()->get('success'))
<div class="alert alert-success" role="alert">
{{ session()->get('success')}}
</div>
@endif
</div>


<!-- flash sms 2 -->
<div>
 @if(session()->get('error'))
<div class="alert alert-danger" role="alert">
{{ session()->get('error')}}
</div>
@endif
</div>



      <a href="{{route('appoints.create')}}" class="btn btn-primary"> new appoitement</a>
      <table class="table">

<thead>
  <tr>
    
    
    <td>name</td>
    <td>prenom</td>
    <td>email</td>
    <td>date</td>
    
    <td>Update</td>
    <td>Remove</td>

  </tr>
</thead>
<tbody>
 @foreach($appoints as $appoint)
  <tr>
    
    <td>{{ $appoint->name}}</td>
    <td>{{ $appoint->prenom}}</td>
    <td>{{ $appoint->email}}</td>
    <td>{{ $appoint->date}}</td>
  <td><a href="{{ route('appoints.edit',$appoint->id) }}" class="btn btn-success">modifier</a></td>
  <!--delete -->
  <td>
  <form action="{{ route('appoints.destroy',$appoint->id) }}" method="post">
  @csrf
  @method('DELETE')
  <button type="submit" class="btn btn-danger">delete</button>
  </from>
  
  </td>
   <!-- end delete -->
  
  </tr>
  @endforeach
</tbody>

</table>
@endsection