@extends('layouts.master')

@section('title')
update reclamation
@endsection

@section('content')


<h1>update appointement</h1>

  <div>
   @if(session()->get('success'))
  <div class="alert alert-danger" role="alert">
  A simple danger alert—check it out!
 </div>
 @endif
  </div>



<form action="{{route('appoints.update',$appoints->id) }}" method="post">
@csrf
@method('patch')
 
 <div class="form-group">
    <label for="name">Name</label>
    <input type="text" class="form-control  " id="name" name="name" value=" {{$appoints->name}}" >
 
   
  </div>
  <div class="form-group">
    <label for="prenom">prenom</label>
    <input type="text" class="form-control " id="prenom" name="prenom" value= "{{$appoints->prenom}}">
   
  </div>
  
    
  <div class="form-group">
    <label for="email">email</label>
    <input type="email" class="form-control" id="email" name="email" value=" {{$appoints->email}}" >
   
    
  </div>
  <div class="form-group">
    <label for="date">date</label>
    <input type="date" class="form-control " id="date" name="date" value="{{$appoints->date}}" >

    
  </div>
 
  
  </div>
  <button type="submit" class="btn btn-primary">update</button>
</form>

@endsection


