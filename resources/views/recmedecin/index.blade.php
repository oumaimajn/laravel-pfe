@extends('layouts.master4')

@section('title')
index reclamation
@endsection

@section('content')
<section class="hero-wrap hero-wrap-2" style="background-image: url('images/a1.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-3 bread">Create Reclamation</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Medecin</a></span> <span>Contact us</span></p>
          </div>
        </div>
      </div>
    </section>




<!-- flash sms 1 -->
<div>
 @if(session()->get('success'))
<div class="alert alert-success" role="alert">
{{ session()->get('success')}}
</div>
@endif
</div>


<!-- flash sms 2 -->
<div>
 @if(session()->get('error'))
<div class="alert alert-danger" role="alert">
{{ session()->get('error')}}
</div>
@endif
</div>
<br>

<div class="container">


<div class="col-md-12 ftco-animate text-center">

     
      <table class="table">

<thead>
  <tr>
    
    
    <td>Description</td>
    <td>Image</td>
    <td>date</td>
    <td>user</td>
    <td>affiche</td>
    <td>repondre</td>
    

  </tr>
</thead>
<tbody>
 @foreach($reclamations as $reclamation)
  <tr>
  
    <td>{{ $reclamation->description}}</td>
    <td>{{$reclamation->image}}</td>
   

    
    <td><div class="d-flex justify-content-between align-items-center">
         <Medium>Posté le {{ $reclamation->created_at->format('d/m/Y a H:m') }}</Medium>
        
    </div></td>

    <td><div class="d-flex justify-content-between align-items-center">
         
         <span class="badge badge-primary"><h6>{{ $reclamation->user->name }}</h6></span>
    </div></td>

   

    
  <td><a href="{{ route('reclamation.show',$reclamation->id) }}" class="btn btn-primary">show</a></td>
  <td><a href="{{route('repondres.create')}}" class="btn btn-primary">repondre</a></td>
  

  
  </tr>
  @endforeach
</tbody>

</table>
 
   

 </div>
 <div class="d-flex justify-content-center mt-3"> {{ $reclamations->links() }} </div>
</div>

@endsection