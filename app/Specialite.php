<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specialite extends Model
{
    protected $guarded = []; 
   // jointure
    public function medecins()
    {
        return $this->hasMany('App\Medecin');
    }
}
