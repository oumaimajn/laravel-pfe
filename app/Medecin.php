<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medecin extends Model
{
    protected $guarded = [];
    //  jointure
    public function specialite()
    {

        return $this->belongsTo('App\Specialite');
    }
}
