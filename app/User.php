<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
//zedeteha
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable,HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function topics()
    {
      return $this->hasMany('App\Topic');
    }
      // pour afficher user dans le reclamation
    public function reclamations()
    {
      return $this->hasMany('App\Reclamation');
    }

     // pour afficher user dans le reclamation
     public function rdvs()
     {
       return $this->hasMany('App\Rdv');
     }

    // pour afficher user pour les paiment

       // pour afficher user dans le role
    public function roles()
    {
      return $this->belongsToMany('App\Role');
    }

      // pour auth name admin autorise de tout
    public function isAdmin()
    {
      return $this->roles()->where('name', 'admin')->first();
    }

        // pour auth name admin et medecin  autorise de tout avec authserviceprovider utilise wherein parceque il ya un admin et medecin
    public function hasAnyRole(array $roles)
    {
      return $this->roles()->whereIn('name', $roles)->first();
    }
   
    
}
