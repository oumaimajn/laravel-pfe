<?php

namespace App\Policies;

use App\Reclamation;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReclamationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any reclamations.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->id == $reclamation->user->id;
    }

    /**
     * Determine whether the user can view the reclamation.
     *
     * @param  \App\User  $user
     * @param  \App\Reclamation  $reclamation
     * @return mixed
     */
    public function view(User $user, Reclamation $reclamation)
    {
        return $user->id == $reclamation->user->id;
    }

    /**
     * Determine whether the user can create reclamations.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->id == $reclamation->user->id;
    }

    /**
     * Determine whether the user can update the reclamation.
     *
     * @param  \App\User  $user
     * @param  \App\Reclamation  $reclamation
     * @return mixed
     */
    public function update(User $user, Reclamation $reclamation)
    {
        return $user->id == $reclamation->user->id;
    }

    /**
     * Determine whether the user can delete the reclamation.
     *
     * @param  \App\User  $user
     * @param  \App\Reclamation  $reclamation
     * @return mixed
     */
    public function delete(User $user, Reclamation $reclamation)
    { 
        return $user->id == $reclamation->user->id;
    }

    /**
     * Determine whether the user can restore the reclamation.
     *
     * @param  \App\User  $user
     * @param  \App\Reclamation  $reclamation
     * @return mixed
     */
    public function restore(User $user, Reclamation $reclamation)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the reclamation.
     *
     * @param  \App\User  $user
     * @param  \App\Reclamation  $reclamation
     * @return mixed
     */
    public function forceDelete(User $user, Reclamation $reclamation)
    {
        //
    }
}
