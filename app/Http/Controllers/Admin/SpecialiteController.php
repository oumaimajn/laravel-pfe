<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Specialite;
use Illuminate\Http\Request;

class SpecialiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $specialites = Specialite::all();
        return view('admin.specialite.index',compact('specialites'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        {
            return view('admin.specialite.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
             
         ]);

         $specialites = Specialite::create([
            'name' => $request->get('name'),
           
        ]);

        //ORM Eloquent
        return redirect('/admin/specialites')->with('success','specialites has been added..');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Specialite  $specialite
     * @return \Illuminate\Http\Response
     */
    public function show(Specialite $specialite)
    {
        return view('admin.specialite.show',compact('specialite'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Specialite  $specialite
     * @return \Illuminate\Http\Response
     */
    public function edit(Specialite $specialite)
    {
        return view('admin.specialite.edit',compact('specialite'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Specialite  $specialite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Specialite $specialite)
    {
        $request->validate([
            'name' => 'required|max:255'
        ]);
        $specialite->name = $request->get('name');
        $specialite->save();
        return redirect('/admin/specialites')->with('success','update    ..');
    }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Specialite  $specialite
     * @return \Illuminate\Http\Response
     */
    public function destroy(Specialite $specialite)
    {
        $specialite->delete();
        return redirect('/admin/specialites')->with('error','deleted  successfully  ..');
    }
}
