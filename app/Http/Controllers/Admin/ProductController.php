<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('admin.product.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin\product\create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'slug' => 'required',
            'subtitle' => 'required',
            'description' => 'required',
            'price'=>'required',
            'image' => 'required|image',
            
            
        ]);
        if($request->image){
            $path= 'product/';
            $files_path = $path . time() . '.jpg';
            $image= \Image::make($request->image)->encode('jpg');
            $path= \Storage::disk('public')->put($files_path,(string) $image);
        }
//save in database
$products = Product::create([
    'title' =>$request->get('title'),
    'slug'=>$request->get('slug'),
    'subtitle'=>$request->get('subtitle'),
    'price' =>$request->get('price'),
    'description' => $request->get('description'),
    'image' => url('/storage'.$files_path),
 
    
]);
//ORM Eloquent
return redirect('admin/product')->with('success','product has been added..');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        
       
        return view('admin.product.show',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
        return view('admin\product\edit',compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([
            'title' => 'required|max:255',
            'slug' => 'required',
            'subtitle' => 'required',
            'price'=>'required',
            'description' => 'required',
            'image' => 'required|image',
            
        ]);
        $product->title = $request->get('title');
        $product->slug = $request->get('slug');
        $product->subtitle = $request->get('subtitle');
        $product->price = $request->get('price');
        $product->description = $request->get('description');
        
        

        Product::whereId($id)->update($validateData);
        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect('/admin/product')->with('error','deleted  successfully  ..');
    }
}
