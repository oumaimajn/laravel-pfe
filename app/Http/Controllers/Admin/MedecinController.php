<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Medecin;
use Illuminate\Http\Request;

class MedecinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $medecins = Medecin::all();
        return view('admin.medecin.index',compact('medecins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        {
            return view('admin.medecin.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation des champs
        $this->validate($request,[
            'name' => 'required',
            'slug' => 'required',
           'title' => 'required',
           'description' => 'required',
            'image' => 'required|image'
            
        ]);

        if($request->image){
            $path= 'medecins/';
            $file_path = $path . time() . '.jpg';
            $image= \Image::make($request->image)->encode('jpg');
            $path= \Storage::disk('public')->put($file_path,(string) $image);
        }

        //save into the database
        $medecins = Medecin::create([
            'name' => $request->get('name'),
            'slug' => $request->get('slug'),
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'image' => url('/storage/'.$file_path),
           
        ]);
       
        //ORM Eloquent
        return redirect('/admin/medecins')->with('success','medecin has been added..');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Medecin  $medecin
     * @return \Illuminate\Http\Response
     */
    public function show(Medecin $medecin)
    {
        return view('admin.medecin.show',compact('medecin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Medecin  $medecin
     * @return \Illuminate\Http\Response
     */
    public function edit(Medecin $medecin)
    {
        return view('admin.medecin.edit',compact('medecin'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Medecin  $medecin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Medecin $medecin)
    {
        $request->validate([
            'name' => 'required|max:255',
            'title' => 'required',
            'description' => 'required'
            //'image' => 'required|image'
        ]);
        $medecin->name = $request->get('name');
        
        $medecin->title= $request->get('title');
         $medecin->description = $request->get('description');
        //$medecin->image = url('/storage/'.$file_path);
        $medecin->save();
        return redirect('/admin/medecins')->with('success','update    ..');



        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Medecin  $medecin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Medecin $medecin)
    {
        $medecin->delete();
        return redirect('/admin/medecins')->with('error','deleted  successfully  ..');
    }
}
