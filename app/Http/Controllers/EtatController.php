<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Etat;

class EtatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $etats = Etat::all();
        //yeraj3li view  aff mel produit.index o nraj3e m3aha l variable
        return view('etat.index',compact('etats'));
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('etat.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation des champs
        $this->validate($request,[
           'type' => 'required'
           
        ]);

       

        //save into the database
        $etats = Etat::create([
            
            'type' => $request->get('type')

            
        ]);
       
        //ORM Eloquent
        return redirect('/etats')->with('success','rendez-vous has been added..');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $etat = Etat::findOrfail($id);
        return view('etat.show',compact('etat'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
