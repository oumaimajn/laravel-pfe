<?php

namespace App\Http\Controllers;

use App\Collecte;
use Illuminate\Http\Request;

class CollecteController extends Controller
{
    
    public function index(Request $request)
    {
        $request->session()->forget('collecte');

        $products = \App\Collecte::all();

        return view('collecte.index',compact('products'));
    }


   

   
    public function createStep1(Request $request)
    {
        $collecte = $request->session()->get('collecte');

        return view('collecte.step1',compact('collecte'));
    }

    public function PostcreateStep1(Request $request)
    {
        $validatedData = $request->validate([
            'titre' => 'required|unique:collectes',
            'date'=> 'required',
            'objectif' => 'required',
            'type' => 'required',
        ]);
        if(empty($request->session()->get('collecte'))){
            $collecte = new \App\Collecte();
            $collecte->fill($validatedData);
            $request->session()->put('collecte', $collecte);
        }else{
            $collecte = $request->session()->get('collecte');
            $collecte->fill($validatedData);
            $request->session()->put('collecte', $collecte);
        }
        return redirect('/collecte2');
    }


    public function createStep2(Request $request)
    {
        $collecte = $request->session()->get('collecte');

        return view('collecte.step2',compact('collecte'));
    }

    public function PostcreateStep2(Request $request)
    {
        $validatedData = $request->validate([
            'description' => 'required|unique:collectes',
            'descriptionn' => 'required',
            'descriptionnn' => 'required',
        ]);
        if(empty($request->session()->get('collecte'))){
            $collecte = new \App\Collecte();
            $collecte->fill($validatedData);
            $request->session()->put('collecte', $collecte);
        }else{
            $collecte = $request->session()->get('collecte');
            $collecte->fill($validatedData);
            $request->session()->put('collecte', $collecte);
        }
        return redirect('/collecte3');
    }


    public function createStep3(Request $request)
    {  
        $collecte = $request->session()->get('collecte');
        return view('collecte.step3',compact('collecte'));
    }

    public function PostcreateStep3(Request $request)
    {
        $collecte = $request->session()->get('collecte');

        if(!isset($collecte->productImg)) {
            $request->validate([
                'productimg' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $fileName = "productImage-" . time() . '.' . request()->productimg->getClientOriginalExtension();
            $request->productimg->storeAs('productimg', $fileName);
            $collecte = $request->session()->get('collecte');
            $collecte->productImg = $fileName;
            $request->session()->put('collecte', $collecte);
        }
        return view('collecte.step4',compact('collecte'));
    }
    


    public function removeImage(Request $request)
    {
        $collecte = $request->session()->get('collecte');

        $collecte->productImg = null;

        return view('collecte.step3',compact('collecte'));
    }

    public function store(Request $request)
    {
        $collecte = $request->session()->get('collecte');

        $collecte->save();

        return redirect('/data');
    }
    

    

    public function show($id)
    {
        
        $products = \App\Collecte::findOrfail($id);
        return view('collecte.show',compact('products'));
    }
    
    

    
   
}
