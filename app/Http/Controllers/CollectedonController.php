<?php

namespace App\Http\Controllers;

use App\Collectedon;
use Illuminate\Http\Request;

class CollectedonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('don.index',compact('collectedons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('don.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Collectedon  $collectedon
     * @return \Illuminate\Http\Response
     */
    public function show(Collectedon $collectedon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Collectedon  $collectedon
     * @return \Illuminate\Http\Response
     */
    public function edit(Collectedon $collectedon)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Collectedon  $collectedon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Collectedon $collectedon)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Collectedon  $collectedon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Collectedon $collectedon)
    {
        //
    }
}
