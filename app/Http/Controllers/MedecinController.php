<?php

namespace App\Http\Controllers;
use App\Medecin;
use Illuminate\Http\Request;

class MedecinController extends Controller
{
    
    public function index()
    {
        $medecins = Medecin::all();
       // $medecins = Medecin::inRandomOrder()->take(12)->get();
        return view('medecins.index')->with('medecins' , $medecins);
    }

   

   
    public function showw($id)
    {
        $medecin = Medecin::where('id', $id)->firstOrFail();
        return view('medecins.showw')->with('medecin',$medecin);
    }

   

    

}
