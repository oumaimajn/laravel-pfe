<?php

namespace App\Http\Controllers;

use App\Topic;
use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Topic $topic)
    {
        request()->validate([
            
            'content'=> 'required|min:5'
        ]);
        
        $comment = new Comment();
        $comment->content = request('content');
        // récupérer utilisateur connecte
        $comment->user_id = auth()->user()->id;

       
        // récupérer topic et enregistre comtr
        $topic->comments()->save($comment);

        return redirect()->route('topics.show', $topic);
    }

    public function  storeCommentReply(Comment $comment)
    {
        request()->validate([
            'replyComment' => 'required|min:3'
        ]);
         //nov rep
         $commentReply = new Comment();
        // ce le contenut de la rep , la resurce envoie 
        $commentReply->content = request('replyComment');
        $commentReply->user_id = auth()->user()->id;

        $comment->comments()->save($commentReply);
        
        //retourn ala page mere 
        return redirect()->back();
    }
}
