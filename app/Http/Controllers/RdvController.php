<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\rdv;
class RdvController extends Controller
{

    public function __construct()
    {
       
      $this->middleware('auth')->except(['index','show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //variable edv traj3li mel table rdv koul chay ali hiya all tjibli donne mwjoud fi DB
      $rdvs = Rdv::latest()->paginate(5);
      //yeraj3li view  aff mel rdv.index o nraj3e m3aha l variable
      return view('rdvpatient.index',compact('rdvs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('rdvpatient.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation des champs
        $data = $request->validate([
            'name' => 'required',
             'prenom' => 'required',
             'email' => 'required',
             'date' => 'required'
             
         ]);
         $rdv = auth()->user()->rdvs()->create($data);
            
              //ORM Eloquent
        return redirect('/rdv')->with('success','rendezvous has been added..');
 
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rdv = Rdv::findOrfail($id);
        return view('rdvpatient.show',compact('rdvs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rdv = Rdv::findOrfail($id);
        return view('rdvpatient.edit',compact('rdvs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation des champs
        $validateData = $request->validate([
          
          'name' => 'required',
          'prenom' => 'required',
          'email' => 'required',
          'date' => 'required'
          
      ]);
      //update recherche produit update
      RDV::whereId($id)->update($validateData);

      //redirect to /rendez-vous page + affiche msg rendez-vous has been ..
      return redirect('/rdv')->with('success','update    ..');
  }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        $rdv->delete();
        return redirect('/rdv')->with('error','deleted  successfully  ..');

    }
}
