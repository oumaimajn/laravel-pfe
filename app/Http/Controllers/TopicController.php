<?php

namespace App\Http\Controllers;

use App\Topic;
use Illuminate\Http\Request;

class TopicController extends Controller

{
    
    // ne pas le droit de faire supp et edite et ajoute un comtr sans connecté 
        
     //affiche sauf method index, show avec except
      public function __construct()
      {
         
        $this->middleware('auth')->except(['index','show']);
      }

      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    //latest : recupere dans un ordre decroissante ,  paginate affiche par page , stoke dans le variable topics
    {
        $topics = Topic::latest()->paginate(5);
        return view('topics.index', compact('topics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //mettre dans la dossier topics est fichier create.blade.php 
        return view('topics.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     // recupper les donne dans "data" , recupper le utilisateur connecte avec auth->user->create
    public function store(Request $request)
    {
        
        $data = $request->validate([
            'title' => 'required|min:5',
            'content'=> 'required|min:10'
        ]);

       $topic = auth()->user()->topics()->create($data);

        return redirect()->route('topics.show', $topic->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function show(Topic $topic)
    {
       return view('topics.show', compact('topic'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function edit(Topic $topic)
    {
        $this->authorize('update', $topic);

        return view('topics.edit',compact('topic'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Topic $topic)
    {
        $this->authorize('update', $topic);
        $data = $request->validate([
            'title' => 'required|min:5',
            'content'=> 'required|min:10'
        ]);
        //  en pass le methode update avec les donnee data 
        $topic->update($data);

        return redirect()->route('topics.show',$topic->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Topic $topic)
    {
        $this->authorize('delete', $topic);
        Topic::destroy($topic->id);
        return redirect('/topics');
    }
}
