<?php

namespace App\Http\Controllers;

use App\Repondre;
use Illuminate\Http\Request;

class RepondreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $repondres = Repondre::all();
        return view('rep.index',compact('repondres'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('recmedecin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'description' => 'required',
             
         ]);

         $repondres = Repondre::create([
            'description' => $request->get('description'),
           
        ]);

        //ORM Eloquent
        return redirect('/medecin/medecin')->with('success','msg has been added..');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Repondre  $repondre
     * @return \Illuminate\Http\Response
     */
    public function show(Repondre $repondre)
    {
        return view('rep.show',compact('repondre'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Repondre  $repondre
     * @return \Illuminate\Http\Response
     */
    public function edit(Repondre $repondre)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Repondre  $repondre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Repondre $repondre)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Repondre  $repondre
     * @return \Illuminate\Http\Response
     */
    public function destroy(Repondre $repondre)
    {
        //
    }
}
