<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\reclamation;
class ReclamationController extends Controller
{

    public function __construct()
    {
       
      $this->middleware('auth')->except(['index','show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //variable prodect traj3li mel table produit koul chay ali hiya all tjibli donne mwjoud fi DB
        $reclamations = Reclamation::latest()->paginate(5);
        //yeraj3li view  aff mel produit.index o nraj3e m3aha l variable
        return view('recpatient.index',compact('reclamations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('recpatient.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation des champs
        $this->validate($request,[
           'description' => 'required',
            'image' => 'required|image'
        ]);

        $reclamation = auth()->user()->reclamations()->create([
            'description' => request('description'),
            'image' => request('image')
           ]);

       /*  if($request->image){
            $path= 'reclamations/';
            $file_path = $path . time() . '.jpg';
            $image= \Image::make($request->image)->encode('jpg');
            $path= \Storage::disk('public')->put($file_path,(string) $image);
        }

        //save into the database
        $reclamations = Reclamation::create([
            'description' => $request->get('description'),
            'image' => url('/storage/'.$file_path),
        ]);
        */
        
    
        //ORM Eloquent
        return redirect('/reclamation')->with('success','reclamation has been added..');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $reclamation = Reclamation::findOrfail($id);
        return view('recpatient.show',compact('reclamation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $reclamation = Reclamation::findOrfail($id);
        return view('recpatient.edit',compact('reclamation'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        //validation des champs
        $validateData = $request->validate([
            
            'description' => 'required'
            
        ]);
        //update recherche produit update
        Reclamation::whereId($id)->update($validateData);

        //redirect to /reclamations page + affiche msg reclamation has been ..
        return redirect('/reclamation')->with('success','update    ..');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Reclamation   $reclamation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reclamation $reclamation)
    {
        $this->authorize('delete', $reclamation);
        $reclamation->delete();
        return redirect('/reclamation')->with('error','deleted  successfully  ..');

    }
}