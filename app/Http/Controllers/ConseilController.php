<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ConseilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('conseils.index');
    }
    public function prevention()
    {
        //
        return view('conseils.prevention');
    }
    public function brochure()
    {
        //
        return view('conseils.brochure');

    }
    public function entreprise()
    {
        //
        return view('conseils.entreprise');

    }
    public function kiabi()
    {
        //
        return view('conseils.kiabi');
    }
    public function orange()
    {
        //
        return view('conseils.orange');
    }
    public function PSA()
    {
        //
        return view('conseils.PSA');
    }
    public function benevol()
    {
        //
        return view('conseils.benevol');
    }
}
   