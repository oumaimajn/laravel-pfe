<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});






/* partie medecin routes*/
Route::group([ 'prefix'=>'medecin' , 'namespace' => 'Medecin' , 'middleware' => ['auth']], function() {
   
     Route::resource('medecin', 'ReclamationController');
     Route::resource('pmedecin', 'PmedecinController');
     Route::resource('amedecin', 'AppointementController');
     
     
   
    
     
     
 });

 /*partie front*/
Route::get('/medecin', 'MedecinController@index')->name('medecin.indexx');
Route::get('/medecin/{id}', 'MedecinController@showw')->name('medecins.showw');


Route::resource('etats','EtatController');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');

Route::resource('typess','typeController');


Route::get('posts/{id}', 'HomeController@show')->name('posts.show');

Route::resource('reclamation','ReclamationController');
//Route::get('/dataa', 'ReclamationController@index');
//Route::post('/storee', 'ReclamationController@store')->name('dataa.store');;

Route::resource('repondres','RepondreController');

Route::resource('informations','InformationController');
Route::resource('premiers','PremierController');
Route::resource('forums','ForumController');

Route::resource('topics','TopicController');
//sujet
Route::post('/comments/{topic}', 'CommentController@store')->name('comments.store');
Route::post('/commentReply/{comment}', 'CommentController@storeCommentReply')->name('comments.storeReply');


//admin
Route::group([ 'prefix'=>'admin' , 'namespace' => 'Admin' , 'middleware' => ['auth']], function() {
   // Route::resource('medicaments', 'MedicamentController');
    Route::resource('specialites', 'SpecialiteController');
    Route::resource('medecins', 'MedecinController');
});


//collecte
Route::get('/collecte1', 'CollecteController@createStep1')->name('signup');
Route::post('/collecte1', 'CollecteController@PostcreateStep1');
Route::get('/collecte2', 'CollecteController@createStep2');
Route::post('/collecte2', 'CollecteController@PostcreateStep2');
Route::get('/collecte3', 'CollecteController@createStep3');
Route::post('/collecte3', 'CollecteController@PostcreateStep3');
Route::post('/remove-image', 'CollecteController@removeImage');
Route::post('/store', 'CollecteController@store');
Route::get('/show', 'CollecteController@show');
Route::get('/data', 'CollecteController@index');
Route::resource('collectes','CollecteController');

Route::resource('collectedons','CollectedonController');
Route::resource('dons','DonController');


/*route d'achat de medicament*/

Route::get('/boutique','ProductController@index')->name('products.index');
Route::get('/boutique/{slug}','ProductController@show')->name('products.show');
Route::get('/search', 'ProductController@search')->name('products.search');

/*panier Routes */
Route::group(['middleware'=>['auth']],function(){
Route::get('/panier', 'CartController@index')->name('cart.index');
Route::post('/panier/ajouter', 'CartController@store')->name('carte.store');
Route::patch('/panier/{rowId}', 'CartController@update')->name('cart.update');
Route::delete('/panier/{rowId}','CartController@destroy')->name('cart.destroy');
});
/*rout de paiement*/
Route::group(['middleware'=>['auth']],function(){
Route::get('/paiement', 'checkoutController@index')->name('checkout.index');
Route::post('/paiement', 'checkoutController@store')->name('checkout.store');
Route::get('/merci', 'checkoutController@thankYou')->name ('checkout.thankYou');
});
/*admin */
Route::group(['prefix' => 'admin', 'namespace' => 'Admin','middleware'=> ['auth']], function () {
    Route::resource('product', 'ProductController'); 
 
     
 });

 //*calendar*//
Route::get('/calendar', function () {
    return view('calendar');
});
Route::resource('/temps','TempsController');
/*rendez_vous*/
Route::resource('rdv','RdvController');

/*types de cancer*/
 Route::resource('chiffre','TypesdecancerController');
 Route::get('/type','TypesController@index')->name('types.index');
 Route::get('/sein','TypesController@index1')->name('types.index1');
 Route::get('/prostat','TypesController@index2')->name('types.index2');
 Route::get('/colon','TypesController@index3')->name('types.index3');
 Route::get('/thyroid','TypesController@index4')->name('types.index4');
 Route::get('/uterus','TypesController@index5')->name('types.index5');
 Route::get('/bouche','TypesController@index6')->name('types.index6');
 Route::get('/rein','TypesController@index7')->name('types.index7');
 Route::get('/foi','TypesController@index8')->name('types.index8');
 Route::get('/cerveau','TypesController@index9')->name('types.index9');
 Route::get('/testiculs','TypesController@index10')->name('types.index10');
 Route::get('/pancreas','TypesController@index11')->name('types.index11');
 Route::get('/os','TypesController@index12')->name('types.index12');
 Route::get('/peau','TypesController@index13')->name('types.index13');
 Route::get('/leucemies','TypesController@index14')->name('types.index14');
 /*conseil*/
 Route::resource('conseil','ConseilController');
 Route::get('/prevention','ConseilController@prevention')->name('conseils.prevention');
 Route::get('/brochure','ConseilController@brochure')->name('conseils.brochure');
 Route::get('/entreprise','ConseilController@entreprise')->name('conseils.entreprise');
 Route::get('/kiabi','ConseilController@kiabi')->name('conseils.kiabi');
 Route::get('/orange','ConseilController@orange')->name('conseils.orange');
 Route::get('/PSA','ConseilController@PSA')->name('conseils.PSA');
 Route::get('/benevol','ConseilController@benevol')->name('conseils.benevol');

/*admin */
Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('can:manage-users')->group(function() {
   
 Route::resource('users','UsersController');
     
 });
